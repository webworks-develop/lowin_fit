<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('regiones', 'Api\ApiController@getRegions')->name('api.regiones');

Route::get('regiones-envios', 'Api\ApiController@getRegionsDispatch')->name('api.regiones-envios');

Route::get('regiones/{region}/comunas', 'Api\ApiController@getComunasByRegion')->name('api.regiones.comunas');
