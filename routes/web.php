<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);

Route::middleware(['auth', 'checkUserSession'])->prefix('administracion')->group(function() {
    
    Route::prefix('/')->name('home.')->group(function () {

        Route::get('/', 'Admin\HomeController@index')->name('index');

        Route::get('profile', 'Admin\HomeController@profile')->name('profile');

        Route::post('profile', 'Admin\HomeController@updateProfile')->name('update-profile');

        Route::post('cancel', 'Admin\HomeController@cancel')->name('cancel');
    });

    Route::prefix('crm')->name('crm.')->group(function () {

        Route::prefix('facturas')->name('invoices.')->group(function () {

            Route::get('/', 'Admin\FacturasController@index')->middleware(['role:superadministrador|administrador'])->name('index');

            Route::get('{id}/detalle', 'Admin\FacturasController@show')->middleware(['role:superadministrador|administrador|vendedor'])->name('show');

            Route::get('{id}/editar', 'Admin\FacturasController@edit')->middleware(['role:superadministrador|administrador'])->name('edit');

            Route::post('{id}/editar', 'Admin\FacturasController@update')->middleware(['role:superadministrador|administrador'])->name('update');

            Route::post('{id}/aprobar', 'Admin\FacturasController@approve')->middleware(['role:superadministrador|administrador'])->name('approve');

            Route::post('{id}/anular', 'Admin\FacturasController@cancel')->middleware(['role:superadministrador|administrador'])->name('cancel');
        });

        Route::prefix('clientes')->middleware(['role:superadministrador|administrador'])->name('clients.')->group(function () {

            Route::get('/', 'Admin\ClientesController@index')->name('index');

            Route::get('pendientes', 'Admin\ClientesController@indexPending')->name('index-pending');

            Route::get('nuevo', 'Admin\ClientesController@create')->name('create');

            Route::post('nuevo', 'Admin\ClientesController@store')->name('store');

            Route::get('{id}/ficha', 'Admin\ClientesController@show')->name('show');

            Route::get('{id}/editar', 'Admin\ClientesController@edit')->name('edit');

            Route::post('{id}/editar', 'Admin\ClientesController@update')->name('update');

            Route::post('{id}/eliminar', 'Admin\ClientesController@delete')->name('delete');

            Route::post('{id}/activar', 'Admin\ClientesController@activate')->name('activate');

            Route::post('{id}/suspender', 'Admin\ClientesController@suspend')->name('suspend');

            Route::post('{id}/anular', 'Admin\ClientesController@cancel')->name('cancel');

            Route::post('{id}/contactar', 'Admin\ClientesController@contact')->name('contact');

            Route::prefix('{id}/evaluaciones')->middleware(['role:superadministrador|administrador'])->name('evaluations.')->group(function () {

                Route::prefix('entrenamientos')->middleware(['role:superadministrador|administrador'])->name('trainers.')->group(function () {

                    Route::get('/', 'Admin\EvaluacionesController@trainers')->name('index');

                    Route::get('nuevo', 'Admin\EvaluacionesController@createTrainers')->name('create');

                    Route::post('nuevo', 'Admin\EvaluacionesController@storeTrainers')->name('store');

                    Route::get('{evaluation}/editar', 'Admin\EvaluacionesController@editTrainers')->name('edit');

                    Route::post('{evaluation}/editar', 'Admin\EvaluacionesController@updateTrainers')->name('update');

                    Route::post('{evaluation}/eliminar', 'Admin\EvaluacionesController@deleteTrainers')->name('delete');
                });

                Route::prefix('nutricionistas')->middleware(['role:superadministrador|administrador'])->name('nutritionists.')->group(function () {
    
                    Route::get('/', 'Admin\EvaluacionesController@nutritionist')->name('index');

                    Route::get('nuevo', 'Admin\EvaluacionesController@createNutritionist')->name('create');

                    Route::post('nuevo', 'Admin\EvaluacionesController@storeNutritionist')->name('store');

                    Route::get('{evaluation}/editar', 'Admin\EvaluacionesController@editNutritionist')->name('edit');

                    Route::post('{evaluation}/editar', 'Admin\EvaluacionesController@updateNutritionist')->name('update');

                    Route::post('{evaluation}/eliminar', 'Admin\EvaluacionesController@deleteNutritionist')->name('delete');
                });

                Route::prefix('{evaluation}/items')->middleware(['role:superadministrador|administrador'])->name('items.')->group(function () {

                    Route::post('{item}/editar', 'Admin\EvaluacionesController@updateItem')->name('update');

                    Route::post('{item}/eliminar', 'Admin\EvaluacionesController@deleteItem')->name('delete');
                });
            });
        });

        Route::prefix('entrenadores')->middleware(['role:superadministrador|administrador'])->name('trainers.')->group(function () {

            Route::get('/', 'Admin\EntrenadoresController@index')->name('index');

            Route::get('nuevo', 'Admin\EntrenadoresController@create')->name('create');

            Route::post('nuevo', 'Admin\EntrenadoresController@store')->name('store');

            Route::get('{id}/detalle', 'Admin\EntrenadoresController@detail')->name('detail');

            Route::get('{id}/editar', 'Admin\EntrenadoresController@edit')->name('edit');

            Route::post('{id}/editar', 'Admin\EntrenadoresController@update')->name('update');

            Route::post('{id}/eliminar', 'Admin\EntrenadoresController@delete')->name('delete');

            Route::post('{id}/activar', 'Admin\EntrenadoresController@activate')->name('activate');

            Route::post('{id}/suspender', 'Admin\EntrenadoresController@suspend')->name('suspend');

            Route::post('{id}/anular', 'Admin\EntrenadoresController@cancel')->name('cancel');
        });

        Route::prefix('nutricionistas')->middleware(['role:superadministrador|administrador'])->name('nutritionists.')->group(function () {

            Route::get('/', 'Admin\NutricionistasController@index')->name('index');

            Route::get('nuevo', 'Admin\NutricionistasController@create')->name('create');

            Route::post('nuevo', 'Admin\NutricionistasController@store')->name('store');

            Route::get('{id}/detalle', 'Admin\NutricionistasController@detail')->name('detail');

            Route::get('{id}/editar', 'Admin\NutricionistasController@edit')->name('edit');

            Route::post('{id}/editar', 'Admin\NutricionistasController@update')->name('update');

            Route::post('{id}/eliminar', 'Admin\NutricionistasController@delete')->name('delete');

            Route::post('{id}/activar', 'Admin\NutricionistasController@activate')->name('activate');

            Route::post('{id}/suspender', 'Admin\NutricionistasController@suspend')->name('suspend');

            Route::post('{id}/anular', 'Admin\NutricionistasController@cancel')->name('cancel');
        });

        /*Route::prefix('informes')->middleware(['role:superadministrador|administrador'])->name('reports.')->group(function () {

            Route::get('/', 'Admin\InformesController@index')->name('index');

            Route::get('proccess', 'Admin\InformesController@proccess')->name('proccess');
        });*/
    });

    Route::prefix('configuracion')->name('configuration.')->group(function () {

        Route::prefix('planes')->middleware(['role:superadministrador|administrador'])->name('plans.')->group(function () {

            Route::get('/', 'Admin\PlanesController@index')->name('index');

            Route::get('nuevo', 'Admin\PlanesController@create')->name('create');

            Route::post('nuevo', 'Admin\PlanesController@store')->name('store');

            Route::get('{id}/editar', 'Admin\PlanesController@edit')->name('edit');

            Route::post('{id}/editar', 'Admin\PlanesController@update')->name('update');

            Route::post('{id}/eliminar', 'Admin\PlanesController@delete')->name('delete');

            Route::post('{id}/suspender', 'Admin\PlanesController@suspend')->name('suspend');

            Route::post('{id}/activar', 'Admin\PlanesController@activate')->name('activate');
        });

        Route::prefix('usuarios')->middleware(['role:superadministrador|administrador'])->name('users.')->group(function () {

            Route::get('/', 'Admin\UsuariosController@index')->name('index');

            Route::get('nuevo', 'Admin\UsuariosController@create')->name('create');

            Route::post('nuevo', 'Admin\UsuariosController@store')->name('store');

            Route::get('{id}/editar', 'Admin\UsuariosController@edit')->name('edit');

            Route::post('{id}/editar', 'Admin\UsuariosController@update')->name('update');

            Route::post('{id}/eliminar', 'Admin\UsuariosController@delete')->name('delete');

            Route::post('{id}/suspender', 'Admin\UsuariosController@suspend')->name('suspend');

            Route::post('{id}/activar', 'Admin\UsuariosController@activate')->name('activate');
        });        
    });
});