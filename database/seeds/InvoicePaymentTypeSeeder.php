<?php

use App\Globals\Constants;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class InvoicePaymentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {

            DB::beginTransaction();

            DB::table('factura_tipo_pago')->insert([
                [
                    'slug'       => Str::slug(Constants::NAME_PAYMENT_TYPE_WEBPAY),
                    'nombre'     => Str::ucfirst(Constants::NAME_PAYMENT_TYPE_WEBPAY),
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ],
                [
                    'slug'       => Str::slug(Constants::NAME_PAYMENT_TYPE_TRANSFER),
                    'nombre'     => Str::ucfirst(Constants::NAME_PAYMENT_TYPE_TRANSFER),
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]
            ]);

            DB::commit();

        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}
