<?php

use App\Globals\Constants;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            
            DB::beginTransaction();

            DB::table('roles')->insert([
                [
                    'name' => Constants::NAME_ROLE_SUPERADMINISTRADOR,
                    'display_name' => Str::ucfirst(Constants::NAME_ROLE_SUPERADMINISTRADOR),
                    'description' => 'Has Special full access to the entire system'
                ],
                [
                    'name' => Constants::NAME_ROLE_ADMINISTRADOR,
                    'display_name' => Str::ucfirst(Constants::NAME_ROLE_ADMINISTRADOR),
                    'description' => 'Has full access to the entire system'
                ],
                [
                    'name' => Constants::NAME_ROLE_ENTRENADOR,
                    'display_name' => Str::ucfirst(Constants::NAME_ROLE_ENTRENADOR),
                    'description' => 'Has limited access to the system'
                ],
                [
                    'name' => Constants::NAME_ROLE_NUTRICIONISTA,
                    'display_name' => Str::ucfirst(Constants::NAME_ROLE_NUTRICIONISTA),
                    'description' => 'Has limited access to the system'
                ],
                [
                    'name' => Constants::NAME_ROLE_CLIENTE,
                    'display_name' => Str::ucfirst(Constants::NAME_ROLE_CLIENTE),
                    'description' => 'Has limited access to the system'
                ]
            ]);

            DB::commit();

        } catch (Exception $e) {
            DB::rollback();
            throw $e;            
        }
    }
}
