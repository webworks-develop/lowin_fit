<?php

use App\Globals\Constants;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ClientStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {

            DB::beginTransaction();

            DB::table('cliente_estado')->insert([
                [
                    'slug' => Str::slug(Constants::NAME_ESTADO_CLIENTE_PENDIENTE),
                    'nombre' => Str::ucfirst(Constants::NAME_ESTADO_CLIENTE_PENDIENTE),
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ],
                [
                    'slug' => Str::slug(Constants::NAME_ESTADO_CLIENTE_ACTIVO),
                    'nombre' => Str::ucfirst(Constants::NAME_ESTADO_CLIENTE_ACTIVO),
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ],
                [
                    'slug' => Str::slug(Constants::NAME_ESTADO_CLIENTE_VENCIDO),
                    'nombre' => Str::ucfirst(Constants::NAME_ESTADO_CLIENTE_VENCIDO),
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ],
                [
                    'slug' => Str::slug(Constants::NAME_ESTADO_CLIENTE_SUSPENDIDO),
                    'nombre' => Str::ucfirst(Constants::NAME_ESTADO_CLIENTE_SUSPENDIDO),
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ],
                [
                    'slug' => Str::slug(Constants::NAME_ESTADO_CLIENTE_ANULADO),
                    'nombre' => Str::ucfirst(Constants::NAME_ESTADO_CLIENTE_ANULADO),
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ],                
            ]);

            DB::commit();

        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}
