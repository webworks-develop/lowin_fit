<?php

use App\Globals\Constants;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class AdminUsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {

            DB::beginTransaction();

            $rolSuperAdmin = DB::table('roles')->where('name', Constants::NAME_ROLE_SUPERADMINISTRADOR)->first();
            $rolAdmin = DB::table('roles')->where('name', Constants::NAME_ROLE_ADMINISTRADOR)->first();

            $idUserSuperAdmin = DB::table('users')->insertGetId([
                'name'              => 'Super Administrador',
                'email'             => 'superadmin@inmobi.cl',
                'password'          => bcrypt('12345678'),
                'email_verified_at' => now(),                
                'remember_token'    => Str::random(10),
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s')
            ]);

            $idUserAdmin = DB::table('users')->insertGetId([
                'name'              => 'Administrador',
                'email'             => 'rmillon@gmail.com',
                'password'          => bcrypt('12345678'),
                'email_verified_at' => now(),                
                'remember_token'    => Str::random(10),
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s')
            ]);

            DB::table('role_user')->insert([
                'user_id' => $idUserSuperAdmin,
                'role_id' => $rolSuperAdmin->id
            ]);

            DB::table('role_user')->insert([
                'user_id' => $idUserAdmin,
                'role_id' => $rolAdmin->id
            ]);

            DB::commit();

        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}
