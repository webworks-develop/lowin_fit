<?php

use App\Helpers\Plan;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class CurrencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {

            DB::beginTransaction();

            DB::table('tipo_moneda')->insert([
                [
                    'slug'        => Str::slug(Plan::CURRENCY_CLP),
                    'nombre'      => Plan::CURRENCY_CLP,
                    'abreviatura' => '$',
                    'created_at'  => date('Y-m-d H:i:s'),
                    'updated_at'  => date('Y-m-d H:i:s'),
                ],
                [
                    'slug'        => Str::slug(Plan::CURRENCY_USD),
                    'nombre'      => Plan::CURRENCY_USD,
                    'abreviatura' => '$',
                    'created_at'  => date('Y-m-d H:i:s'),
                    'updated_at'  => date('Y-m-d H:i:s'),
                ]
            ]);

            DB::commit();
            
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}
