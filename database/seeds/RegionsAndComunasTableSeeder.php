<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class RegionsAndComunasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {

            DB::beginTransaction();

            $data = $this->getData();

            foreach ($data as $region) {

                $regionId = DB::table('region')->insertGetId([
                    'slug' => Str::slug($region['nombre']),
                    'nombre' => $region['nombre']
                ]);

                foreach ($region['comunas'] as $comuna) {

                    DB::table('comuna')->insert([
                        'slug' => Str::slug($comuna),
                        'nombre' => $comuna,
                        'region_id' => $regionId
                    ]);
                }
            }

            DB::commit();

        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function getData()
    {
        return [
            [
                "nombre" => "Arica y Parinacota",
                'ISO_3166_2_CL' => 'CL-AP',
                "comunas" => ["Arica", "Camarones", "Putre", "General Lagos"]
            ],
            [
                "nombre" => "Tarapacá",
                'ISO_3166_2_CL' => 'CL-TA',
                "comunas" => ["Iquique", "Alto Hospicio", "Pozo Almonte", "Camiña", "Colchane", "Huara", "Pica"]
            ],
            [
                "nombre" => "Antofagasta",
                'ISO_3166_2_CL' => 'CL-AN',
                "comunas" => ["Antofagasta", "Mejillones", "Sierra Gorda", "Taltal", "Calama", "Ollagüe", "San Pedro de Atacama", "Tocopilla", "María Elena"]
            ],
            [
                "nombre" => "Atacama",
                'ISO_3166_2_CL' => 'CL-AT',
                "comunas" => ["Copiapó", "Caldera", "Tierra Amarilla", "Chañaral", "Diego de Almagro", "Vallenar", "Alto del Carmen", "Freirina", "Huasco"]
            ],
            [
                "nombre" => "Coquimbo",
                'ISO_3166_2_CL' => 'CL-CO',
                "comunas" => ["La Serena", "Coquimbo", "Andacollo", "La Higuera", "Paiguano", "Vicuña", "Illapel", "Canela", "Los Vilos", "Salamanca", "Ovalle", "Combarbalá", "Monte Patria", "Punitaqui", "Río Hurtado"]
            ],
            [
                "nombre" => "Valparaíso",
                'ISO_3166_2_CL' => 'CL-VS',
                "comunas" => ["Valparaíso", "Casablanca", "Concón", "Juan Fernández", "Puchuncaví", "Quintero", "Viña del Mar", "Isla de Pascua", "Los Andes", "Calle Larga", "Rinconada", "San Esteban", "La Ligua", "Cabildo", "Papudo", "Petorca", "Zapallar", "Quillota", "Calera", "Hijuelas", "La Cruz", "Nogales", "San Antonio", "Algarrobo", "Cartagena", "El Quisco", "El Tabo", "Santo Domingo", "San Felipe", "Catemu", "Llaillay", "Panquehue", "Putaendo", "Santa María", "Quilpué", "Limache", "Olmué", "Villa Alemana"]
            ],
            [
                "nombre" => "Región del Libertador Gral. Bernardo O’Higgins",
                'ISO_3166_2_CL' => 'CL-LI',
                "comunas" => ["Rancagua", "Codegua", "Coinco", "Coltauco", "Doñihue", "Graneros", "Las Cabras", "Machalí", "Malloa", "Mostazal", "Olivar", "Peumo", "Pichidegua", "Quinta de Tilcoco", "Rengo", "Requínoa", "San Vicente", "Pichilemu", "La Estrella", "Litueche", "Marchihue", "Navidad", "Paredones", "San Fernando", "Chépica", "Chimbarongo", "Lolol", "Nancagua", "Palmilla", "Peralillo", "Placilla", "Pumanque", "Santa Cruz"]
            ],
            [
                "nombre" => "Región del Maule",
                'ISO_3166_2_CL' => 'CL-ML',
                "comunas" => ["Talca", "ConsVtución", "Curepto", "Empedrado", "Maule", "Pelarco", "Pencahue", "Río Claro", "San Clemente", "San Rafael", "Cauquenes", "Chanco", "Pelluhue", "Curicó", "Hualañé", "Licantén", "Molina", "Rauco", "Romeral", "Sagrada Familia", "Teno", "Vichuquén", "Linares", "Colbún", "Longaví", "Parral", "ReVro", "San Javier", "Villa Alegre", "Yerbas Buenas"]
            ],
            [
                "nombre" => "Región del Biobío",
                'ISO_3166_2_CL' => 'CL-BI',
                "comunas" => ["Concepción", "Coronel", "Chiguayante", "Florida", "Hualqui", "Lota", "Penco", "San Pedro de la Paz", "Santa Juana", "Talcahuano", "Tomé", "Hualpén", "Lebu", "Arauco", "Cañete", "Contulmo", "Curanilahue", "Los Álamos", "Tirúa", "Los Ángeles", "Antuco", "Cabrero", "Laja", "Mulchén", "Nacimiento", "Negrete", "Quilaco", "Quilleco", "San Rosendo", "Santa Bárbara", "Tucapel", "Yumbel", "Alto Biobío", "Chillán", "Bulnes", "Cobquecura", "Coelemu", "Coihueco", "Chillán Viejo", "El Carmen", "Ninhue", "Ñiquén", "Pemuco", "Pinto", "Portezuelo", "Quillón", "Quirihue", "Ránquil", "San Carlos", "San Fabián", "San Ignacio", "San Nicolás", "Treguaco", "Yungay"]
            ],
            [
                "nombre" => "Región de la Araucanía",
                'ISO_3166_2_CL' => 'CL-AR',
                "comunas" => ["Temuco", "Carahue", "Cunco", "Curarrehue", "Freire", "Galvarino", "Gorbea", "Lautaro", "Loncoche", "Melipeuco", "Nueva Imperial", "Padre las Casas", "Perquenco", "Pitrufquén", "Pucón", "Saavedra", "Teodoro Schmidt", "Toltén", "Vilcún", "Villarrica", "Cholchol", "Angol", "Collipulli", "Curacautín", "Ercilla", "Lonquimay", "Los Sauces", "Lumaco", "Purén", "Renaico", "Traiguén", "Victoria",]
            ],
            [
                "nombre" => "Región de Los Ríos",
                'ISO_3166_2_CL' => 'CL-LR',
                "comunas" => ["Valdivia", "Corral", "Lanco", "Los Lagos", "Máfil", "Mariquina", "Paillaco", "Panguipulli", "La Unión", "Futrono", "Lago Ranco", "Río Bueno"]
            ],
            [
                "nombre" => "Región de Los Lagos",
                'ISO_3166_2_CL' => 'CL-LL',
                "comunas" => ["Puerto Montt", "Calbuco", "Cochamó", "Fresia", "FruVllar", "Los Muermos", "Llanquihue", "Maullín", "Puerto Varas", "Castro", "Ancud", "Chonchi", "Curaco de Vélez", "Dalcahue", "Puqueldón", "Queilén", "Quellón", "Quemchi", "Quinchao", "Osorno", "Puerto Octay", "Purranque", "Puyehue", "Río Negro", "San Juan de la Costa", "San Pablo", "Chaitén", "Futaleufú", "Hualaihué", "Palena"]
            ],
            [
                "nombre" => "Región Aisén del Gral. Carlos Ibáñez del Campo",
                'ISO_3166_2_CL' => 'CL-AI',
                "comunas" => ["Coihaique", "Lago Verde", "Aisén", "Cisnes", "Guaitecas", "Cochrane", "O’Higgins", "Tortel", "Chile Chico", "Río Ibáñez"]
            ],
            [
                "nombre" => "Región de Magallanes y de la AntárVca Chilena",
                'ISO_3166_2_CL' => 'CL-MA',
                "comunas" => ["Punta Arenas", "Laguna Blanca", "Río Verde", "San Gregorio", "Cabo de Hornos (Ex Navarino)", "AntárVca", "Porvenir", "Primavera", "Timaukel", "Natales", "Torres del Paine"]
            ],
            [
                "nombre" => "Región Metropolitana de Santiago",
                'ISO_3166_2_CL' => 'CL-RM',
                "comunas" => ["Cerrillos", "Cerro Navia", "Conchalí", "El Bosque", "Estación Central", "Huechuraba", "Independencia", "La Cisterna", "La Florida", "La Granja", "La Pintana", "La Reina", "Las Condes", "Lo Barnechea", "Lo Espejo", "Lo Prado", "Macul", "Maipú", "Ñuñoa", "Pedro Aguirre Cerda", "Peñalolén", "Providencia", "Pudahuel", "Quilicura", "Quinta Normal", "Recoleta", "Renca", "San Joaquín", "San Miguel", "San Ramón", "Vitacura", "Puente Alto", "Pirque", "San José de Maipo", "Colina", "Lampa", "TilVl", "San Bernardo", "Buin", "Calera de Tango", "Paine", "Melipilla", "Alhué", "Curacaví", "María Pinto", "San Pedro", "Talagante", "El Monte", "Isla de Maipo", "Padre Hurtado", "Peñaflor", "Santiago Centro"]
            ]
        ];
    }
}
