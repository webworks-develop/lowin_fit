<?php

use App\Globals\Constants;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class InvoiceStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {

            DB::beginTransaction();

            DB::table('factura_estado')->insert([
                [
                    'slug'       => Str::slug(Constants::NAME_ESTADO_FACTURA_PENDIENTE),
                    'nombre'     => Str::ucfirst(Constants::NAME_ESTADO_FACTURA_PENDIENTE),
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ],
                [
                    'slug'       => Str::slug(Constants::NAME_ESTADO_FACTURA_APROBADO),
                    'nombre'     => Str::ucfirst(Constants::NAME_ESTADO_FACTURA_APROBADO),
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ],
                [
                    'slug'       => Str::slug(Constants::NAME_ESTADO_FACTURA_ANULADO),
                    'nombre'     => Str::ucfirst(Constants::NAME_ESTADO_FACTURA_ANULADO),
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ]
            ]);

            DB::commit();

        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }    
}
