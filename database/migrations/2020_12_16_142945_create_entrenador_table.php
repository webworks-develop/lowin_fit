<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntrenadorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entrenador', function (Blueprint $table) {
            $table->id();
            $table->string('codigo')->unique();
            $table->string('nombre');
            $table->string('apellido');
            $table->string('rut', 20);
            $table->string('telefono', 25);
            $table->string('telefono_extra', 25)->nullable();
            $table->date('fecha_nacimiento');
            $table->string('experiencia')->nullable();
            $table->string('especialidad')->nullable();
            $table->string('direccion');
            $table->unsignedBigInteger('comuna_id');
            $table->unsignedBigInteger('user_id');
            $table->tinyInteger('estado')->default(1);
            $table->text('observaciones')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('comuna_id', 'entrenador_comuna_id_foreign')
                ->references('id')->on('comuna')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('user_id', 'entrenador_user_id_foreign')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entrenador');
    }
}
