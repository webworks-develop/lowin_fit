<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacturaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factura', function (Blueprint $table) {
            $table->id();
            $table->string('orden', 191)->unique();
            $table->string('boleta', 50)->unique()->nullable();
            $table->date('fecha');
            $table->date('fecha_vencimiento');
            $table->unsignedBigInteger('tipo_moneda_id');
            $table->float('subtotal');
            $table->smallInteger('porcentaje_iva')->default(0);
            $table->float('iva')->default(0);
            $table->float('descuento')->default(0);
            $table->float('total');
            $table->unsignedBigInteger('cliente_id');
            $table->unsignedBigInteger('factura_estado_id');
            $table->unsignedBigInteger('factura_tipo_pago_id')->nullable();
            $table->string('path_boleta', 191)->nullable();
            $table->string('observaciones')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('tipo_moneda_id', 'factura_tipo_moneda_id_foreign')
                ->references('id')->on('tipo_moneda')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('cliente_id', 'factura_cliente_id_foreign')
                ->references('id')->on('cliente')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('factura_estado_id', 'factura_estado_id_foreign')
                ->references('id')->on('factura_estado')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('factura_tipo_pago_id', 'factura_tipo_pago_id_foreign')
                ->references('id')->on('factura_tipo_pago')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('factura');
    }
}
