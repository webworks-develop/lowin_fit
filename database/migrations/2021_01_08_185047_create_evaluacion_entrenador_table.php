<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEvaluacionEntrenadorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluacion_entrenador', function (Blueprint $table) {
            $table->unsignedBigInteger('evaluacion_id');
            $table->unsignedBigInteger('entrenador_id');

            $table->foreign('evaluacion_id', 'evaluacion_entrenador_evaluacion_id_foreign')
                ->references('id')->on('evaluacion')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('entrenador_id', 'evaluacion_entrenador_entrenador_id_foreign')
                ->references('id')->on('entrenador')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evaluacion_entrenador');
    }
}
