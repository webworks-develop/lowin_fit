<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComunaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comuna', function (Blueprint $table) {
            $table->id();
            $table->string('slug', 255)->unique();
            $table->string('nombre', 255);
            $table->bigInteger('region_id')->unsigned();

            $table->index(["region_id"], 'FK_regions_ids1_idx');

            $table->foreign('region_id', 'FK_regions_ids1_idx')
                ->references('id')->on('region')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comuna');
    }
}
