<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEvaluacionItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluacion_item', function (Blueprint $table) {
            $table->id();
            $table->smallInteger('orden');
            $table->unsignedBigInteger('evaluacion_id');
            $table->string('pregunta', 255);
            $table->string('respuesta', 255);
            $table->tinyInteger('estado')->default(1);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('evaluacion_id', 'evaluacion_item_evaluacion_id_foreign')
                ->references('id')->on('evaluacion')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evaluacion_item');
    }
}
