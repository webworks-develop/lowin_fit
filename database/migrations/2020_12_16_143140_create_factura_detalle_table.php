<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacturaDetalleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factura_detalle', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('factura_id');
            $table->unsignedInteger('plan_id');
            $table->string('nombre');
            $table->integer('cantidad');
            $table->float('precio');
            $table->timestamps();

            $table->foreign('factura_id', 'factura_id_foreign')
                ->references('id')->on('factura')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('plan_id', 'plan_id_foreign')
                ->references('id')->on('plans')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('factura_detalle');
    }
}
