<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEvaluacionNutricionistaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluacion_nutricionista', function (Blueprint $table) {
            $table->unsignedBigInteger('evaluacion_id');
            $table->unsignedBigInteger('nutricionista_id');

            $table->foreign('evaluacion_id', 'evaluacion_nutricionista_evaluacion_id_foreign')
                ->references('id')->on('evaluacion')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('nutricionista_id', 'evaluacion_nutricionista_nutricionista_id_foreign')
                ->references('id')->on('nutricionista')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evaluacion_nutricionista');
    }
}
