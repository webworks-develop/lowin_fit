<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClienteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cliente', function (Blueprint $table) {
            $table->id();
            $table->string('codigo')->unique();
            $table->string('nombre');
            $table->string('apellido');
            $table->string('rut', 20);
            $table->string('telefono', 25);
            $table->string('telefono_extra', 25)->nullable();
            $table->date('fecha_nacimiento');
            $table->float('peso')->nullable();
            $table->smallInteger('altura')->nullable();
            $table->string('direccion');
            $table->unsignedBigInteger('comuna_id');
            $table->unsignedBigInteger('cliente_estado_id');
            $table->unsignedBigInteger('user_id');
            $table->text('observaciones')->nullable();            
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('comuna_id', 'cliente_comuna_id_foreign')
                ->references('id')->on('comuna')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('cliente_estado_id', 'cliente_estado_cliente_id_foreign')
                ->references('id')->on('cliente_estado')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('user_id', 'cliente_user_id_foreign')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cliente');
    }
}
