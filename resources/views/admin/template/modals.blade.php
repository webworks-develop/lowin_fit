<div class="content-modals">    
    <div class="modal fade" id="contact-modal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            {!! Form::open(['id' => 'contact-modal-form', 'method' => 'POST', 'files' => true]) !!}
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="defaultModalLabel">Contactar Cliente</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -25px; font-size: 28px;">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row clearfix">
                            <div class="col-xs-12">
                                <div class="form-group form-float">
                                    <label class="form-label">Asunto (Opcional)</label>
                                    <input type="text" class="form-control" name="asunto">
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="form-group form-float">
                                    <label class="form-label">Mensaje</label>
                                    <textarea name="mensaje" class="form-control no-resize" rows="4" required="required"></textarea>                                
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="form-group form-float">
                                    <label class="form-label">Adjuntar archivo (Opcional)</label>
                                    <input type="file" class="form-control" name="file">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">                        
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CANCELAR</button>
                        <button type="submit" class="btn btn-link waves-effect">ENVIAR</button>
                    </div>
                </div>                    
            {!! Form::close() !!}
        </div>
    </div>
</div>