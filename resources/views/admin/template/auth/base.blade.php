<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@section('head')
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', 'Löwin | Fit & Nut')</title>

    <!-- Favicon-->
    <link rel="icon" href="../../favicon.ico" type="image/x-icon">

    @routes

    @section('styles')
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext&display=swap" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons&display=swap" rel="stylesheet" type="text/css">
        <!-- Bootstrap Core Css -->
        <link href="{{ asset('assets/admin/plugins/bootstrap/css/bootstrap.css') }}" rel="stylesheet">
        <!-- Waves Effect Css -->
        <link href="{{ asset('assets/admin/plugins/node-waves/waves.css') }}" rel="stylesheet" />
        <!-- Animation Css -->
        <link href="{{ asset('assets/admin/plugins/animate-css/animate.css') }}" rel="stylesheet" />
        <!-- Custom Css -->
        <link href="{{ asset('assets/admin/css/style.css') }}" rel="stylesheet">

        <style>
            .card .body input {
                padding: 0 5px;
            }
        </style>
    @show
</head>
@show

<body class="login-page">
    <div class="login-box">
        <div class="logo">
            <a href="javascript:void(0);">
                <b>Löwin | Fit & Nut</b>
            </a>
        </div>
        <div class="card">
            <div class="body">
                @yield('content')
            </div>
        </div>
    </div>

    @section('scripts')
        <!-- Jquery Core Js -->
        <script src="{{ asset('assets/admin/plugins/jquery/jquery.min.js') }}"></script>
        <!-- Bootstrap Core Js -->
        <script src="{{ asset('assets/admin/plugins/bootstrap/js/bootstrap.js') }}"></script>
        <!-- Waves Effect Plugin Js -->
        <script src="{{ asset('assets/admin/plugins/node-waves/waves.js') }}"></script>
        <!-- Validation Plugin Js -->
        <script src="{{ asset('assets/admin/plugins/jquery-validation/jquery.validate.js') }}"></script>
        <!-- Custom Js -->
        <script src="{{ asset('assets/admin/js/admin.js') }}"></script>
        <script src="{{ asset('assets/admin/js/pages/examples/sign-in.js') }}"></script>
    @show
</body>
</html>