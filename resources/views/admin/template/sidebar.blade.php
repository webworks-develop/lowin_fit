<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info">
            <div class="info-container">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><h3>{{ auth()->user()->name }}</h3></div>
                <div class="email">{{ auth()->user()->email }}</div>
                <div class="btn-group user-helper-dropdown">
                    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="javascript:void(0);"><i class="material-icons">person</i>Perfil</a></li>
                        <li role="seperator" class="divider"></li>
                        <li><a href="javascript:void(0);"><i class="material-icons">group</i>Mis Coach</a></li>
                        <li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Mi plan</a></li>
                        <li role="seperator" class="divider"></li>
                        <li>
                            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="material-icons">input</i>
                                Salir
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            @role([Constants::NAME_ROLE_SUPERADMINISTRADOR, Constants::NAME_ROLE_ADMINISTRADOR])
                <ul class="list">
                    <li class="header">MENU PRINCIPAL</li>
                    <li class="{{ Ekko::isActiveRoute(['home.index'], 'active') }}">
                        <a href="{{ route('home.index') }}">
                            <i class="material-icons col-red">home</i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li class="{{ Ekko::areActiveRoutes(['crm.clients.*'], 'active') }}">
                        <a href="{{ route('crm.clients.index') }}">
                            <i class="material-icons col-red">work</i>
                            <span>Clientes</span>
                        </a>
                    </li>
                    <li class="{{ Ekko::areActiveRoutes(['crm.trainers.*'], 'active') }}">
                        <a href="{{ route('crm.trainers.index') }}">
                            <i class="material-icons col-red">fitness_center</i>
                            <span>Entrenadores</span>
                        </a>
                    </li>
                    <li class="{{ Ekko::areActiveRoutes(['crm.nutritionists.*'], 'active') }}">
                        <a href="{{ route('crm.nutritionists.index') }}">
                            <i class="material-icons col-red">assignment_ind</i>
                            <span>Nutricionistas</span>
                        </a>
                    </li>
                    <li class="{{ Ekko::areActiveRoutes(['crm.invoices.*'], 'active') }}">
                        <a href="{{ route('crm.invoices.index') }}">
                            <i class="material-icons col-red">insert_drive_file</i>
                            <span>Facturas</span>
                        </a>
                    </li>                    
                    <li class="{{ Ekko::areActiveRoutes(['configuration.plans.*'], 'active') }}">
                        <a href="{{ route('configuration.plans.index') }}">
                            <i class="material-icons col-red">card_membership</i>
                            <span>Planes</span>
                        </a>
                    </li>
                    <li class="{{ Ekko::areActiveRoutes(['configuration.users.*'], 'active') }}">
                        <a href="{{ route('configuration.users.index') }}">
                            <i class="material-icons col-red">supervisor_account</i>
                            <span>Usuarios</span>
                        </a>
                    </li>
                    <li style="display: none;">
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons col-red">settings</i>
                            <span>Configuración</span>
                        </a>
                        <ul class="ml-menu">                        
                        </ul>
                    </li>
                    <li>
                        <a href="#">
                            <i class="material-icons col-light-blue">announcement</i>
                            <span>Solicitar Ayuda</span>
                        </a>
                    </li>
                </ul>
            @endrole

            @role(Constants::NAME_ROLE_CLIENTE)
                <ul class="list">
                    <li class="header">MAIN NAVIGATION</li>
                    <li class="active">
                        <a href="index.html">
                            <i class="material-icons">trending_up</i>
                            <span>Mis Estadísticas</span>
                        </a>
                    </li>
                    <li>
                        <a href="ficha.html">
                            <i class="material-icons col-red">favorite</i>
                            <span>Mi Ficha</span>
                        </a>
                    </li>
                    <li>
                        <a href="entrenamiento.html">
                            <i class="material-icons col-red">fitness_center</i>
                            <span>Mi Entrenamiento</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="material-icons col-red">kitchen</i>
                            <span>Mi Plan Nutricional</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="material-icons col-green">spa</i>
                            <span>Bienestar</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="material-icons col-green">wb_incandescent</i>
                            <span>Tips</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="material-icons col-green">card_giftcard</i>
                            <span>Mis Gift Card</span>
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class="material-icons col-light-blue">announcement</i>
                            <span>Solicitar Ayuda</span>
                        </a>
                    </li>
                </ul>
            @endrole
        </div>
        <!-- #Menu -->
        <!-- Footer -->
        <div class="legal">
            <div class="copyright">
                &copy; 2020 - 2021&nbsp;<a href="{{ route('home.index') }}">Löwin | Fit & Nut</a>
            </div>
            <div class="version">
                <b>Version:&nbsp;</b>&nbsp;{{ Constants::VERSION_SYSTEM }}
            </div>
        </div>
        <!-- #Footer -->
    </aside>
    <!-- #END# Left Sidebar -->
    <!-- Right Sidebar -->
    <aside id="rightsidebar" class="right-sidebar">
        <div>
            <div class="fade in active in active" id="settings">
                <div class="demo-settings">
                    <p>NOTIFICACIONES</p>
                    <ul class="setting-list">
						<li>
                            <span>Notificame por Email</span>
                            <div class="switch">
                                <label><input type="checkbox" checked><span class="lever"></span></label>
                            </div>
                        </li>
                        <li>
                            <span>Respuestas</span>
                            <div class="switch">
                                <label><input type="checkbox" checked><span class="lever"></span></label>
                            </div>
                        </li>
                        <li>
                            <span>Preguntas</span>
                            <div class="switch">
                                <label><input type="checkbox"><span class="lever"></span></label>
                            </div>
                        </li>
						<li>
                            <span>Nuevo Contenidos</span>
                            <div class="switch">
                                <label><input type="checkbox"><span class="lever"></span></label>
                            </div>
                        </li>
						<li>
                            <span>Termino Plan</span>
                            <div class="switch">
                                <label><input type="checkbox"><span class="lever"></span></label>
                            </div>
                        </li>
                    </ul>
                    <p>RECORDATORIOS</p>
                    <ul class="setting-list">
                        <li>
                            <span>Recordatorios por Email</span>
                            <div class="switch">
                                <label><input type="checkbox" checked><span class="lever"></span></label>
                            </div>
                        </li>
                        <li>
                            <span>Entrenamiento</span>
                            <div class="switch">
                                <label><input type="checkbox" checked><span class="lever"></span></label>
                            </div>
                        </li>
                        <li>
                            <span>Evaluación</span>
                            <div class="switch">
                                <label><input type="checkbox" checked><span class="lever"></span></label>
                            </div>
                        </li>
						<li>
                            <span>Renovación de plan</span>
                            <div class="switch">
                                <label><input type="checkbox"><span class="lever"></span></label>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </aside>
    <!-- #END# Right Sidebar -->
</section>