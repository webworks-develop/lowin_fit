<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@section('head')
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', 'Löwin | Fit & Nut')</title>

    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    @routes

    @section('styles')
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext&display=swap" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons&display=swap" rel="stylesheet" type="text/css">
        <!-- Bootstrap Core Css -->
        <link href="{{ asset('assets/admin/plugins/bootstrap/css/bootstrap.css') }}" rel="stylesheet">
        <!-- Waves Effect Css -->
        <link href="{{ asset('assets/admin/plugins/node-waves/waves.css') }}" rel="stylesheet" />
        <!-- Animation Css -->
        <link href="{{ asset('assets/admin/plugins/animate-css/animate.css') }}" rel="stylesheet" />
        <!-- Sweetalert Css -->
        <link href="{{ asset('assets/admin/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet" />
        <!-- Custom Css -->
        <link href="{{ asset('assets/admin/css/style.css') }}" rel="stylesheet">
        <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
        <link href="{{ asset('assets/admin/css/themes/theme-red.min.css') }}" rel="stylesheet" />        
    @show
</head>
@show
<body class="theme-red">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Cargando...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->

    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->

    <!-- Top Bar -->
    @include('admin.template.header')
    <!-- #Top Bar -->
    
    <!-- Sidebar -->
    @include('admin.template.sidebar')
    <!-- #Sidebar -->

    <!-- Content -->
    <section class="content">
        <div class="container-fluid">
			@yield('content')
        </div>
    </section>
    <!-- #Content -->

    <!-- Modals -->
	@include('admin.template.modals')
	<!-- End Modals -->

    <div>
        <form id="form-post-request" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        </form>
    </div>

    @section('scripts')
        <!-- Jquery Core Js -->
        <script src="{{ asset('assets/admin/plugins/jquery/jquery.min.js') }}"></script>
        <!-- Bootstrap Core Js -->
        <script src="{{ asset('assets/admin/plugins/bootstrap/js/bootstrap.js') }}"></script>
        <!-- Slimscroll Plugin Js -->
        <script src="{{ asset('assets/admin/plugins/jquery-slimscroll/jquery.slimscroll.js') }}"></script>
        <!-- Waves Effect Plugin Js -->
        <script src="{{ asset('assets/admin/plugins/node-waves/waves.js') }}"></script>
        <!-- Jquery CountTo Plugin Js -->
        <script src="{{ asset('assets/admin/plugins/jquery-countto/jquery.countTo.js') }}"></script>
        <!-- SweetAlert Plugin Js -->
        <script src="{{ asset('assets/admin/plugins/sweetalert/sweetalert.min.js') }}"></script>
        <!-- Bootstrap Notify Plugin Js -->
        <script src="{{ asset('assets/admin/plugins/bootstrap-notify/bootstrap-notify.js') }}"></script>
        <!-- Custom Js -->
        <script src="{{ asset('assets/admin/js/admin.js') }}"></script>        
        <!-- Demo Js -->
        <script src="{{ asset('assets/admin/js/demo.js') }}"></script>        
        <script>
            $(function () {
                $(document).on('click', '.btn-request-post', function(e) {
                    e.preventDefault();
                    var url = $(this).attr('href');

                    swal({
                        title: "¿Estás seguro que desea realizar la acción seleccionada?",
                        text: "Una vez confirmada, no podrá restablecer dicha acción!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                    }, function (isConfirm) {
                        if (isConfirm) {
                            $formPostRequest = $('#form-post-request');
                            $formPostRequest.attr('action', url);
                            $formPostRequest.submit();
                        }
                    });
                });

                $('.btn-contact-modal').on('click', function(event) {
					event.preventDefault();

					var $link = $(this),
						$divModalContact = $('#contact-modal');

					//Clean Form
					$divModalContact.find('form input, form textarea').not('input[name="_token"]').val('');
					//Assign URL action to form
					$divModalContact.find('form').attr('action', $link.attr('href'));

					$divModalContact.modal('show');
                });

                $(document).on('click', '.btn-send-verify', function(e) {
                    e.preventDefault();
                    var url = $(this).attr('href');

                    $formPostRequest = $('#form-post-request');
                    $formPostRequest.attr('action', url);
                    $formPostRequest.submit();
                });
            });
        </script>

        @if(!auth()->user()->hasVerifiedEmail() && !session('resent'))
			<script>
				$(function () {
                    $.notify({
                        title: 'Verifique su dirección de correo electrónico',
                        message: 'Si no recibió el correo electrónico, haga click aquí para solicitar otro.',
                        icon: 'la la-info-circle',
                        url: route('verification.resend'),
                        target: '_self'
                    }, {
                        type: 'bg-green',
                        allow_dismiss: true,
                        newest_on_top: true,
                        timer: 3000,
                        placement: {
                            from: 'top',
                            align: 'right'
                        },
                        template: '<div data-notify="container" class="bootstrap-notify-container alert alert-dismissible {0}" role="alert" style="max-width:460px;">' +
                            '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                            '<span data-notify="icon"></span> ' +
                            '<span data-notify="title">{1}</span> ' +
                            '<span data-notify="message">{2}</span>' +
                            '<div class="progress" data-notify="progressbar">' +
                            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                            '</div>' +
                            '<a href="{3}" target="{4}" class="btn-send-verify" data-notify="url"></a>' +
                            '</div>'
                    });					
				});
			</script>
		@endif

		@if(session()->has('messageType'))
	        @if(session()->get('messageType') == 'success')
	            <script>
                    swal("Acción Completada!", "{{ session()->get('message') }}", "success");
	            </script>
	        @elseif(session()->get('messageType') == 'error')
	            <script>
	                swal("Acción Completada!", "{{ session()->get('message') }}", "danger");
	            </script>
			@endif
		@elseif(session('resent'))
			<script>
                swal("Confirmación de Correo Electrónico", "Se ha enviado un nuevo enlace de verificación a su dirección de correo electrónico.", "success");
			</script>
		@elseif(session('verified'))
			<script>
                swal("Confirmación de Correo Electrónico", "Su dirección de correo electrónico ha sido confirmada correctamente.", "success");
			</script>
		@elseif(session('status'))
			<script>
                swal("{{ session('status') }}", "", "success");
	        </script>
	    @endif
    @show
</body>
</html>