@extends('admin.template.emails.base')

@section('content')

    <table cellspacing="0" cellpadding="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
        <tr style="border-collapse:collapse"> 
            <td align="left" style="padding:0;Margin:0;width:560px"> 
                <table cellspacing="0" cellpadding="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;border-width:1px;border-style:solid;border-color:transparent" role="presentation"> 
                    <tr style="border-collapse:collapse"> 
                        <td align="center" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px">
                            <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:28px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:56px;color:#333333">
                                <strong>Hola, {{ $fullName }}</strong>
                            </p>
                        </td> 
                    </tr>                    
                    <tr style="border-collapse:collapse"> 
                        <td align="left" style="padding:0;Margin:0;padding-right:20px">
                            <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:24px;color:#333333">
                                Recibió este correo electrónico porque recibimos una solicitud de restablecimiento de contraseña para su cuenta.
                            </p>
                        </td> 
                    </tr>
                    <tr style="border-collapse:collapse"> 
                        <td align="center" style="Margin:0;padding-bottom:20px;padding-left:20px;padding-right:20px;padding-top:20px">
                            <span class="es-button-border" style="border-style:solid;border-color:transparent;background:#1ABC9A;border-width:3px;display:inline-block;border-radius:0px;width:auto">
                                <a href="{{ url(config('app.url') . route('password.reset', $token, false)) }}" class="es-button" target="_blank" style="mso-style-priority:100 !important;text-decoration:none;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:arial, 'helvetica neue', helvetica, sans-serif;font-size:18px;color:#FFFFFF;border-style:solid;border-color:#1ABC9A;border-width:15px 25px;display:inline-block;background:#1ABC9A;border-radius:0px;font-weight:normal;font-style:normal;line-height:22px;width:auto;text-align:center">
                                    Recuperar contraseña
                                </a>
                            </span>
                        </td> 
                    </tr>
                    <tr style="border-collapse:collapse"> 
                        <td align="left" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px">
                            <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:24px;color:#333333">
                                Este enlace de restablecimiento de contraseña caducará en {{ config('auth.passwords.users.expire') }} minutos.
                            </p>
                        </td>
                    </tr>
                    <tr style="border-collapse:collapse"> 
                        <td align="left" style="padding:0;Margin:0">
                            <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:24px;color:#333333">
                                Si no solicitó un restablecimiento de contraseña, no se requiere ninguna otra acción.
                            </p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

@endsection

@section('subcopy')

    <tr style="border-collapse:collapse"> 
        <td align="center" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px">
            <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:12px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:18px;color:#FFFFFF">
                Si tiene problemas para hacer click en el botón "Restablecer Contraseña", copia y pega la URL a continuación en tu navegador web: 
                <a class="view" target="" href="{{ url(config('app.url') . route('password.reset', $token, false)) }}" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:lato, 'helvetica neue', helvetica, arial, sans-serif;font-size:14px;text-decoration:none;color:#ccc;">{{ url(config('app.url') . route('password.reset', $token, false)) }}</a>
            </p>
        </td> 
    </tr>    

@endsection