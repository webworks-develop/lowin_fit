@extends('admin.template.emails.base')

@section('content')

    <table cellspacing="0" cellpadding="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
        <tr style="border-collapse:collapse"> 
            <td align="left" style="padding:0;Margin:0;width:560px"> 
                <table cellspacing="0" cellpadding="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;border-width:1px;border-style:solid;border-color:transparent" role="presentation"> 
                    <tr style="border-collapse:collapse">
                        <td align="center" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px">
                            <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:28px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:56px;color:#333333">
                                <strong>Hola, {{ $client->getFullName() }}</strong>
                            </p>
                        </td> 
                    </tr>                    
                    <tr style="border-collapse:collapse"> 
                        <td align="left" style="padding:0;Margin:0;padding-right:20px">
                            <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:24px;color:#333333;text-align:justify;">
                                Bienvenido al primer y único portal que facilita la compra y venta de ejemplares en Chile. Un excelente espacio para aquellos que buscan un nuevo compañero de equitación como propietarios que quieren dejar su caballo en buenas manos.
                            </p>
                        </td> 
                    </tr>
                    <tr style="border-collapse:collapse"> 
                        <td align="left" style="padding:0;Margin:0;padding-top:15px;padding-right:20px">
                            <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:24px;color:#333333;text-align:justify;">
                                Por los momentos tu cuenta se encuentra inactiva. Para comenzar a publicar tus ejemplares, primero debes realizar el pago correspondiente 
                                al plan que anteriormente seleccionaste. Posterior a esto, debes confirmar tu pago a las ejecutivas de ventas para su revisión y proceder a la activación de su cuenta.
                            </p>
                        </td> 
                    </tr>
                    <tr style="border-collapse:collapse"> 
                        <td align="left" style="padding:0;Margin:0;padding-top:15px;padding-right:20px">
                            <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:24px;color:#333333;text-align:justify;">
                                Al momento de confirmar su pago recibirá una notificación indicando que su cuenta ha sido activada y puede proceder a la publicación de sus ejemplares.
                            </p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

@endsection

@section('subcopy')

    <tr style="border-collapse:collapse"> 
        <td align="center" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px">
            <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:12px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:18px;color:#FFFFFF">
                Recibió este correo electrónico porque realizó un registro en <a href="{{ '#' }}">lowin.fit</a>, si desconoce esta acción ignore este correo.
            </p>
        </td> 
    </tr>    

@endsection