@extends('admin.template.emails.base')

@section('content')

    <table cellspacing="0" cellpadding="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px"> 
        <tr style="border-collapse:collapse"> 
            <td align="left" style="padding:0;Margin:0;width:560px"> 
                <table cellspacing="0" cellpadding="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;border-width:1px;border-style:solid;border-color:transparent" role="presentation"> 
                    <tr style="border-collapse:collapse">
                        <td align="center" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px">
                            <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:28px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:56px;color:#333333">
                                <strong>Hola, {{ $client->getFullName() }}</strong>
                                <br>
                                Tienes un nuevo mensaje
                            </p>
                        </td> 
                    </tr>                    
                    <tr style="border-collapse:collapse"> 
                        <td align="left" style="padding:0;Margin:0;padding-top:10px;padding-right:20px">
                            <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:16px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:24px;color:#333333">
                                {{ $mensaje }}
                            </p>
                        </td> 
                    </tr>                    
                </table>
            </td>
        </tr>
    </table>

@endsection

@section('subcopy')

    <tr style="border-collapse:collapse"> 
        <td align="center" style="padding:0;Margin:0;padding-top:10px;padding-bottom:10px">
            <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:12px;font-family:arial, 'helvetica neue', helvetica, sans-serif;line-height:18px;color:#FFFFFF">
                Este mensaje fue enviado por LOWIN FIT (<a href="mailto:admin@lowinfit.cl">admin@lowinfit.cl</a>) a <a href="{{ $client->user->email }}">{{ $client->user->email }}</a> a través de su plataforma.
            </p>
        </td> 
    </tr>    

@endsection