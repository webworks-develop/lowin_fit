@extends('admin.template.base')

@section('styles')

	@parent
	<!-- JQuery DataTable Css -->
    <link href="{{ asset('assets/admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet" />    
	
@endsection

@section('content')

    <div class="row clearfix">
        <div class="col-xs-12">
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home.index') }}">
                        <i class="material-icons">home</i>&nbsp;Inicio
                    </a>
                </li>
                <li class="active">
                    <i class="material-icons">insert_drive_file</i>&nbsp;Facturas
                </li>
            </ol>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        LISTADO DE FACTURAS
                    </h2>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                            <thead>
                                <tr>
                                    <th># Factura</th>
                                    <th>N° Orden</th>
                                    <th>Cliente</th>
                                    <th>Fecha</th>
                                    <th>Fecha Venc.</th>
                                    <th>Monto</th>
                                    <th>Tipo de Pago</th>
                                    <th>Estado</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th># Factura</th>
                                    <th>N° Orden</th>
                                    <th>Cliente</th>
                                    <th>Fecha</th>
                                    <th>Fecha Venc.</th>
                                    <th>Monto</th>
                                    <th>Tipo de Pago</th>
                                    <th>Estado</th>
                                    <th>Acciones</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                @foreach($facturas as $factura)
                                    <tr>
                                        <td>{{ $factura->id }}</td>
                                        <td><a href="{{ route('crm.invoices.edit', $factura->id) }}">{{ $factura->orden }}</a></td>
                                        <td>{{ $factura->cliente->getFullName() }}</td>
                                        <td>{{ $factura->fecha->format('d-m-Y') }}</td>
                                        <td>{{ $factura->fecha_vencimiento->format('d-m-Y') }}</td>                                        
                                        <td>{{ Plan::formatMoneyValueByCurrencyAndValue($factura->tipoMoneda->slug, $factura->total) }}</td>
                                        <td>{{ $factura->tipoPago->nombre }}</td>
                                        <td>{{ $factura->estado->nombre }}</td>
                                        <td>
                                            <a href="{{ route('crm.invoices.show', $factura->id) }}" title="Ver Detalle">
                                                <i class="material-icons">remove_red_eye</i>
                                            </a>
                                            <a href="{{ route('crm.invoices.edit', $factura->id) }}" title="Editar">
                                                <i class="material-icons">mode_edit</i>
                                            </a>                                        

                                            @if($factura->estado->slug !== Constants::NAME_ESTADO_FACTURA_ANULADO)
                                                <a href="{{ route('crm.invoices.cancel', $factura->id) }}" class="btn-request-post" title="Cancelar">
                                                    <i class="material-icons">clear</i>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')

	@parent
	<!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('assets/admin/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/admin/js/pages/tables/jquery-datatable.js') }}"></script>
    
@endsection