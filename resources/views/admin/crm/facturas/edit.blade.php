@extends('admin.template.base')

@section('styles')

    @parent
    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="{{ asset('assets/admin/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
    <style>
        .card .body .col-xs-12 {
            margin-bottom: 10px;
        }
    </style>

@endsection

@section('content')

    @include('admin.errors.errors')

    <div class="row clearfix">
        <div class="col-xs-12">
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home.index') }}">
                        <i class="material-icons">home</i>&nbsp;Inicio
                    </a>
                </li>
                <li>
                    <a href="{{ route('crm.invoices.index') }}">
                        <i class="material-icons">insert_drive_file</i>&nbsp;Facturas
                    </a>
                </li>
                <li>
                    <a>
                        {{  '#' . $factura->id }}
                    </a>
                </li>
                <li class="active">
                    Editar
                </li>
            </ol>
        </div>
    </div>
    <div class="row clearfix">
        @if($factura->estado->slug === Constants::NAME_ESTADO_FACTURA_PENDIENTE)
            <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2 class="text-center">DATOS DE CONTACTO</h2>
                        <h4 class="text-center">Factura N°:&nbsp;{{ $factura->id }}</h4>
                    </div>
                    <div class="body">
                        {!! Form::open(['id' => 'form-approve-invoice', 'url' => route('crm.invoices.approve', $factura->id), 'method' => 'POST', 'files' => true]) !!}
                            <div class="row clearfix">
                                <div class="col-xs-12">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <label class="form-label">Nombre</label>
                                            <input type="text" class="form-control" name="nombre" value="{{ (old('nombre')) ? old('nombre') : $factura->cliente->nombre }}" required="required" tabindex="1">                                        
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <label class="form-label">Apellido</label>
                                            <input type="text" class="form-control" name="apellido" value="{{ (old('apellido')) ? old('apellido') : $factura->cliente->apellido }}" required="required" tabindex="2">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <label class="form-label">RUT</label>
                                            <input type="text" class="form-control" name="rut" value="{{ (old('rut')) ? old('rut') : $factura->cliente->rut }}" required="required" tabindex="3">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <label class="form-label">Teléfono</label>
                                            <input type="text" class="form-control" name="telefono" value="{{ (old('telefono')) ? old('telefono') : $factura->cliente->telefono }}" required="required" tabindex="4">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <label class="form-label">Teléfono Extra</label>
                                            <input type="text" class="form-control" name="telefono_extra" value="{{ (old('telefono_extra')) ? old('telefono_extra') : $factura->cliente->telefono_extra }}" required="required" tabindex="5">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row clearfix">
                                <div class="col-xs-12">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <label class="form-label">Orden de Pago</label>
                                            <input type="text" class="form-control" name="orden" value="{{ (old('orden')) ? old('orden') : $factura->orden }}" disabled="disabled">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <label class="form-label">N° Boleta</label>
                                            <input type="text" class="form-control" name="boleta" value="{{ (old('boleta')) ? old('boleta') : $factura->boleta }}" tabindex="6">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group form-float">
                                        <div class="form-lines">
                                            <label class="form-label">Adjuntar Boleta</label>
                                            <input type="file" class="form-control" name="file" tabindex="7">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <label class="form-label">Tipo de Pago</label>
                                            <select name="tipo_pago" class="form-control" required="required" tabindex="8">
                                                <option value="">Seleccione un tipo</option>
                                                @foreach ($tiposPago as $tipoPago)
                                                    <option value="{{ $tipoPago->id }}" {{ ($tipoPago->id == $factura->factura_tipo_pago_id) ? 'selected' : '' }}>{{ $tipoPago->nombre }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>                        
                            <div class="row">
                                <div class="col-xs-12 col-md-6">
                                    <div class="pull-left">
                                        <a href="{{ route('crm.invoices.cancel', $factura->id) }}" data-toggle="tooltip" class="btn btn-danger btn-request-post" data-original-title="Anular">Anular</a>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-md-6">
                                    <div class="pull-right">
                                        <button type="submit" class="btn btn-primary" data-toggle="tooltip" data-original-title="Aprobar">Aprobar</button>
                                    </div>
                                </div>
                            </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        @elseif($factura->estado->slug !== Constants::NAME_ESTADO_FACTURA_PENDIENTE)
            <div class="col-md-4 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2 class="text-center">DATOS DE CONTACTO</h2>
                        <h4 class="text-center">Factura N°:&nbsp;{{ $factura->id }}</h4>
                    </div>
                    <div class="body">
                        <div class="row">
                            <div class="col-xs-12">
                                <!-- Datos Cliente -->
                                <table class="table table-striped table-hover table-responsive m-t-10">
                                    <tbody>
                                        <tr>
                                            <td class="text-left"><b>Nombre:</b></td>
                                            <td><a href="{{ route('crm.clients.edit', $factura->cliente_id) }}">{{ $factura->cliente->getFullName() }}</a></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"><b>RUT:</b></td>
                                            <td>{{ $factura->cliente->rut }}</td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"><b>Email</b></td>
                                            <td><a href="mailto:{{ $factura->cliente->user->email }}">{{ $factura->cliente->user->email }}</a></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"><b>Teléfono:</b></td>
                                            <td><a href="tel:{{ $factura->cliente->telefono }}">{{ $factura->cliente->telefono }}</a></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"><b>Teléfono Extra:</b></td>
                                            <td><a href="tel:{{ $factura->cliente->telefono_extra }}">{{ $factura->cliente->telefono_extra }}</a></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <hr>
                                <!-- Datos Pago -->
                                <table class="table table-striped table-hover table-responsive m-t-10">
                                    <tbody>
                                        <tr>
                                            <td class="text-left"><b>Orden de Pago:</b></td>
                                            <td>{{ $factura->orden }}</td>
                                        </tr>												
                                        <tr>
                                            <td class="text-left"><b>Tipo de Pago:</b></td>
                                            <td>{{ $factura->tipoPago->nombre }}</td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"><b>Estado:</b></td>
                                            <td>{{ $factura->estado->nombre }}</td>
                                        </tr>
                                        <tr>
                                            <td class="text-left"><b>N° Boleta:</b></td>
                                            <td>{{ $factura->boleta }}</td>
                                        </tr>												
                                        <tr>
                                            <td class="text-left"><b>Adjunto Boleta:</b></td>
                                            <td>
                                                @if($factura->boleta && $factura->path_boleta)
                                                    <a href="{{ asset('storage/boletas/' . $factura->id . '/' . $factura->path_boleta) }}" target="_blank">Ver</a>
                                                @endif
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>                        
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="pull-right">
                                    <a href="{{ route('crm.invoices.cancel', $factura->id) }}" data-toggle="tooltip" class="btn btn-danger btn-request-post" data-original-title="Anular">Anular</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        <div class="col-md-8 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2 class="text-center">DETALLE DE FACTURA</h2>
                </div>
                <div class="body">
                    <div class="row">
                        <div class="col-md-12">
							<div class="invoice-detail">					
								<div class="invoice-item">
									<div class="table-responsive">
										<table class="table table-striped">
											<thead>
												<tr>
													<td class="text-center"><b>Producto</b></td>
													<td class="text-center"><b>Precio</b></td>
													<td class="text-center"><b>Cantidad</b></td>
													<td class="text-right"><b>Total</b></td>
												</tr>
											</thead>
											<tbody>
												@foreach($factura->detalles as $detalle)
													<tr>
														<td class="text-left">{{ $detalle->nombre }}</td>
														<td class="text-right">{{ Plan::formatMoneyValueByCurrencyAndValue($factura->tipoMoneda->slug, $detalle->precio) }}</td>
														<td class="text-center">{{ $detalle->cantidad }}</td>
														<td class="text-right">{{ Plan::formatMoneyValueByCurrencyAndValue($factura->tipoMoneda->slug, $detalle->cantidad * $detalle->precio) }}</td>
													</tr>													
												@endforeach

												<tr>
													<td></td>
													<td></td>
													<td class="text-right"><b>Subtotal:</b></td>
													<td class="text-right"><b>{{ Plan::formatMoneyValueByCurrencyAndValue($factura->tipoMoneda->slug, $factura->subtotal) }}</b></td>
												</tr>

												@if($factura->iva)
													<tr>
														<td></td>
														<td></td>
														<td class="text-right"><b>Iva:</b></td>
														<td class="text-right"><b>{{ Plan::formatMoneyValueByCurrencyAndValue($factura->tipoMoneda->slug, $factura->iva) }}</b></td>
													</tr>
												@endif

												@if($factura->descuento)
													<tr>
														<td></td>
														<td></td>
														<td class="text-right"><b>Descuento:</b></td>
														<td class="text-right"><b>{{ Plan::formatMoneyValueByCurrencyAndValue($factura->tipoMoneda->slug, $factura->descuento) }}</b></td>
													</tr>
												@endif

												<tr>
													<td></td>
													<td></td>
													<td class="text-right"><b>Total:</b></td>
													<td class="text-right"><b>{{ Plan::formatMoneyValueByCurrencyAndValue($factura->tipoMoneda->slug, $factura->total) }}</b></td>
												</tr>												
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div> 
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            &nbsp;
                        </div>
						<div class="col-md-6 transfer-total">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="pull-right">
                                        <h4 class="sub text-right"><b>Total</b></h4>
                                        <div class="price text-right">{{ Plan::formatMoneyValueByCurrencyAndValue($factura->tipoMoneda->slug, $factura->total) }}</div>
                                    </div>
                                </div>
                                <div class="col-xs-12">
                                    <div class="pull-right mb-5">
								        <h4 class="sub text-right"><b>Pagado</b></h4>
								        <div class="price text-right" style="color: #333;">{{ ($factura->estado->slug === Constants::NAME_ESTADO_FACTURA_APROBADO) ? 'Si' : 'No' }}</div>
							        </div>
                                </div>                                
                            </div>
						</div>
					</div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')

    @parent
    
    @if($factura->estado->slug === Constants::NAME_ESTADO_FACTURA_PENDIENTE)
        <!-- Jquery Validation Plugin Css -->
        <script src="{{ asset('assets/admin/plugins/jquery-validation/jquery.validate.js') }}"></script>
        <!-- Moment Plugin Js -->
        <script src="{{ asset('assets/admin/plugins/momentjs/moment.js') }}"></script>
        <!-- Bootstrap Material Datetime Picker Plugin Js -->
        <script src="{{ asset('assets/admin/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
        <script>
            $(function () {

                var region = parseInt("{{ (old('region')) ? old('region') : $factura->cliente->comuna->region_id }}"),
                    comuna = parseInt("{{ (old('comuna')) ? old('comuna') : $factura->cliente->comuna_id }}");

                setTimeout(() => {
                if(region) {
                    loadComunas(region, comuna);
                } 
                }, 800);

                $('#form-approve-invoice').validate({
                    rules: {
                        nombre: "required",
                        apellido: "required",
                        email: "required",
                        rut: {
                            required: true,
                            validateRut: true
                        },
                        telefono: {
                            required: true,
                            minlength: 4,
                        },
                        telefono_extra: {
                            required: false,
                            minlength: 4,
                        }
                    },
                    highlight: function (input) {
                        $(input).parents('.form-line').addClass('error');
                    },
                    unhighlight: function (input) {
                        $(input).parents('.form-line').removeClass('error');
                    },
                    errorPlacement: function (error, element) {
                        $(element).parents('.form-group').append(error);
                    }
                });

                $.validator.addMethod('validateRut', function (value, element) {
                    return validarRut(value);
                }, 'RUT ingresado no es válido');

                /**
                * Cargas las comunas de acuerdo a una region seleccionada
                * 
                * @param   int region
                * @return  void
                */
                function loadComunas(region, comuna = null)
                {
                    var url = route('api.regiones.comunas', region);

                    $.get(url, function(data) {
                            
                        var options = '';
                        options += '<option value=""></option>';

                        $.each(data, function(index, value) {
                            if(value.id == comuna){
                                options += '<option value="' + value.id + '" selected>' + value.nombre + '</option>';
                            }
                            else{
                                options += '<option value="' + value.id + '">' + value.nombre + '</option>';
                            }
                        });

                        $('select[name="comuna"]')
                            .empty()
                            .append(options);

                    }).fail(function(data){
                        console.log(data);
                    });
                }

                /**
                * Validar si un rut ingresado por el usuario es valido
                * 
                * @param    string rut
                * @return  bool
                */
                function validarRut(rut)
                {
                    var rut = rut.split( '-' );
                    
                    if( rut.length < 2 ) return false;
                    
                    var T = rut[ 0 ];
                    var DV1 = rut[ 1 ];
                    var M = 0, S = 1, R = T;
                    
                    for( ; T; T = Math.floor( T / 10 ) )
                        S = ( S + T % 10 * ( 9 - M++ %6 ) ) % 11;
                    
                    var DV2 = S ? S - 1 : 'k';
                    
                    if( DV1 != DV2 ) return false;
                    
                    return true;
                }
            });
        </script>
    @endif
    
@endsection