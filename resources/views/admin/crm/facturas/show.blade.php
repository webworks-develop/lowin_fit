@extends('admin.template.base')

@section('styles')

    @parent
    <style type="text/css">        
	    h5,h3{
	        text-transform: capitalize;
	    }
      	img{
        	max-width: 20%;
      	}
        .fw-600 {
            font-weight: 600 !important;
        }
      	.text-white {
      		color: #fff !important;
      	}
      	.b-t{
          	border-top: 1px solid #ddd;
      	}
      	.bg-light {
    		background-color: #f8f9fa !important;
		}
		.bg-dark {
    		background-color: #333333 !important;
		}
		.mb-2, .my-2 {
    		margin-bottom: .5rem!important;
		}
	    .pt-3, .py-3 {
		    padding-top: 1rem !important;
		}
		.p-4 {
			padding: 1.5rem !important;
		}
	    .pt-4, .py-4 {
    		padding-top: 1.5rem !important;
		}
	    .p-5{
	        padding: 3rem !important;
	    }
	    .pt-5{
	        padding-top: 3rem!important
	    }
        .mt-5{
	        margin-top: 3rem!important
        }
        .container-invoice {
            margin: 20px 0 40px 0;
        }

	    @media(max-width: 768px) {
	        .text-right{
	            text-align: center !important;
	        }
	        .pull-right{
	            float: none;
	            text-align: center;
	        }
	        .center{
	            text-align: center;
	        }
	        .bg-light.p-5:nth-child(1){
	            padding: 1rem !important;
	        }
	        img{
	            max-width: 30%;
	            margin: 0 auto;
	            display: block;
	        }
	        .text-right h5:nth-child(3){
	            padding-top: 15px !important;
	        }
	        .p-5{
	            padding: 1rem !important;
	        }
	        .pt-5{
	            padding-top: 1rem!important
            }
            .mt-5{
	            margin-top: 1rem!important
            }
	    }

        @media print {
            #leftsidebar, #rightsidebar, .breadcrumb, .btn-print-invoice {
                display: none;
            }
            .row {
                width: 100%;
                float: left;
            }
            .content {
                width: 100%;
                left: 0;
                margin: 0 !important;
                float: left !important;

            }
            .col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
                float: left;
            }
            .col-sm-12 {
                 width: 100%;
            }
            .col-sm-11 {
                width: 91.66666667%;
            }
            .col-sm-10 {
                width: 83.33333333%;
            }
            .col-sm-9 {
                width: 75%;
            }
            .col-sm-8 {
                width: 66.66666667%;
            }
            .col-sm-7 {
                width: 58.33333333%;
            }
            .col-sm-6 {
                width: 50%;
            }
            .col-sm-5 {
                width: 41.66666667%;
            }
            .col-sm-4 {
                width: 33.33333333%;
            }
            .col-sm-3 {
                width: 25%;
            }
            .col-sm-2 {
                width: 16.66666667%;
            }
            .col-sm-1 {
                width: 8.33333333%;
            }
        }
    </style>

@endsection

@section('content')

    @include('admin.errors.errors')

    <div class="row clearfix">
        <div class="col-xs-12">
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home.index') }}">
                        <i class="material-icons">home</i>&nbsp;Inicio
                    </a>
                </li>
                <li>
                    <a href="{{ route('crm.invoices.index') }}">
                        <i class="material-icons">insert_drive_file</i>&nbsp;Facturas
                    </a>
                </li>
                <li>
                    <a>
                        {{  '#' . $factura->id }}
                    </a>
                </li>
                <li class="active">
                    Detalle
                </li>
            </ol>
        </div>
    </div>
    <div class="container-invoice">
        <div class="bg-light p-5">            
            <div class="row pt-3 mb-2">
                <div class="col-md-6 pull-left"><img src="http://lowin.fit/prototipo/images/logo@4x.png" class="img-responsive"></div>
                <div class="col-md-6 text-right">
                    <h5 class="pt-4">Orden de Pago #{{ $factura->id }}</h5>
                    <p class="text-muted mb-0"><i>{{ ucwords($factura->fecha->monthName) . ' ' . $factura->fecha->format('d') . ', ' . $factura->fecha->format('Y') }}</i></p>
                </div>
            </div>
            <div class="row b-t pt-5">
                <div class="col-sm-6 pt-3 center">
                    <h5>Información del Cliente</h5>
                    <p>
                        {{ $factura->cliente->getFullName() }}
                        <br>
                        {{ $factura->cliente->rut }}
                        <br>
                        {{ $factura->cliente->telefono }}
                    </p>
                    <p>
                        {{ $factura->cliente->direccion }} 
                        <br>
                        {{ $factura->cliente->comuna->nombre }}
                    </p>
                </div>
                <div class="col-sm-6 text-right">
                    <h5>Detalles del Pago</h5>
                    <p>VAT: 1425782</p>
                    <p>VAT ID: 10253642</p>
                    <p>Tipo de Pago:&nbsp;{{ $factura->tipoPago->nombre }}</p>
                    <br>
                    <h5>Estado del Pago</h5>
                    <p>{{ $factura->estado->nombre }}</p>
                </div>
            </div>
            <h4 class="pt-3">Resumen de Orden</h4>
            <table class="table">
                <tr>
                    <thead>
                        <td class="fw-600">Nombre</td>
                        <td class="fw-600">Cantidad</td>
                        <td class="fw-600">Precio</td>
                        <td class="fw-600">Total</td>
                    </thead>
                </tr>

                @foreach ($factura->detalles as $detalle)
                    <tr>
                        <td>{{ $detalle->nombre }}</td>
						<td>{{ Plan::formatMoneyValueByCurrencyAndValue($factura->tipoMoneda->slug, $detalle->precio) }}</td>
						<td>{{ $detalle->cantidad }}</td>
						<td>{{ Plan::formatMoneyValueByCurrencyAndValue($factura->tipoMoneda->slug, $detalle->cantidad * $detalle->precio) }}</td>
                    </tr>    
                @endforeach
            </table>
        </div>
        <div class="bg-dark text-white p-5">
            <div class="row">
                <div class="col-sm-4 text-left">
                    <button type="button" class="btn bg-teal btn-print-invoice waves-effect mt-5" onclick="window.print();">
                        <i class="material-icons">print</i>
                        <span>IMPRIMIR</span>
                    </button>
                </div>
                <div class="col-sm-3 text-right">
                    <h6>Sub - Total</h6>
                    <h3>{{ Plan::formatMoneyValueByCurrencyAndValue($factura->tipoMoneda->slug, $factura->subtotal) }}</h3>
                </div>
                <div class="col-sm-2 text-right">
                    <h6>Descuento</h6>
                    <h3>{{ Plan::formatMoneyValueByCurrencyAndValue($factura->tipoMoneda->slug, $factura->descuento) }}</h3>
                </div>
                <div class="col-sm-3 text-right">
                    <h6>Total</h6>
                    <h3>{{ Plan::formatMoneyValueByCurrencyAndValue($factura->tipoMoneda->slug, $factura->total) }}</h3>
                </div>
            </div>
        </div>
    </div>

@endsection