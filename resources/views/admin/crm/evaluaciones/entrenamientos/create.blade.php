@extends('admin.template.base')

@section('styles')

    @parent
    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="{{ asset('assets/admin/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />
    <!--WaitMe Css-->
    <link href="{{ asset('assets/admin/plugins/waitme/waitMe.css') }}" rel="stylesheet" />

    <style>
        .card-items-evaluation > .body {
            max-height: 100vh;
            overflow-y: scroll;
        }
        .card.card-question {
            margin-bottom: 20px !important;
        }
        .card.card-question .btn-delete-question {
            cursor: pointer;
        }
        .card-question .form-group {
            margin-bottom: 0 !important;
        }
    </style>

@endsection

@section('content')

    @include('admin.errors.errors')

    <div class="row clearfix">
        <div class="col-xs-12">
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home.index') }}">
                        <i class="material-icons">home</i>&nbsp;Inicio
                    </a>
                </li>
                <li>
                    <a href="{{ route('crm.clients.index') }}">
                        <i class="material-icons">work</i>&nbsp;Clientes
                    </a>
                </li>
                <li>
                    <a href="{{ route('crm.clients.show', $cliente->id) }}">
                        {{ $cliente->getFullName() }}
                    </a>
                </li>
                <li>
                    <a href="{{ route('crm.clients.evaluations.trainers.index', $cliente->id) }}">
                        Evaluaciones Entrenamientos
                    </a>
                </li>
                <li class="active">
                    Nuevo
                </li>
            </ol>
        </div>
    </div>

    {!! Form::open(['id' => 'form-create-trainer-evaluation', 'url' => route('crm.clients.evaluations.trainers.store', $cliente->id), 'method' => 'POST']) !!}
        <div class="row clearfix">
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>DATOS INFORMATIVOS</h2>
                    </div>
                    <div class="body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Código</label>
                                        <input type="text" class="form-control" name="codigo" value="{{ $codigo }}" required="required" readonly="readonly">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="text" class="form-control datepicker" name="fecha" value="{{ old('fecha') }}" placeholder="Fecha" required="required" tabindex="1">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Estado</label>
                                        <select name="estado" class="form-control" required="required" tabindex="2">
                                            <option value="{{ Constants::CODE_ESTADO_EVALUACION_PENDIENTE }}" {{ (old('estado') === Constants::CODE_ESTADO_EVALUACION_PENDIENTE) ? 'selected' : null }}>Pendiente</option>
                                            <option value="{{ Constants::CODE_ESTADO_EVALUACION_COMPLETADO }}" {{ (old('estado') === Constants::CODE_ESTADO_EVALUACION_COMPLETADO) ? 'selected' : null }}>Completado</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <button class="btn btn-primary waves-effect pull-right" type="submit">Guardar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="card card-items-evaluation">
                    <div class="header clearfix">
                        <div class="pull-left">
                            <h2 style="margin-top: 10px;">ITEMS DE EVALUACIÓN</h2>
                        </div>
                        <div class="pull-right">
                            <a class="btn btn-sm btn-info btn-add-question">
                                <i class="material-icons">add</i>
                            </a>
                        </div>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-xs-12">
                                <div class="box-evaluation-questions"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}

@endsection

@section('scripts')

	@parent
	<!-- Jquery Validation Plugin Css -->
    <script src="{{ asset('assets/admin/plugins/jquery-validation/jquery.validate.js') }}"></script>
    <!-- Moment Plugin Js -->
    <script src="{{ asset('assets/admin/plugins/momentjs/moment-with-locale.js') }}"></script>
    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="{{ asset('assets/admin/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
    <!-- Wait Me Plugin Js -->
    <script src="{{ asset('assets/admin/plugins/waitme/waitMe.js') }}"></script>
    <script src="{{ asset('assets/admin/js/pages/cards/basic.js') }}"></script>
    <script>
        $(function () {

            $('#form-create-trainer-evaluation').validate({
                rules: {
                    codigo: "required",
                    fecha: "required",
                    estado: "required",
                },
                highlight: function (input) {
                    $(input).parents('.form-line').addClass('error');
                },
                unhighlight: function (input) {
                    $(input).parents('.form-line').removeClass('error');
                },
                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error);
                }
            });

            $('.datepicker').bootstrapMaterialDatePicker({
                format: 'DD-MM-YYYY',
                lang : 'es',
                cancelText: 'Cancelar',
                clearText: 'Limpiar',
                clearButton: true,
                weekStart: 1,
                time: false
            });

            $(document).on('click', '.btn-add-question', function(event) {
                event.preventDefault();
                $('.box-evaluation-questions').append(makeEvaluationQuestionHTML());
            });

            $(document).on('click', '.btn-delete-question', function(event) {
                event.preventDefault();
                $(this).parents('.card-question').remove();
            });

            function makeEvaluationQuestionHTML()
            {
                var html = '';

                html += '<div class="card card-question">';
                html += '    <div class="header">';
                html += '        <h5 class="m-b-20">Escriba una Pregunta ....</h5>';
                html += '        <div class="form-group form-float">';
                html += '            <div class="form-line">';
                html += '                <input type="text" name="questions[]" class="form-control" placeholder="¿Cuantas Calorias ... ?">';
                html += '            </div>';
                html += '        </div>';
                html += '        <ul class="header-dropdown m-r--5">';
                html += '            <li>';
                html += '                <a class="btn-delete-question" title="Eliminar Item">';
                html += '                    <i class="material-icons">delete</i>';
                html += '                </a>';
                html += '            </li>';
                html += '        </ul>';
                html += '    </div>';
                html += '    <div class="body">';
                html += '        <div class="form-group form-float">';
                html += '            <div class="form-line">';
                html += '                <textarea name="answers[]" class="form-control no-resize" placeholder="Escriba una respuesta ...."></textarea>';
                html += '            </div>';
                html += '        </div>';
                html += '    </div>';
                html += '</div>';

                return html;
            }
        });
    </script>
    
@endsection