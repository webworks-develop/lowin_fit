@extends('admin.template.base')

@section('content')

    <div class="row clearfix">
        <div class="col-xs-12">
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home.index') }}">
                        <i class="material-icons">home</i>&nbsp;Inicio
                    </a>
                </li>
                <li>
                    <i class="material-icons">work</i>&nbsp;Clientes
                </li>
                <li>
                    <a>
                        {{ $cliente->getFullName() }}
                    </a>
                </li>
                <li class="active">
                    Evaluaciones Entrenamientos
                </li>
            </ol>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        {{ strtoupper($cliente->getFullName()) . ' - EVALUACIONES ENTRENAMIENTOS' }}
					</h2>
					<ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="{{ route('crm.clients.evaluations.trainers.create', $cliente->id) }}">Agregar Evaluación</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation">
                            <a href="{{ route('crm.clients.show', $cliente->id) }}" aria-expanded="false">
                                <i class="material-icons">account_circle</i>&nbsp;PERFIL
                            </a>
                        </li>
                        <li role="presentation" class="active">
                            <a href="{{ route('crm.clients.evaluations.trainers.index', $cliente->id) }}" aria-expanded="false">
                                <i class="material-icons">assignment</i>&nbsp;EVALUACION ENTRENAMIENTO
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="{{ route('crm.clients.evaluations.nutritionists.index', $cliente->id) }}" aria-expanded="false">
                                <i class="material-icons">assignment</i>&nbsp;EVALUACION NUTRICIONAL
                            </a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade active in" id="fit">                            									
							@if($cliente->evaluacionesCliente->isNotEmpty())
								<div class="m-t-20">
									<div class="panel-group" id="{{ 'accordion_' . $cliente->id }}" role="tablist" aria-multiselectable="true">										
										@foreach($cliente->evaluacionesCliente as $evaluacion)
											<div class="panel panel-primary">
												<div class="panel-heading" role="tab" id="{{ 'headingOne_' . $evaluacion->id }}">
													<h4 class="panel-title">
														<a role="button" data-toggle="collapse" data-parent="{{ 'accordion_' . $cliente->id }}" href="{{ '#collapseOne_' . $evaluacion->id }}" aria-expanded="false" aria-controls="{{ 'collapseOne_' . $evaluacion->id }}" class="collapsed">
															<div class="row box-panel-title">
                                                                <div class="col-xs-12" style="margin-bottom: 0;">
                                                                    <div class="pull-left">
                                                                        {{ 'Evaluación #' . $evaluacion->id . ' | ' . $evaluacion->fecha->format('d') . ' ' . ucwords($evaluacion->fecha->monthName) . ' ' . $evaluacion->fecha->format('Y') }}
                                                                    </div>
                                                                    <div class="pull-right">
                                                                        <i class="material-icons btn-edit-evaluation" data-link="{{ route('crm.clients.evaluations.trainers.edit', [$cliente->id, $evaluacion->id]) }}">mode_edit</i>
                                                                        <i class="material-icons btn-delete-evaluation" data-link="{{ route('crm.clients.evaluations.trainers.delete', [$cliente->id, $evaluacion->id]) }}">delete_forever</i>
                                                                    </div>
                                                                </div>
                                                            </div>
														</a>
													</h4>
												</div>
												<div id="{{ 'collapseOne_' . $evaluacion->id }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="{{ 'headingOne_' . $evaluacion->id }}" aria-expanded="false" style="height: 0px;">
													<div class="panel-body">
														<div id="wizard_horizontal">
															@foreach($evaluacion->itemsEvaluacion as $item)
																<h2>{{ $item->pregunta }}</h2>
																<section>
																	<p class="text-justify">
																		{{ $item->respuesta }}
																	</p>
																</section>
															@endforeach
														</div>
													</div>
												</div>
											</div>
										@endforeach
									</div>
								</div>
							@endif
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')

	@parent
	<!-- Jquery Validation Plugin Css -->
    <script src="{{ asset('assets/admin/plugins/jquery-validation/jquery.validate.js') }}"></script>
	<!-- JQuery Steps Plugin Js -->	
	<script src="{{ asset('assets/admin/plugins/jquery-steps/jquery.steps.js') }}"></script>
	<!-- Custom Js -->
    <script src="{{ asset('assets/admin/js/pages/forms/form-wizard.js') }}"></script>
    <script>
        $(function () {
            $(document).on('click', '.btn-edit-evaluation', function(event) {
                event.preventDefault();
                event.stopPropagation();
                $(location).attr('href', $(this).attr('data-link'));
            });

            $(document).on('click', '.btn-delete-evaluation', function(event) {
                event.preventDefault();
                event.stopPropagation();
                console.log('delete evaluation');
            });
        });
    </script>
	
@endsection