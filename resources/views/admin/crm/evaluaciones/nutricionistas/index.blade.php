@extends('admin.template.base')

@section('content')

    <div class="row clearfix">
        <div class="col-xs-12">
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home.index') }}">
                        <i class="material-icons">home</i>&nbsp;Inicio
                    </a>
                </li>
                <li>
                    <i class="material-icons">work</i>&nbsp;Clientes
                </li>
                <li>
                    <a>
                        {{ $cliente->getFullName() }}
                    </a>
                </li>
                <li class="active">
                    Ficha
                </li>
            </ol>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        {{ strtoupper($cliente->getFullName()) . ' - FICHA' }}
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="{{ route('crm.clients.evaluations.nutritionists.create', $cliente->id) }}">Agregar Evaluación</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation">
                            <a href="{{ route('crm.clients.show', $cliente->id) }}" aria-expanded="false">
                                <i class="material-icons">account_circle</i>&nbsp;PERFIL
                            </a>
                        </li>
                        <li role="presentation">
                            <a href="{{ route('crm.clients.evaluations.trainers.index', $cliente->id) }}" aria-expanded="false">
                                <i class="material-icons">assignment</i>&nbsp;EVALUACION ENTRENAMIENTO
                            </a>
                        </li>
                        <li role="presentation" class="active">
                            <a href="{{ route('crm.clients.evaluations.nutritionists.index', $cliente->id) }}" aria-expanded="false">
                                <i class="material-icons">assignment</i>&nbsp;EVALUACION NUTRICIONAL
                            </a>
                        </li>
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content">                       
                        <div role="tabpanel" class="tab-pane fade active in" id="nut">
                            <b>Message Content</b>
                            <p>
                                Lorem ipsum dolor sit amet, ut duo atqui exerci dicunt, ius impedit mediocritatem an. Pri ut tation electram moderatius.
                                Per te suavitate democritum. Duis nemore probatus ne quo, ad liber essent aliquid
                                pro. Et eos nusquam accumsan, vide mentitum fabellas ne est, eu munere gubergren
                                sadipscing mel.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection