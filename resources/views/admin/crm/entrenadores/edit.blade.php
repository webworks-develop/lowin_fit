@extends('admin.template.base')

@section('styles')

    @parent
    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="{{ asset('assets/admin/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css') }}" rel="stylesheet" />

@endsection

@section('content')

    @include('admin.errors.errors')

    <div class="row clearfix">
        <div class="col-xs-12">
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home.index') }}">
                        <i class="material-icons">home</i>&nbsp;Inicio
                    </a>
                </li>
                <li>
                    <a href="{{ route('crm.trainers.index') }}">
                        <i class="material-icons">fitness_center</i>&nbsp;Entrenadores
                    </a>
                </li>
                <li>
                    <a>
                        {{  '#' . $entrenador->id }}
                    </a>
                </li>
                <li class="active">
                    Editar
                </li>
            </ol>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>EDITAR ENTRENADOR {{ '#' . $entrenador->id }}</h2>
                </div>
                <div class="body">
                    {!! Form::open(['id' => 'form-edit-trainer', 'url' => route('crm.trainers.update', $entrenador->id), 'method' => 'POST']) !!}
                        
                        <div class="row clearfix">
                            <div class="col-md-4">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Nombre</label>
                                        <input type="text" class="form-control" name="nombre" value="{{ (old('nombre')) ? old('nombre') : $entrenador->nombre }}" required="required" tabindex="1">                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Apellido</label>
                                        <input type="text" class="form-control" name="apellido" value="{{ (old('apellido')) ? old('apellido') : $entrenador->apellido }}" required="required" tabindex="2">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">RUT</label>
                                        <input type="text" class="form-control" name="rut" value="{{ (old('rut')) ? old('rut') : $entrenador->rut }}" required="required" tabindex="3">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-md-3">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Teléfono</label>
                                        <input type="text" class="form-control" name="telefono" value="{{ (old('telefono')) ? old('telefono') : $entrenador->telefono }}" required="required" tabindex="4">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Fecha Nacimiento</label>
                                        <input type="text" class="form-control datepicker" name="fecha_nacimiento" value="{{ (old('fecha_nacimiento')) ? old('fecha_nacimiento') : date('d-m-Y', strtotime($entrenador->fecha_nacimiento)) }}" required="required" tabindex="5">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Experiencia</label>
                                        <input type="number" class="form-control" name="experiencia" value="{{ (old('experiencia')) ? old('experiencia') : $entrenador->experiencia }}" required="required" tabindex="6">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Especialidad</label>
                                        <input type="number" class="form-control" name="especialidad" value="{{ (old('especialidad')) ? old('especialidad') : $entrenador->especialidad }}" required="required" tabindex="7">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-md-4">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Teléfono Extra</label>
                                        <input type="text" class="form-control" name="telefono_extra" value="{{ (old('telefono_extra')) ? old('telefono_extra') : $entrenador->telefono_extra }}" required="required" tabindex="8">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Región</label>
                                        <select name="region" class="form-control" required="required" tabindex="9">
                                            <option value=""></option>
                                            @foreach($regiones as $region)
                                                @if(old('region'))
                                                    <option value="{{ $region->id }}" {{ (old('region') == $region->id) ? 'selected' : null }}>{{ $region->nombre }}</option>
                                                @else
                                                    <option value="{{ $region->id }}" {{ ($entrenador->comuna->region_id == $region->id) ? 'selected' : null }}>{{ $region->nombre }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Comuna</label>
                                        <select name="comuna" class="form-control" required="required" tabindex="10"></select>
                                    </div>
                                </div>
                            </div>                        
                        </div>
                        <div class="row clearfix">
                            <div class="col-md-6">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Dirección</label>
                                        <textarea name="direccion" class="form-control no-resize" required="required" tabindex="11">{{ (old('direccion')) ? old('direccion') : $entrenador->direccion }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Observaciones</label>
                                        <textarea name="observaciones" class="form-control no-resize" tabindex="12">{{ (old('observaciones')) ? old('observaciones') : $entrenador->observaciones }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <button class="btn btn-primary waves-effect pull-right" type="submit">Guardar</button>
                            </div>
                        </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')

	@parent
	<!-- Jquery Validation Plugin Css -->
    <script src="{{ asset('assets/admin/plugins/jquery-validation/jquery.validate.js') }}"></script>
    <!-- Moment Plugin Js -->
    <script src="{{ asset('assets/admin/plugins/momentjs/moment-with-locale.js') }}"></script>
    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="{{ asset('assets/admin/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') }}"></script>
    <script>
        $(function () {

            var region = parseInt("{{ (old('region')) ? old('region') : $entrenador->comuna->region_id }}"),
                comuna = parseInt("{{ (old('comuna')) ? old('comuna') : $entrenador->comuna_id }}");

            setTimeout(() => {
               if(region) {
                   loadComunas(region, comuna);
               } 
            }, 800);

            $('#form-edit-trainer').validate({
                rules: {
                    nombre: "required",
                    apellido: "required",
                    email: "required",
                    rut: {
						required: true,
						validateRut: true
					},
					telefono: {
						required: true,
						minlength: 4,
                    },
                    telefono_extra: {
						required: false,
						minlength: 4,
                    },
                    fecha_nacimiento: "required",
                    experiencia: {
						required: true,
						minlength: 4,
                    },
                    especialidad: {
						required: true,
						minlength: 4,
                    },
                    email: "required",
                    region: {
				      	required: true,
				      	digits: true
				    },
				    comuna: {
				      	required: true,
				      	digits: true
				    },
					direccion: "required",
					observaciones: {
						required: false,
						minlength: 1,
					}                  
                },
                highlight: function (input) {
                    $(input).parents('.form-line').addClass('error');
                },
                unhighlight: function (input) {
                    $(input).parents('.form-line').removeClass('error');
                },
                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error);
                }
            });

            $.validator.addMethod('validateRut', function (value, element) {
    			return validarRut(value);
            }, 'RUT ingresado no es válido');

            $('.datepicker').bootstrapMaterialDatePicker({
                format: 'DD-MM-YYYY',
                lang : 'es',
                cancelText: 'Cancelar',
                clearText: 'Limpiar',
                clearButton: true,
                weekStart: 1,
                time: false
            });

            $('select[name="region"]').on('click', function(event) {
                if($(this).find('option:selected').val()) {
                    loadComunas($(this).find('option:selected').val());
                }
            })

            /**
			 * Cargas las comunas de acuerdo a una region seleccionada
			 * 
			 * @param   int region
			 * @return  void
			 */
            function loadComunas(region, comuna = null)
            {
				var url = route('api.regiones.comunas', region);

                $.get(url, function(data) {
                        
                    var options = '';
                    options += '<option value=""></option>';

                    $.each(data, function(index, value) {
                        if(value.id == comuna){
                            options += '<option value="' + value.id + '" selected>' + value.nombre + '</option>';
                        }
                        else{
                            options += '<option value="' + value.id + '">' + value.nombre + '</option>';
                        }
                    });

                    $('select[name="comuna"]')
                        .empty()
                        .append(options);

                }).fail(function(data){
                    console.log(data);
                });
            }

            /**
			 * Validar si un rut ingresado por el usuario es valido
			 * 
			 * @param    string rut
			 * @return  bool
			 */
			function validarRut(rut)
			{
				var rut = rut.split( '-' );
				
				if( rut.length < 2 ) return false;
				
				var T = rut[ 0 ];
				var DV1 = rut[ 1 ];
				var M = 0, S = 1, R = T;
				
				for( ; T; T = Math.floor( T / 10 ) )
					S = ( S + T % 10 * ( 9 - M++ %6 ) ) % 11;
				
				var DV2 = S ? S - 1 : 'k';
				
				if( DV1 != DV2 ) return false;
				
				return true;
			}
        });
    </script>
    
@endsection