@extends('admin.template.base')

@section('styles')

	@parent
	<!-- JQuery DataTable Css -->
    <link href="{{ asset('assets/admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet" />    
	
@endsection

@section('content')

    <div class="row clearfix">
        <div class="col-xs-12">
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home.index') }}">
                        <i class="material-icons">home</i>&nbsp;Inicio
                    </a>
                </li>
                <li class="active">
                    <i class="material-icons">fitness_center</i>&nbsp;Entrenadores
                </li>
            </ol>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        LISTADO DE ENTRENADORES
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="{{ route('crm.trainers.create') }}">Agregar Entrenador</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                            <thead>
                                <tr>
                                    <th>Nombre Completo</th>
                                    <th>Email</th>
                                    <th>Teléfono</th>
                                    <th>RUT</th>
                                    <th>Estado</th>
                                    <th>Creado</th>
                                    <th>Modificado</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Nombre Completo</th>
                                    <th>Email</th>
                                    <th>Teléfono</th>
                                    <th>RUT</th>
                                    <th>Estado</th>
                                    <th>Creado</th>
                                    <th>Modificado</th>
                                    <th>Acciones</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                @foreach($entrenadores as $entrenador)
                                    <tr>
                                        <td><a href="{{ route('crm.trainers.edit', $entrenador->id) }}">{{ $entrenador->getFullName() }}</a></td>
                                        <td>
                                            @if(!empty($entrenador->user))
                                                <a href="mailto:{{ $entrenador->user->email }}">{{ $entrenador->user->email }}</a>
                                            @else
                                                &nbsp;
                                            @endif                                            
                                        </td>
                                        <td><a href="tel:{{ $entrenador->telefono }}">{{ $entrenador->telefono }}</a></td>
                                        <td>{{ $entrenador->rut }}</td>
                                        <td>{{ $entrenador->getStatus() }}</td>
                                        <td>{{ $entrenador->created_at->diffForHumans() }}</td>
                                        <td>{{ $entrenador->updated_at->diffForHumans() }}</td>
                                        <td>
                                            <a href="{{ route('crm.trainers.edit', $entrenador->id) }}" title="Editar">
                                                <i class="material-icons">mode_edit</i>
                                            </a>
                                            
                                            <a href="{{ route('crm.trainers.delete', $entrenador->id) }}" class="btn-request-post" title="Eliminar">
                                                <i class="material-icons">delete_forever</i>
                                            </a>

                                            @if($entrenador->estado)
                                                <a href="{{ route('crm.trainers.suspend', $entrenador->id) }}" class="btn-request-post" title="Suspender">
                                                    <i class="material-icons">visibility_off</i>
                                                </a>
                                            @else
                                                <a href="{{ route('crm.trainers.activate', $entrenador->id) }}" class="btn-request-post" title="Activar">                                                    
                                                    <i class="material-icons">restore</i>
                                                </a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')

	@parent
	<!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('assets/admin/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/admin/js/pages/tables/jquery-datatable.js') }}"></script>
    
@endsection