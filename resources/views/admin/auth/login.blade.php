@extends('admin.template.auth.base')

@section('content')

    <form method="POST" class="m-t-10" action="{{ route('login') }}">

        @csrf

        <div class="content-errors px-3">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        {{ $error }}
                    @endforeach
                </div>
            @endif
        </div>

        <div class="input-group">
            <span class="input-group-addon">
                <i class="material-icons">email</i>
            </span>
            <div class="form-line">
                <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="email" value="{{ old('email') }}" required="required" autocomplete="email" autofocus="autofocus">
            </div>
        </div>
        <div class="input-group">
            <span class="input-group-addon">
                <i class="material-icons">lock</i>
            </span>
            <div class="form-line">
                <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="password" required="required" autocomplete="current-password">
            </div>
        </div>
        <div class="row">
            <div class="col-xs-8 p-t-5">
                <input type="checkbox" name="remember" id="remember" class="filled-in chk-col-pink" {{ old('remember') ? 'checked' : '' }}>
                <label for="remember">Recordar</label>
            </div>
            <div class="col-xs-4">
                <button type="submit" class="btn btn-block bg-pink waves-effect">Entrar</button>
            </div>
        </div>
        <div class="row m-t-15 m-b--20">
            <div class="col-xs-6">
                &nbsp;
            </div>

            @if (Route::has('password.request'))
                <div class="col-xs-6 align-right">
                    <a href="{{ route('password.request') }}">Olvidó su Contraseña?</a>
                </div>
            @endif
        </div>
    </form>

@endsection