@extends('admin.template.auth.base')

@section('content')

    <form method="POST" class="m-t-10" action="{{ route('password.email') }}">

        @csrf

        <div class="content-errors px-3">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @elseif(!empty($errors) && $errors->has('email'))
                <div class="alert alert-danger" role="alert">
                    {{ $errors->first('email') }}
                </div>
            @endif
        </div>
        
        <div class="msg">
            Ingrese su dirección de correo electrónico que utilizó para registrarse. Le enviaremos un correo electrónico con su nombre de usuario y un enlace para restablecer su contraseña.
        </div>
        <div class="input-group">
            <span class="input-group-addon">
                <i class="material-icons">email</i>
            </span>
            <div class="form-line">
                <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" id="email" value="{{ old('email') }}" placeholder="Ingrese su Email" required="required" autocomplete="email" autofocus="autofocus">
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <button type="submit" class="btn btn-block bg-pink waves-effect">Enviar Solicitud de Reestablecimiento</button>
            </div>
        </div>
    </form>

@endsection