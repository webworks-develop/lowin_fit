@extends('admin.template.base')

@section('content')

    @include('admin.errors.errors')

    <div class="row clearfix">
        <div class="col-xs-12">
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home.index') }}">
                        <i class="material-icons">home</i>&nbsp;Inicio
                    </a>
                </li>
                <li>
                    <a href="{{ route('configuration.plans.index') }}">
                        <i class="material-icons">card_membership</i>&nbsp;Planes
                    </a>
                </li>
                <li>
                    <a>
                        {{  '#' . $plan->id }}
                    </a>
                </li>
                <li class="active">
                    Editar
                </li>
            </ol>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>EDITAR PLAN {{ '#' . $plan->id }}</h2>
                </div>
                <div class="body">
                    {!! Form::open(['id' => 'form-edit-plan', 'url' => route('configuration.plans.update', $plan->id), 'method' => 'POST']) !!}
                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Nombre</label>
                                        <input type="text" name="name" class="form-control" value="{{ (old('name')) ? old('name') : $plan->name }}" tabindex="1">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Límite de usuarios activos</label>
                                        <input type="number" name="active_subscribers_limit" class="form-control" value="{{ (old('active_subscribers_limit')) ? old('active_subscribers_limit') : $plan->active_subscribers_limit }}" tabindex="2">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Intervalo del Plan</label>
                                        <select name="invoice_interval" class="form-control" required="required" tabindex="3">
									        <option value=""></option>                                            
                                            @foreach ($arrayListIntervals as $key => $interval)
                                                @if(old('invoice_interval'))
												    <option value="{{ $key }}" {{ (old('invoice_interval') === $key) ? 'selected' : null }}>{{ $interval }}</option>
											    @else
												    <option value="{{ $key }}" {{ ($plan->invoice_interval === $key) ? 'selected' : null }}>{{ $interval }}</option>
											    @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Cantidad de Intervalo del Plan</label>
                                        <input type="number" name="invoice_period" class="form-control" value="{{ (old('invoice_period')) ? old('invoice_period') : $plan->invoice_period }}" tabindex="4">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Tipo Moneda</label>
                                        <select name="currency" class="form-control" tabindex="5">
									        <option value=""></option>                                            
                                            @foreach ($arraylistCurrency as $key => $currency)
										        @if(old('currency'))
												    <option value="{{ $key }}" {{ (old('currency') === $key) ? 'selected' : null }}>{{ $currency }}</option>
											    @else
    												<option value="{{ $key }}" {{ ($plan->currency === $key) ? 'selected' : null }}>{{ $currency }}</option>
											    @endif
									        @endforeach
								        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Precio</label>
                                        <input type="text" name="price" class="form-control" value="{{ Plan::formatMoneyValueByCurrencyAndValue((old('currency')) ? old('currency') : $plan->currency, (old('price')) ? old('price') : $plan->price) }}" required="required" tabindex="6">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Intervalo Tiempo de Prueba</label>
                                        <select name="trial_interval" class="form-control" tabindex="7">
									        <option value=""></option>                                            
                                            @foreach ($arrayListIntervals as $key => $interval)
                                                @if(old('trial_interval'))
												    <option value="{{ $key }}" {{ (old('trial_interval') === $key) ? 'selected' : null }}>{{ $interval }}</option>
											    @else
    												<option value="{{ $key }}" {{ ($plan->trial_interval === $key) ? 'selected' : null }}>{{ $interval }}</option>
											    @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Cantidad de Intervalo de Prueba</label>
                                        <input type="number" name="trial_period" class="form-control" value="{{ (old('trial_period')) ? old('trial_period') : $plan->trial_period }}" tabindex="8">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">                        
                            <div class="col-xs-12">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Descripción</label>
                                        <textarea name="description" class="form-control" rows="3" required="required" tabindex="9">{{ (old('description')) ? old('description') : $plan->description }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <button class="btn btn-primary waves-effect pull-right" type="submit">Guardar</button>
                            </div>
                        </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')

	@parent
	<!-- Jquery Validation Plugin Css -->
    <script src="{{ asset('assets/admin/plugins/jquery-validation/jquery.validate.js') }}"></script>
    <!-- jQuery MaskMoney -->
	<script src="{{ asset('assets/admin/plugins/jquery-maskMoney/jquery.maskMoney.js') }}"></script>
    <script>
        $(function () {

            setTimeout(() => {
				reviewPlansCurrencySelected( $('select[name="currency"] option:selected').val() );
			}, 500);

            $('#form-edit-plan').validate({
                rules: {
                    name: "required",
                    currency: "required",
                    price: "required",
					descripcion: "required",
					invoice_period: "required",
                    trial_period: {
				      	digits: true
					},
					active_subscribers_limit: {
				      	digits: true
				    }
                },
                highlight: function (input) {
                    $(input).parents('.form-line').addClass('error');
                },
                unhighlight: function (input) {
                    $(input).parents('.form-line').removeClass('error');
                },
                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error);
                }
            });

            $('select[name="currency"]').on('change', function(event) {
				event.preventDefault();
				reviewPlansCurrencySelected($(this).val());
			});	

            function reviewPlansCurrencySelected(currencyValue)
			{
				var $form = $('#form-edit-plan');

				if(!currencyValue) {
					$form.find('input[name="price"], input[name="signup_fee"]').val('');
					$form.find('input[name="price"], input[name="signup_fee"]').removeClass('mask-money-usd').removeClass('mask-money-clp').attr('disabled', 'disabled');
				} else {
					if(currencyValue === 'CLP') {
						$form.find('input[name="price"], input[name="signup_fee"]').removeClass('mask-money-usd').addClass('mask-money-clp').removeAttr('disabled');

						$('.mask-money-clp').maskMoney({
							prefix: '$',
							thousands: '.',
							decimal: ',',
							allowZero: true,
							precision: 0
						});

					} else if(currencyValue === 'USD') {
						$form.find('input[name="price"], input[name="signup_fee"]').removeClass('mask-money-clp').addClass('mask-money-usd').removeAttr('disabled');

						$('.mask-money-usd').maskMoney({
							prefix: '$',
							thousands: ',',
							decimal: '.',
							allowZero: true,
							precision: 2
						});
					}
				}
			}
        });
    </script>
    
@endsection