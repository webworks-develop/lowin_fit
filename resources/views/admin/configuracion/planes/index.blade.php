@extends('admin.template.base')

@section('styles')

	@parent
	<!-- JQuery DataTable Css -->
    <link href="{{ asset('assets/admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet" />    
	
@endsection

@section('content')

    <div class="row clearfix">
        <div class="col-xs-12">
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home.index') }}">
                        <i class="material-icons">home</i>&nbsp;Inicio
                    </a>
                </li>
                <li class="active">
                    <i class="material-icons">card_membership</i>&nbsp;Planes
                </li>
            </ol>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        LISTADO DE PLANES
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <li class="dropdown">
                            <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="{{ route('configuration.plans.create') }}">Agregar Plan</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nombre</th>
                                    <th>Moneda</th>
                                    <th>Precio</th>
                                    <th>Intervalo</th>
                                    <th>Estado</th>
                                    <th>Creado</th>
                                    <th>Actualizado</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>ID</th>
                                    <th>Nombre</th>
                                    <th>Moneda</th>
                                    <th>Precio</th>
                                    <th>Intervalo</th>
                                    <th>Estado</th>
                                    <th>Creado</th>
                                    <th>Actualizado</th>
                                    <th>Acciones</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                @foreach($planes as $plan)
                                    <tr>
                                        <td>{{ $plan->id }}</td>
                                        <td>{{ $plan->name }}</td>
                                        <td>{{ $plan->currency }}</td>
                                        <td>{{ Plan::formatMoneyValueByCurrencyAndValue($plan->currency, $plan->price) }}</td>
                                        <td>{{ Plan::getNameInterval($plan->invoice_interval) }}</td>
                                        <td>{{ Plan::getFullStatus($plan->is_active) }}</td>
                                        <td>{{ $plan->created_at->diffForHumans() }}</td>
                                        <td>{{ $plan->updated_at->diffForHumans() }}</td>
                                        <td>
                                            <a href="{{ route('configuration.plans.edit', $plan->id) }}" title="Editar">
                                                <i class="material-icons">mode_edit</i>
                                            </a>
                                            <a href="{{ route('configuration.plans.delete', $plan->id) }}" class="btn-request-post" title="Eliminar">
                                                <i class="material-icons">delete_forever</i>
                                            </a>

                                            @if($plan->is_active)
                                                <a href="{{ route('configuration.plans.suspend', $plan->id) }}" class="btn-request-post" title="Suspender">
                                                    <i class="material-icons">visibility_off</i>
                                                </a>
                                            @else
                                                <a href="{{ route('configuration.plans.activate', $plan->id) }}" class="btn-request-post" title="Activar">
                                                    <i class="material-icons">restore</i>
                                                </a>
                                            @endif                                            
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')

	@parent
	<!-- Jquery DataTable Plugin Js -->
    <script src="{{ asset('assets/admin/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/admin/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/admin/js/pages/tables/jquery-datatable.js') }}"></script>
    
@endsection