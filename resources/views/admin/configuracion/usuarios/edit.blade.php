@extends('admin.template.base')

@section('content')

    @include('admin.errors.errors')

    <div class="row clearfix">
        <div class="col-xs-12">
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home.index') }}">
                        <i class="material-icons">home</i>&nbsp;Inicio
                    </a>
                </li>
                <li>
                    <a href="{{ route('configuration.users.index') }}">
                        <i class="material-icons">supervisor_account</i>&nbsp;Usuarios
                    </a>
                </li>
                <li>
                    <a>
                        {{  '#' . $user->id }}
                    </a>
                </li>
                <li class="active">
                    Editar
                </li>
            </ol>
        </div>
    </div>
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>EDITAR USUARIO {{ '#' . $user->id }}</h2>
                </div>
                <div class="body">
                    {!! Form::open(['id' => 'form-edit-user', 'url' => route('configuration.users.update', $user->id), 'method' => 'POST']) !!}
                        
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Nombre</label>
                                        <input type="text" class="form-control" name="name" value="{{ (old('name')) ? old('name') : $user->name }}" tabindex="1">                                        
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Email</label>
                                        <input type="text" class="form-control" name="email" value="{{ (old('name')) ? old('email') : $user->email }}" required="required" tabindex="2">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <label class="form-label">Rol</label>
                                        <select id="role" name="role" class="form-control" required="required" tabindex="3">
                                            <option value="">&nbsp;Seleccione un rol</option>
                                            @foreach($roles as $role)
                                                @if(old('role'))
                                                    <option value="{{ $role->name }}" {{ (old('role') === $role->name) ? 'selected' : '' }}>{{ $role->display_name }}</option>
                                                @else
                                                    <option value="{{ $role->name }}" {{ ($roleUser->name === $role->name) ? 'selected' : '' }}>{{ $role->display_name }}</option>
                                                @endif
                                            @endforeach
                                        </select>                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="password" name="password" class="form-control" id="password" value="{{ old('password') }}" tabindex="4">
                                        <label class="form-label">Contraseña</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" value="{{ old('password_confirmation') }}" tabindex="5">
                                        <label class="form-label">Confirmar Contraseña</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <button class="btn btn-primary waves-effect pull-right" type="submit">Guardar</button>
                            </div>
                        </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')

	@parent
	<!-- Jquery Validation Plugin Css -->
    <script src="{{ asset('assets/admin/plugins/jquery-validation/jquery.validate.js') }}"></script>
    <script>
        $(function () {
            $('#form-create-user').validate({
                rules: {
                    role: "required",
					email: {
						required: true,
						email: true
					},
					password: {
						minlength: 4
					},
					password_confirmation: {
						minlength: 4,
						equalTo: "#password"
					}
                },
                highlight: function (input) {
                    $(input).parents('.form-line').addClass('error');
                },
                unhighlight: function (input) {
                    $(input).parents('.form-line').removeClass('error');
                },
                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error);
                }
            });
        });
    </script>
    
@endsection