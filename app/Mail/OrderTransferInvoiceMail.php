<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderTransferInvoiceMail extends Mailable
{
    use Queueable, SerializesModels;

    public $appSettings, $pedido;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($appSettings, $pedido)
    {
        $this->appSettings = $appSettings;
        $this->pedido = $pedido;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('admin.emails.order-transfer-invoice')->subject('Resumen de Compra #' . $this->pedido->id)->with([
            'appSettings' => $this->appSettings,
            'pedido'      => $this->pedido,
        ]);
    }
}
