<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ApproveInvoiceMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $invoice;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($invoice)
    {
        $this->invoice = $invoice;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email = $this->view('admin.emails.invoices.approve')->with(['invoice' => $this->invoice]);
        $email->subject('Confirmación de Orden de Pago #' . $this->invoice->id);

        if ($this->invoice->path_boleta && file_exists(public_path('storage/boletas/' . $this->invoice->id . '/' . $this->invoice->path_boleta))) {
            $email->attach(public_path('storage/boletas/' . $this->invoice->id . '/' . $this->invoice->path_boleta));
        }
        
        return $email;
    }
}
