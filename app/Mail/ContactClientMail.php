<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactClientMail extends Mailable
{
    use Queueable, SerializesModels;

    public $client, $subject, $message, $filename;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($client, $subject, $message, $filename)
    {
        $this->client = $client;
        $this->subject = $subject;
        $this->message = $message;
        $this->filename = $filename;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $email = $this->view('admin.emails.clients.contact')->with(['client' => $this->client, 'mensaje' => $this->message]);
        $email->subject(($this->subject) ? $this->subject : 'Tienes un nuevo mensaje:');

        if($this->filename && file_exists(public_path('storage/temp/' . $this->filename))) {
            $email->attach(public_path('storage/temp/' . $this->filename));
        }
    }
}
