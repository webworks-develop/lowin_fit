<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Globals;

/**
 * Description of Constantes
 *
 */
final class Constants 
{	
    /**
     * General
     */
    const STATUS_ACTIVE = 1;
    const NAME_STATUS_ACTIVE = 'Activo';
    const NAME_STATUS_INACTIVE = 'Inactivo';
    const LIMIT_REGISTROS_TABLE = 10;
    const LIMIT_LATEST_REGISTROS_TABLE = 5;

    const LIMIT_LIST_PRODUCTS = 15;
    const LIMIT_CHARACTERS_TITLE_FEATURED_PRODUCT = 45;
    const LIMIT_CHARACTERS_DESCRIPTION_FEATURED_PRODUCT = 75;
    const LIMIT_CHARACTERS_DESCRIPTION_PRODUCT = 300;

    const NAME_DEFAULT_COMPANY = 'base';
    const PATH_USER_DEFAULT_IMAGE_PROFILE = 'assets/admin/img/profile.jpg';

    const REGION_ID_RM = '13';
    const VALUE_DISPATCH_RM = 0;
    const VALUE_VAT_PERCENT = 19;

    const MAX_COUNT_DAYS_NEW_PRODUCT = 3;

    /**
     * Version
     */
    const VERSION_SYSTEM = '1.0.0';

    /**
     * TRANSBANK
     */
    const NAME_WEBPAY_TRANSACTION_NORMAL = 'TR_NORMAL';
    const CODE_RESPONSE_APPROVED = 0;
    const CODE_RESPONSE_REJECTED = -1;

    /**
     * Roles
     */
    const NAME_ROLE_SUPERADMINISTRADOR = 'superadministrador';
    const NAME_ROLE_ADMINISTRADOR = 'administrador';
    const NAME_ROLE_ENTRENADOR = 'entrenador';
    const NAME_ROLE_NUTRICIONISTA = 'nutricionista';
    const NAME_ROLE_CLIENTE = 'cliente';

    /**
     * Estado Factura
     */
    const NAME_ESTADO_FACTURA_PENDIENTE = 'pendiente';
    const NAME_ESTADO_FACTURA_APROBADO = 'aprobado';
    const NAME_ESTADO_FACTURA_ANULADO = 'anulado';

    /**
     * Tipos de Pagos Facturas
     */
    const NAME_PAYMENT_TYPE_TRANSFER = 'transferencia';
    const NAME_PAYMENT_TYPE_WEBPAY = 'webpay';

    /**
     * Estados Clientes
     */
    const NAME_ESTADO_CLIENTE_PENDIENTE = 'pendiente';
    const NAME_ESTADO_CLIENTE_ACTIVO = 'activo';
    const NAME_ESTADO_CLIENTE_VENCIDO = 'vencido';
    const NAME_ESTADO_CLIENTE_ANULADO = 'anulado';
    const NAME_ESTADO_CLIENTE_SUSPENDIDO = 'suspendido';

    /**
     * Estados Evaluaciones
     */
    const CODE_ESTADO_EVALUACION_PENDIENTE = 0;
    const CODE_ESTADO_EVALUACION_COMPLETADO = 1;

    /**
     * Informes
     */
    const NAME_REPORT_VENTAS_TRANSBANK = 'venta-transbank';
    const NAME_REPORT_CLIENTES_SUSCRIPTORES = 'cliente-suscriptores';
}