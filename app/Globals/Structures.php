<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Globals;

final class Structures
{	
	/**
	 * Dias de la semana
	 * @var  array
	 */
	public static $dias = [
		'0' => 'Domingo',
		'1' => 'Lunes',
		'2' => 'Martes',
		'3' => 'Miercoles',
		'4' => 'Jueves',
		'5' => 'Viernes',
		'6' => 'Sabado'
	];

	/**
	 * Meses del ano
	 * @var array
	 */
	public static $meses = [
		'01' => 'Enero',
		'02' => 'Febrero',
		'03' => 'Marzo',
		'04' => 'Abril',
		'05' => 'Mayo',
		'06' => 'Junio',
		'07' => 'Julio',
		'08' => 'Agosto',
		'09' => 'Septiembre',
		'10' => 'Octubre',
		'11' => 'Noviembre',
		'12' => 'Diciembre',
	];

	public static $mesesc = [
		'1' => 'Enero',
		'2' => 'Febrero',
		'3' => 'Marzo',
		'4' => 'Abril',
		'5' => 'Mayo',
		'6' => 'Junio',
		'7' => 'Julio',
		'8' => 'Agosto',
		'9' => 'Septiembre',
		'10' => 'Octubre',
		'11' => 'Noviembre',
		'12' => 'Diciembre',
	];
	
    /**
     * Lista de sexo
     * @var array
     */
    public static $sexo = [
        'M' => 'Hombre',
        'F' => 'Mujer'
    ];
    
    /**
     * Lista de tipos de pago Transbank
     * @var array
     */
    public static $tipoPagoTbk = [
    	'VN' => 'Venta Normal', 
    	'VC' => 'Cuotas Normales', 
    	'SI' => 'Cuotas sin Interés', 
    	'CI' => 'Crédito', 
    	'VD' => 'Venta Débito',
        'NC' => 'N Cuotas'
    ];

    /**
     * Lista de tipos de cuotas Transbank
     * @var array
     */
    public static $tipoCuotaTbk = [
    	'VN' => 'Sin Cuotas', 
    	'VC' => 'Cuotas normales', 
		'SI' => 'Sin Interés',
		'S2' => 'Sin Interés', 
    	'CI' => 'Cuotas Comercio', 
    	'VD' => 'Venta Débito',
        'NC' => 'Sin Interés'
   	];

   	public static $codigoRespuestaTbk = [
   		'0'  => 'Transacción aprobada',
   		'-1' => 'Rechazo de transacción',
   		'-2' => 'Transacción debe reitentarse',
   		'-3' => 'Error en transacción',
   		'-4' => 'Rechazo de transacción',
   		'-5' => 'Rechazo por error de tasa',
   		'-6' => 'Excede cupo máximo mensual',
   		'-7' => 'Excede límite diario por transacción',
   		'-8' => 'Rubro no autorizado',
   	];

   	public static $codigoRespuestaVCI = [
   		''     => '',
   		'TSY'  => 'Autenticación exitosa',
   		'TSN'  => 'Autenticación fallida',
   		'TO'   => 'Tiempo máximo excedido para autenticación',
   		'ABO'  => 'Autenticación abortada por tarjetahabiente',
   		'U3'   => 'Error interno en la autenticación',
   		'NP'   => 'No Participa, probablemente por ser una tarjeta extranjera que no participa en el programa 3DSecure',
   		'ACS2' => 'Autenticación fallida extranjera',
   		'A'    => 'Intento de autenticación',
   		'INV'  => 'Autenticación inválida',
   		'EOP'  => 'Error Operativo'
   	];
}