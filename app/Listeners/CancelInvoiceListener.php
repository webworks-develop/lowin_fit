<?php

namespace App\Listeners;

use App\Events\CancelInvoiceEvent;
use App\Mail\CancelInvoiceMail;
use App\Repositories\Facturas\FacturasRepoI;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class CancelInvoiceListener
{
    protected $facturasRepo;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(FacturasRepoI $facturasRepo)
    {
        $this->facturasRepo = $facturasRepo;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(CancelInvoiceEvent $event)
    {
        $invoice = $this->facturasRepo->getFullInvoiceById($event->invoice);

        if(!empty($invoice)) {
            Mail::to('jesusdavid1004@gmail.com')->send(new CancelInvoiceMail($invoice));
        }
    }
}
