<?php

namespace App\Listeners;

use App\Events\RegisteredClientEvent;
use App\Mail\RegisteredClientMail;
use App\Repositories\Clientes\ClientesRepoI;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class RegisteredClientListener
{
    protected $clientesRepo;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(ClientesRepoI $clientesRepo)
    {
        $this->clientesRepo = $clientesRepo;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(RegisteredClientEvent $event)
    {
        $client = $this->clientesRepo->with(['user'])->findOneBy(['id' => $event->client]);

        if (!empty($client)) {
            Mail::to('jesusdavid1004@gmail.com')->send(new RegisteredClientMail($client));
        }
    }
}
