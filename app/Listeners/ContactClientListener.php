<?php

namespace App\Listeners;

use App\Events\ContactClientEvent;
use App\Mail\ContactClientMail;
use App\Repositories\Clientes\ClientesRepoI;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class ContactClientListener
{
    protected $clientsRepo;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(ClientesRepoI $clientsRepo)
    {
        $this->clientsRepo = $clientsRepo;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(ContactClientEvent $event)
    {
        $client = $this->clientsRepo->with(['user'])->findOneBy(['id' => $event->client]);

        if(!empty($client)) {
            Mail::to($client->user->email)->send(new ContactClientMail($client, $event->subject, $event->message, $event->filename));
        }
    }
}