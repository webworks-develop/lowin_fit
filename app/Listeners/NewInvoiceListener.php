<?php

namespace App\Listeners;

use App\Events\NewInvoiceEvent;
use App\Mail\NewInvoiceMail;
use App\Repositories\Facturas\FacturasRepoI;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class NewInvoiceListener
{
    protected $facturasRepo;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(FacturasRepoI $facturasRepo)
    {
        $this->facturasRepo = $facturasRepo;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(NewInvoiceEvent $event)
    {
        $invoice = $this->facturasRepo->getFullInvoiceById($event->invoice);

        if (!empty($invoice)) {
            Mail::to($invoice->cliente->user->email)->send(new NewInvoiceMail($invoice));
        }
    }
}
