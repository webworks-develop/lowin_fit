<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class FacturaTipoPago extends Model
{
    use Sluggable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'factura_tipo_pago';

    protected $fillable = [
        'slug', 'nombre', 'estado'
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'nombre'
            ]
        ];
    }

    public function facturas()
    {
        return $this->hasMany(\App\Models\Factura::class, 'factura_tipo_pago_id');
    }
}
