<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Entrenador extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'entrenador';

    protected $fillable = [
        'codigo', 'nombre', 'apellido', 'rut', 'telefono', 'telefono_extra', 'fecha_nacimiento', 'especialidad', 'experiencia', 'comuna_id', 'direccion', 'user_id', 'estado', 'observaciones'
    ];

    protected $dates = [
        'fecha_nacimiento', 'created_at', 'updated_at', 'deleted_at'
    ];

    public static function boot()
    {
        parent::boot();

        static::deleted(function ($model) {
            //Delete User
            $model->user->delete();
        });
    }

    public function comuna()
    {
        return $this->belongsTo(\App\Models\Comuna::class, 'comuna_id');
    }

    public function user()
    {
        return $this->belongsTo(\App\User::class, 'user_id');
    }

    public function getFullName()
    {
        return $this->nombre . ' ' . $this->apellido;
    }

    public function getStatus()
    {
        return ($this->estado) ? 'Activo' : 'Inactivo';
    }
}
