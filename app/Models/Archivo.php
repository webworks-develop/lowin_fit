<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Archivo extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'archivo';

    protected $fillable = [
        'titulo', 'nombre', 'nombre_original', 'extension'
    ];

    protected $dates = [
        'created_at', 'updated_at', 'deleted_at'
    ];
}
