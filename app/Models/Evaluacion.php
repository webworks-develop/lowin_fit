<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Evaluacion extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'evaluacion';

    protected $fillable = [
        'codigo', 'fecha', 'cliente_id', 'estado'
    ];

    protected $dates = [
        'fecha', 'created_at', 'updated_at', 'deleted_at'
    ];

    public function cliente()
    {
        return $this->belongsTo(\App\Models\Cliente::class, 'cliente_id');
    }

    public function itemsEvaluacion()
    {
        return $this->hasMany(\App\Models\EvaluacionItem::class, 'evaluacion_id');
    }

    public function getStatus()
    {
        return ($this->estado) ? 'Activo' : 'Inactivo';
    }
}
