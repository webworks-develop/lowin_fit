<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EvaluacionItem extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'evaluacion_item';

    protected $fillable = [
        'evaluacion_id', 'orden', 'pregunta', 'respuesta', 'estado'
    ];

    protected $dates = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    public function evaluacion()
    {
        return $this->belongsTo(\App\Models\Evaluacion::class, 'evaluacion_id');
    }

    public function getStatus()
    {
        return ($this->estado) ? 'Activo' : 'Inactivo';
    }
}
