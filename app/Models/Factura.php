<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Factura extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'factura';

    protected $fillable = [
        'orden', 'boleta', 'fecha', 'fecha_vencimiento', 'tipo_moneda_id', 'subtotal', 'porcentaje_iva', 'iva', 'descuento', 'total',
        'cliente_id', 'factura_estado_id', 'factura_tipo_pago_id', 'path_boleta', 'observaciones'
    ];

    protected $dates = [
        'fecha', 'fecha_vencimiento', 'created_at', 'updated_at', 'deleted_at'
    ];

    public function tipoMoneda()
    {
        return $this->belongsTo(\App\Models\TipoMoneda::class, 'tipo_moneda_id');
    }

    public function cliente()
    {
        return $this->belongsTo(\App\Models\Cliente::class, 'cliente_id');
    }

    public function estado()
    {
        return $this->belongsTo(\App\Models\FacturaEstado::class, 'factura_estado_id');
    }

    public function tipoPago()
    {
        return $this->belongsTo(\App\Models\FacturaTipoPago::class, 'factura_tipo_pago_id');
    }

    public function detalles()
    {
        return $this->hasMany(\App\Models\FacturaDetalle::class, 'factura_id');
    }
}
