<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EvaluacionNutricionista extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'evaluacion_nutricionista';

    protected $fillable = [
        'evaluacion_id', 'nutricionista_id'
    ];

    public function evaluacion()
    {
        return $this->belongsTo(\App\Models\Evaluacion::class, 'evaluacion_id');
    }

    public function nutricionista()
    {
        return $this->belongsTo(\App\Models\Nutricionista::class, 'nutricionista_id');
    }
}
