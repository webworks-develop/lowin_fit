<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TipoMoneda extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tipo_moneda';

    protected $fillable = [
        'slug', 'nombre', 'abreviatura', 'estado'
    ];

    protected $dates = [
        'created_at', 'updated_at', 'deleted_at'
    ];

    public function tiposMonedaProducto()
    {
        return $this->hasMany(\App\Models\Producto::class, 'producto_id');
    }
}
