<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClienteSuscriptor extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cliente_suscriptor';

    protected $fillable = [
        'email'
    ];

    protected $dates = [
        'created_at', 'updated_at'
    ];
}
