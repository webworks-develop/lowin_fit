<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cliente extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cliente';

    protected $fillable = [
        'codigo', 'nombre', 'apellido', 'rut', 'telefono', 'telefono_extra', 'fecha_nacimiento', 
        'peso', 'altura', 'direccion', 'comuna_id', 'cliente_estado_id', 'user_id', 'observaciones'
    ];

    protected $dates = [
        'fecha_nacimiento', 'created_at', 'updated_at', 'deleted_at'
    ];

    public static function boot()
    {
        parent::boot();

        static::deleted(function ($model) {
            //Cancel Suscription
            $model->user->subscription('main')->cancel();

            //Delete User
            $model->user->delete();
        });
    }

    public function comuna()
    {
        return $this->belongsTo(\App\Models\Comuna::class, 'comuna_id');
    }

    public function user()
    {
        return $this->belongsTo(\App\User::class, 'user_id');
    }

    public function estado()
    {
        return $this->belongsTo(\App\Models\ClienteEstado::class, 'cliente_estado_id');
    }

    public function facturasCliente()
    {
        return $this->hasMany(\App\Models\Factura::class, 'cliente_id');
    }

    public function evaluacionesCliente()
    {
        return $this->hasMany(\App\Models\Evaluacion::class, 'cliente_id');
    }

    public function getFullName()
    {
        return $this->nombre . ' ' . $this->apellido;
    }
}
