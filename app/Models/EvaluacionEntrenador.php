<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EvaluacionEntrenador extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'evaluacion_entrenador';

    protected $fillable = [
        'evaluacion_id', 'entrenador_id'
    ];

    public function evaluacion()
    {
        return $this->belongsTo(\App\Models\Evaluacion::class, 'evaluacion_id');
    }

    public function entrenador()
    {
        return $this->belongsTo(\App\Models\Entrenador::class, 'entrenador_id');
    }
}
