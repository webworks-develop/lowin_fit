<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FacturaDetalle extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'factura_detalle';

    protected $fillable = [
        'factura_id', 'plan_id', 'nombre', 'cantidad', 'precio'
    ];

    public function factura()
    {
        return $this->belongsTo(\App\Models\Factura::class, 'factura_id');
    }

    public function plan()
    {
        return $this->belongsTo(app('rinvex.subscriptions.plan'), 'plan_id');
    }
}
