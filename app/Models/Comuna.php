<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Comuna extends Model
{
    use Sluggable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'comuna';

    protected $fillable = [
        'slug', 'nombre', 'region_id'
    ];

    protected $hidden = ['pivot'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'nombre'
            ]
        ];
    }

    public function region()
    {
        return $this->belongsTo(\App\Models\Region::class);
    }
}
