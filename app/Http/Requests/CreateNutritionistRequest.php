<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateNutritionistRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre'           => 'required|string',
            'apellido'         => 'required|string',
            'rut'              => 'required|string|unique:nutricionista',
            'telefono'         => 'required|alpha_num',
            'fecha_nacimiento' => 'required|date',
            'grado_academico'  => 'required|string',
            'especialidad'     => 'required|string',
            'telefono_extra'   => 'nullable|alpha_num',
            'comuna'           => 'required|integer',
            'direccion'        => 'required|string',
            'observaciones'    => 'nullable|string',

            'email'            => 'required|email|unique:users',
            'password'         => 'required|min:4|confirmed',
            'confirm_password' => 'min:4',
        ];
    }
}
