<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre'           => 'required|string',
            'apellido'         => 'required|string',
            'rut'              => 'required|string|unique:cliente',
            'telefono'         => 'required|alpha_num',
            'fecha_nacimiento' => 'required|date',
            'peso'             => 'required|numeric',
            'altura'           => 'required|numeric',
            'telefono_extra'   => 'nullable|alpha_num',
            'comuna'           => 'required|integer',
            'direccion'        => 'required|string',
            'observaciones'    => 'nullable|string',

            'email'            => 'required|email|unique:users',
            'password'         => 'required|min:4|confirmed',
            'confirm_password' => 'min:4',

            'plan'             => 'required|string'
        ];
    }
}
