<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $clientId = intval($this->segment(2));

        return [
            'nombre'           => 'required|string',
            'apellido'         => 'required|string',
            'rut'              => 'required|string|unique:cliente,id,' . $clientId,
            'telefono'         => 'required|alpha_num',
            'telefono_extra'   => 'nullable|alpha_num',
            'fecha_nacimiento' => 'required|date',
            'peso'             => 'required|numeric',
            'altura'           => 'required|numeric',
            'comuna'           => 'required|integer',
            'direccion'        => 'required|string',
            'observaciones'    => 'nullable|string'
        ];
    }
}
