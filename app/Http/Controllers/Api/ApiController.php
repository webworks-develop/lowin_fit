<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\Comunas\ComunasRepoI;
use App\Repositories\Regiones\RegionesRepoI;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    protected $comunasRepo, $regionesRepo;

    public function __construct(ComunasRepoI $comunasRepo, RegionesRepoI $regionesRepo)
    {
        $this->comunasRepo = $comunasRepo;
        $this->regionesRepo = $regionesRepo;        
    }

    public function getRegions()
    {
        $regions = $this->regionesRepo->getRegionsList();

        return response()->json($regions);
    }

    public function getRegionsDispatch()
    {
        $regions = $this->regionesRepo->getRegionsDispatchList();

        return response()->json($regions);
    }

    public function getComunasByRegion($region)
    {
        $comunas = $this->comunasRepo->getComunasListByRegion($region);

        return response()->json($comunas);
    }
}
