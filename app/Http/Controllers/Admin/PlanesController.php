<?php

namespace App\Http\Controllers\Admin;

use App\Globals\Alerts;
use App\Helpers\Plan;
use App\Http\Controllers\Controller;
use App\Repositories\Planes\PlanesRepoI;
use Illuminate\Http\Request;

class PlanesController extends Controller
{
    protected $planesRepo;

    public function __construct(PlanesRepoI $planesRepo)
    {
        $this->planesRepo = $planesRepo;
    }

    public function index()
    {
        $planes = $this->planesRepo->getAllPlans();

        return view('admin.configuracion.planes.index', [
            'planes' => $planes
        ]);
    }

    public function create()
    {
        $arraylistCurrency = Plan::getListPlansCurrency();
        $arrayListIntervals = Plan::getListPlansIntervals();

        return view('admin.configuracion.planes.create', [
            'arraylistCurrency'  => $arraylistCurrency,
            'arrayListIntervals' => $arrayListIntervals
        ]);
    }

    public function store(Request $request)
    {
        $data = $request->except(['_token']);

        if ($data['currency'] === Plan::CURRENCY_CLP) {
            $data['price'] = Plan::formatCurrencyCLPtoNumberToSqlStandard($data['price']);
        } else if ($data['currency'] === Plan::CURRENCY_USD) {
            $data['price'] = Plan::formatCurrencyUSDtoNumberToSqlStandard($data['price']);
        }

        $data['signup_fee'] = ($request->get('signup_fee')) ? $request->get('signup_fee') : 0;
        $data['trial_interval'] = ($request->get('trial_interval')) ? $request->get('trial_interval') : '';
        $data['trial_period'] = ($request->get('trial_period')) ? $request->get('trial_period') : 0;

        $this->planesRepo->createPlan($data);

        return redirect()->route('configuration.plans.index')
            ->with('message', Alerts::CREATE_SUCCESS)
            ->with('messageType', 'success');
    }

    public function edit($id)
    {
        $plan = $this->planesRepo->findOneBy(['id' => $id]);

        $arraylistCurrency = Plan::getListPlansCurrency();
        $arrayListIntervals = Plan::getListPlansIntervals();

        return view('admin.configuracion.planes.edit', [
            'plan'               => $plan,
            'arraylistCurrency'  => $arraylistCurrency,
            'arrayListIntervals' => $arrayListIntervals
        ]);
    }

    public function update($id, Request $request)
    {
        $data = $request->except(['_token', 'publications', 'images', 'resources', 'featured', 'instagram']);
        $dataPlanFeatures = $request->only(['publications', 'images', 'resources', 'featured', 'instagram']);

        if ($data['currency'] === Plan::CURRENCY_CLP) {
            $data['price'] = Plan::formatCurrencyCLPtoNumberToSqlStandard($data['price']);
        } else if ($data['currency'] === Plan::CURRENCY_USD) {
            $data['price'] = Plan::formatCurrencyUSDtoNumberToSqlStandard($data['price']);
        }

        $data['signup_fee'] = ($request->get('signup_fee')) ? $request->get('signup_fee') : 0;
        $data['trial_interval'] = ($request->get('trial_interval')) ? $request->get('trial_interval') : '';
        $data['trial_period'] = ($request->get('trial_period')) ? $request->get('trial_period') : 0;

        $this->planesRepo->editPlan($id, $data);

        return redirect()->route('configuration.plans.index')
            ->with('message', Alerts::UPDATE_SUCCESS)
            ->with('messageType', 'success');
    }

    public function delete($id, Request $request)
    {
        $this->planesRepo->deleteBy(['id' => $id]);

        return redirect()->back()
            ->with('message', Alerts::DELETE_SUCCESS)
            ->with('messageType', 'success');
    }

    public function suspend($id, Request $request)
    {
        $this->planesRepo->suspend($id);

        return redirect()->back()
            ->with('message', Alerts::SUSPEND_SUCCESS)
            ->with('messageType', 'success');
    }

    public function activate($id, Request $request)
    {
        $this->planesRepo->activate($id);

        return redirect()->back()
            ->with('message', Alerts::ACTIVATE_SUCCESS)
            ->with('messageType', 'success');
    }
}
