<?php

namespace App\Http\Controllers\Admin;

use Exception;
use App\Globals\Alerts;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\EditUserRequest;
use App\Repositories\Roles\RolesRepoI;
use App\Repositories\Usuarios\UsuariosRepoI;
use Illuminate\Http\Request;

class UsuariosController extends Controller
{
    protected $rolesRepo, $usuariosRepo;

    public function __construct(RolesRepoI $rolesRepo, UsuariosRepoI $usuariosRepo)
    {
        $this->rolesRepo = $rolesRepo;
        $this->usuariosRepo = $usuariosRepo;
    }

    public function index()
    {   
        $users = $this->usuariosRepo->getAllUsers();

        return view('admin.configuracion.usuarios.index', [
            'users' => $users
        ]);
    }

    public function create()
    {
        $roles = $this->rolesRepo->getRolesList();

        return view('admin.configuracion.usuarios.create', [
            'roles' => $roles
        ]);
    }

    public function store(CreateUserRequest $request)
    {
        try {

            $data = [
                'name'     => $request->name,
                'email'    => $request->email,
                'password' => bcrypt($request->password),
            ];

            $this->usuariosRepo->createUser($data, $request->role);

            return redirect()->route('configuration.users.index')
                ->with('message', Alerts::CREATE_SUCCESS)
                ->with('messageType', 'success');

        } catch (Exception $e) {
            throw $e;
        }
    }

    public function edit($id)
    {
        $roles = $this->rolesRepo->getRolesList();
        $user = $this->usuariosRepo->findOneBy(['id' => $id]);
        $roleUser = $user->roles->first();

        return view('admin.configuracion.usuarios.edit', [
            'roles'    => $roles,
            'roleUser' => $roleUser,
            'user'     => $user
        ]);
    }

    public function update($id, EditUserRequest $request)
    {
        try {
            $data = [
                'name'  => $request->name,
                'email' => $request->email,
            ];

            if ($request->password) {
                $data['password'] = bcrypt($request->password);
            }

            $this->usuariosRepo->updateUser($id, $data, $request->role);

            return redirect()->route('configuration.users.index')
                ->with('message', Alerts::UPDATE_SUCCESS)
                ->with('messageType', 'success');

        } catch (Exception $e) {
            throw $e;
        }
    }

    public function delete($id, Request $request)
    {
        $this->usuariosRepo->deleteBy(['id' => $id]);

        return redirect()->back()
            ->with('message', Alerts::DELETE_SUCCESS)
            ->with('messageType', 'success');
    }

    public function suspend($id, Request $request)
    {
        $this->usuariosRepo->suspend($id);

        return redirect()->back()
            ->with('message', Alerts::SUSPEND_SUCCESS)
            ->with('messageType', 'success');
    }

    public function activate($id, Request $request)
    {
        $this->usuariosRepo->activate($id);

        return redirect()->back()
            ->with('message', Alerts::ACTIVATE_SUCCESS)
            ->with('messageType', 'success');
    }
}
