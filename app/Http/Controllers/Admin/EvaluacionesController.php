<?php

namespace App\Http\Controllers\Admin;

use App\Globals\Alerts;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateEvaluationNutritionistRequest;
use App\Http\Requests\CreateEvaluationTrainerRequest;
use App\Http\Requests\EditEvaluationTrainerRequest;
use App\Repositories\Clientes\ClientesRepoI;
use App\Repositories\Evaluaciones\EvaluacionesRepoI;
use Illuminate\Http\Request;

class EvaluacionesController extends Controller
{
    protected $clientesRepo, $evaluacionesRepo;

    public function __construct(ClientesRepoI $clientesRepo, EvaluacionesRepoI $evaluacionesRepo)
    {
        $this->clientesRepo = $clientesRepo;
        $this->evaluacionesRepo = $evaluacionesRepo;
    }

    public function trainers($clientId)
    {
        $cliente = $this->clientesRepo->with(['evaluacionesCliente', 'evaluacionesCliente.itemsEvaluacion'])->findOneBy(['id' => $clientId]);

        return view('admin.crm.evaluaciones.entrenamientos.index', [
            'cliente' => $cliente
        ]);
    }

    public function createTrainers($clientId)
    {
        $codigo = $this->evaluacionesRepo->getUniqueEvaluationCode();
        $cliente = $this->clientesRepo->findOneBy(['id' => $clientId]);

        return view('admin.crm.evaluaciones.entrenamientos.create', [
            'cliente' => $cliente,
            'codigo'  => $codigo
        ]);
    }

    public function storeTrainers($clientId, CreateEvaluationTrainerRequest $request)
    {
        $data = $request->except(['_token', 'questions', 'answers']);
        $dataItems = [];

        $data['cliente_id'] = $clientId;
        $data['fecha'] = date('Y-m-d', strtotime($data['fecha']));

        /** Set Data Evaluation Items */
        foreach ($request->post('questions') as $key => $question) {
            $dataItems[] = [
                'orden'     => ($key + 1),
                'pregunta'  => $question,
                'respuesta' => $request->post('answers')[$key]
            ];
        }

        $this->evaluacionesRepo->createEvaluationTrainers($data, $dataItems);

        return redirect()->route('crm.clients.evaluations.trainers.index', $clientId)   
            ->with('message', Alerts::CREATE_SUCCESS)
            ->with('messageType', 'success');        
    }

    public function editTrainers($clientId, $id)
    {
        $evaluacion = $this->evaluacionesRepo->with(['cliente', 'itemsEvaluacion'])->findOneBy(['id' => $id]);

        return view('admin.crm.evaluaciones.entrenamientos.edit', [
            'evaluacion' => $evaluacion,
        ]);
    }

    public function updateTrainers($clientId, $id, EditEvaluationTrainerRequest $request)
    {
        $data = $request->except(['_token', 'codigo', 'questions', 'answers']);
        $data['fecha'] = date('Y-m-d', strtotime($data['fecha']));

        $this->evaluacionesRepo->updateBy(['id' => $id], $data);

        return redirect()->route('crm.clients.evaluations.trainers.index', $clientId)
            ->with('message', Alerts::UPDATE_SUCCESS)
            ->with('messageType', 'success');
    }

    public function deleteTrainers($clientId, $id, Request $request)
    {
        //$this->evaluacionesRepo->createEvaluationTrainers($data, $dataItems);

        return redirect()->route('crm.clients.evaluations.trainers.index', $clientId)
            ->with('message', Alerts::DELETE_SUCCESS)
            ->with('messageType', 'success');
    }

    public function nutritionist($clientId)
    {
        $cliente = $this->clientesRepo->findOneBy(['id' => $clientId]);

        return view('admin.crm.evaluaciones.nutricionistas.index', [
            'cliente' => $cliente
        ]);
    }

    public function storeNutritionists($clientId, CreateEvaluationNutritionistRequest $request)
    {
        $data = $request->except(['_token', 'questions', 'answers']);
        $dataItems = [];

        $data['cliente_id'] = $clientId;
        $data['fecha'] = date('Y-m-d', strtotime($data['fecha']));

        /** Set Data Evaluation Items */
        foreach ($request->post('questions') as $key => $question) {
            $dataItems[] = [
                'orden'     => ($key + 1),
                'pregunta'  => $question,
                'respuesta' => $request->post('answers')[$key]
            ];
        }

        $this->evaluacionesRepo->createEvaluationTrainers($data, $dataItems);

        return redirect()->route('crm.clients.evaluations.trainers.index', $clientId)
            ->with('message', Alerts::CREATE_SUCCESS)
            ->with('messageType', 'success');
    }

    public function updateItem($clientId, $evaluacionId, $id, Request $request)
    {
        return response()->json(true);
    }
}
