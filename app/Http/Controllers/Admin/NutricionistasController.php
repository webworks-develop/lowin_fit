<?php

namespace App\Http\Controllers\Admin;

use App\Globals\Alerts;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateNutritionistRequest;
use App\Http\Requests\EditNutritionistRequest;
use App\Repositories\Nutricionistas\NutricionistasRepoI;
use App\Repositories\Regiones\RegionesRepoI;
use Illuminate\Http\Request;

class NutricionistasController extends Controller
{
    protected $nutricionistasRepo, $regionesRepo;

    public function __construct(NutricionistasRepoI $nutricionistasRepo, RegionesRepoI $regionesRepo)
    {
        $this->nutricionistasRepo = $nutricionistasRepo;
        $this->regionesRepo = $regionesRepo;
    }

    public function index()
    {
        $nutricionistas = $this->nutricionistasRepo->getAllNutritionists();

        return view('admin.crm.nutricionistas.index', [
            'nutricionistas' => $nutricionistas
        ]);
    }

    public function create()
    {
        $regiones = $this->regionesRepo->getRegionsList();

        return view('admin.crm.nutricionistas.create', [
            'regiones' => $regiones
        ]);
    }

    public function store(CreateNutritionistRequest $request)
    {
        $data = $request->except(['_token', 'region', 'comuna', 'email', 'password', 'password_confirmation']);
        $dataUser = $request->only(['email', 'password']);

        //Format fields
        $data['fecha_nacimiento'] = date('Y-m-d', strtotime($data['fecha_nacimiento']));
        $data['comuna_id'] = $request->post('comuna');
        //Crypt Password
        $dataUser['password'] = bcrypt($dataUser['password']);

        $this->nutricionistasRepo->store($data, $dataUser);

        return redirect()->route('crm.nutritionists.index')
            ->with('message', Alerts::CREATE_SUCCESS)
            ->with('messageType', 'success');
    }

    public function edit($id)
    {
        $nutricionista = $this->nutricionistasRepo->with(['comuna'])->findOneBy(['id' => $id]);
        $regiones = $this->regionesRepo->getRegionsList();

        return view('admin.crm.nutricionistas.edit', [
            'nutricionista' => $nutricionista,
            'regiones'      => $regiones
        ]);
    }

    public function update($id, EditNutritionistRequest $request)
    {
        $data = $request->except(['_token', 'region', 'comuna']);
        $data['comuna_id'] = $request->get('comuna');

        $this->nutricionistasRepo->updateBy(['id' => $id], $data);

        return redirect()->route('crm.nutritionists.index')
            ->with('message', Alerts::UPDATE_SUCCESS)
            ->with('messageType', 'success');
    }

    public function delete($id, Request $request)
    {
        $this->nutricionistasRepo->deleteNutritionist($id);

        return redirect()->route('crm.nutritionists.index')
            ->with('message', Alerts::DELETE_SUCCESS)
            ->with('messageType', 'success');
    }

    public function activate($id, Request $request)
    {
        $this->nutricionistasRepo->activate(($id));

        return redirect()->route('crm.nutritionists.index')
            ->with('message', Alerts::ACTIVATE_SUCCESS)
            ->with('messageType', 'success');
    }

    public function suspend($id, Request $request)
    {
        $this->nutricionistasRepo->suspend($id);

        return redirect()->route('crm.nutritionists.index')
            ->with('message', Alerts::SUSPEND_SUCCESS)
            ->with('messageType', 'success');
    }
}
