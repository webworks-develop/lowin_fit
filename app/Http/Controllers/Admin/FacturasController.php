<?php

namespace App\Http\Controllers\Admin;

use PDF;
use App\Globals\Alerts;
use App\Http\Controllers\Controller;
use App\Repositories\Facturas\FacturasRepoI;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class FacturasController extends Controller
{
    protected $facturasRepo;

    public function __construct(FacturasRepoI $facturasRepo)
    {
        $this->facturasRepo = $facturasRepo;
    }

    public function index()
    {
        $facturas = $this->facturasRepo->getAllInvoices();

        return view('admin.crm.facturas.index', [
            'facturas' => $facturas
        ]);
    }

    public function show($id)
    {
        $factura = $this->facturasRepo->getFullInvoiceById($id);

        return view('admin.crm.facturas.show', [
            'factura' => $factura,
        ]);
    }

    public function edit($id)
    {
        $factura = $this->facturasRepo->getFullInvoiceById($id);
        $tiposPago = $this->facturasRepo->getInvoicePaymentTypes();

        return view('admin.crm.facturas.edit', [
            'factura'   => $factura,
            'tiposPago' => $tiposPago
        ]);
    }

    public function approve($id, Request $request)
    {
        $data = $request->except('_token', 'file');
        $file = $request->file;

        if (isset($request->file) && $request->hasFile('file') && $file->isValid()) {

            $filename = $data['boleta'] . '.' . $file->getClientOriginalExtension();

            //Save file in folder temp
            Storage::disk('public')->putFileAs('boletas/' . $id, $file, $filename);

            $data['path_boleta'] = $filename;
        }

        $this->facturasRepo->approve($id, $data);

        return redirect()->route('crm.invoices.index')
            ->with('message', Alerts::ORDER_APPROVED)
            ->with('messageType', 'success');
    }

    public function cancel($id, Request $request)
    {
        $this->facturasRepo->cancel($id);

        return redirect()->route('crm.invoices.index')
            ->with('message', Alerts::ORDER_CANCELED)
            ->with('messageType', 'success');
    }
}
