<?php

namespace App\Http\Controllers\Admin;

use App\Globals\Alerts;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateTrainerRequest;
use App\Http\Requests\EditTrainerRequest;
use App\Repositories\Entrenadores\EntrenadoresRepoI;
use App\Repositories\Regiones\RegionesRepoI;
use Illuminate\Http\Request;

class EntrenadoresController extends Controller
{
    protected $entrenadoresRepo, $regionesRepo;

    public function __construct(EntrenadoresRepoI $entrenadoresRepo, RegionesRepoI $regionesRepo)
    {
        $this->entrenadoresRepo = $entrenadoresRepo;
        $this->regionesRepo = $regionesRepo;
    }

    public function index()
    {
        $entrenadores = $this->entrenadoresRepo->getAllTrainers();

        return view('admin.crm.entrenadores.index', [
            'entrenadores' => $entrenadores
        ]);
    }

    public function create()
    {
        $regiones = $this->regionesRepo->getRegionsList();

        return view('admin.crm.entrenadores.create', [        
            'regiones' => $regiones
        ]);
    }

    public function store(CreateTrainerRequest $request)
    {
        $data = $request->except(['_token', 'region', 'comuna', 'email', 'password', 'password_confirmation']);
        $dataUser = $request->only(['email', 'password']);

        //Format fields
        $data['fecha_nacimiento'] = date('Y-m-d', strtotime($data['fecha_nacimiento']));
        $data['comuna_id'] = $request->post('comuna');
        //Crypt Password
        $dataUser['password'] = bcrypt($dataUser['password']);

        $this->entrenadoresRepo->store($data, $dataUser);

        return redirect()->route('crm.trainers.index')
            ->with('message', Alerts::CREATE_SUCCESS)
            ->with('messageType', 'success');
    }

    public function edit($id)
    {
        $entrenador = $this->entrenadoresRepo->with(['comuna'])->findOneBy(['id' => $id]);
        $regiones = $this->regionesRepo->getRegionsList();

        return view('admin.crm.entrenadores.edit', [
            'entrenador' => $entrenador,
            'regiones'   => $regiones
        ]);
    }

    public function update($id, EditTrainerRequest $request)
    {
        $data = $request->except(['_token', 'region', 'comuna']);
        $data['comuna_id'] = $request->get('comuna');

        $this->entrenadoresRepo->updateBy(['id' => $id], $data);

        return redirect()->route('crm.trainers.index')
            ->with('message', Alerts::UPDATE_SUCCESS)
            ->with('messageType', 'success');
    }

    public function delete($id, Request $request)
    {
        $this->entrenadoresRepo->deleteTrainer($id);

            return redirect()->route('crm.trainers.index')
        ->with('message', Alerts::DELETE_SUCCESS)
            ->with('messageType', 'success');
    }

    public function activate($id, Request $request)
    {
        $this->entrenadoresRepo->activate(($id));

        return redirect()->route('crm.trainers.index')
            ->with('message', Alerts::ACTIVATE_SUCCESS)
            ->with('messageType', 'success');
    }

    public function suspend($id, Request $request)
    {
        $this->entrenadoresRepo->suspend($id);

        return redirect()->route('crm.trainers.index')
            ->with('message', Alerts::SUSPEND_SUCCESS)
            ->with('messageType', 'success');
    }
}
