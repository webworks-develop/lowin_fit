<?php

namespace App\Http\Controllers\Admin;

use Exception;
use App\Events\ContactClientEvent;
use App\Globals\Alerts;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateClientRequest;
use App\Http\Requests\EditClientRequest;
use App\Repositories\Clientes\ClientesRepoI;
use App\Repositories\Planes\PlanesRepoI;
use App\Repositories\Regiones\RegionesRepoI;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ClientesController extends Controller
{
    protected $clientesRepo, $planesRepo, $regionesRepo;

    public function __construct(ClientesRepoI $clientesRepo, PlanesRepoI $planesRepo, RegionesRepoI $regionesRepo)
    {
        $this->clientesRepo = $clientesRepo;
        $this->planesRepo = $planesRepo;
        $this->regionesRepo = $regionesRepo;
    }

    public function index()
    {
        $clientes = $this->clientesRepo->getAllClients();

        return view('admin.crm.clientes.index', [
            'clientes' => $clientes
        ]);
    }

    public function indexPending()
    {
        $clientes = $this->clientesRepo->getAllPendingClients();

        return view('admin.crm.clientes.pendientes', [
            'clientes' => $clientes
        ]);
    }

    public function create()
    {
        $planes = $this->planesRepo->getAllPlans(1);
        $regiones = $this->regionesRepo->getRegionsList();

        return view('admin.crm.clientes.create', [
            'planes'   => $planes,
            'regiones' => $regiones
        ]);
    }

    public function store(CreateClientRequest $request)
    {
        $data = $request->except(['_token', 'region', 'comuna', 'email', 'password', 'password_confirmation', 'plan']);
        $dataUser = $request->only(['email', 'password']);

        //Format fields
        $data['fecha_nacimiento'] = date('Y-m-d', strtotime($data['fecha_nacimiento']));
        $data['comuna_id'] = $request->post('comuna');
        //Crypt Password
        $dataUser['password'] = bcrypt($dataUser['password']);
        
        $this->clientesRepo->store($data, $dataUser, $request->post('plan'));

        return redirect()->route('crm.clients.index')
            ->with('message', Alerts::CREATE_SUCCESS)
            ->with('messageType', 'success');
    }

    public function show($id)
    {
        $cliente = $this->clientesRepo->with(['comuna'])->findOneBy(['id' => $id]);
        $estadosCliente = $this->clientesRepo->getAllClientStatus();
        $regiones = $this->regionesRepo->getRegionsList();

        return view('admin.crm.clientes.show', [
            'cliente'        => $cliente,
            'estadosCliente' => $estadosCliente,
            'regiones'       => $regiones
        ]);
    }

    public function edit($id)
    {
        $cliente = $this->clientesRepo->with(['comuna'])->findOneBy(['id' => $id]);
        $estadosCliente = $this->clientesRepo->getAllClientStatus();
        $regiones = $this->regionesRepo->getRegionsList();

        return view('admin.crm.clientes.edit', [
            'cliente'        => $cliente,
            'estadosCliente' => $estadosCliente,
            'regiones'       => $regiones
        ]);
    }

    public function update($id, EditClientRequest $request)
    {
        $data = $request->except(['_token', 'region', 'comuna']);
        $data['comuna_id'] = $request->get('comuna');

        $this->clientesRepo->updateBy(['id' => $id], $data);

        return redirect()->back()
            ->with('message', Alerts::UPDATE_SUCCESS)
            ->with('messageType', 'success');
    }

    public function delete($id, Request $request)
    {
        $this->clientesRepo->deleteClient($id);

        return redirect()->route('crm.clients.index')
            ->with('message', Alerts::DELETE_SUCCESS)
            ->with('messageType', 'success');
    }

    public function activate($id, Request $request)
    {
        $this->clientesRepo->activate(($id));

        return redirect()->route('crm.clients.index')
            ->with('message', Alerts::ACTIVATE_SUCCESS)
            ->with('messageType', 'success');
    }

    public function suspend($id, Request $request)
    {
        $this->clientesRepo->suspend($id);

        return redirect()->route('crm.clients.index')
            ->with('message', Alerts::SUSPEND_SUCCESS)
            ->with('messageType', 'success');
    }

    public function cancel($id, Request $request)
    {
        $this->clientesRepo->cancel($id);

        return redirect()->route('crm.clients.index')
            ->with('message', Alerts::CANCEL_SUCCESS)
            ->with('messageType', 'success');
    }

    public function contact($id, Request $request)
    {
        try {

            $fileName = null;

            if (isset($request->file) && $request->hasFile('file') && $request->file('file')->isValid()) {
                $file = $request->file('file');
                //Store File
                $fileName = $file->getClientOriginalName();

                Storage::disk('public')->putFileAs('temp/', $file, $fileName);
            }

            //Contact Event
            event(new ContactClientEvent($id, $request->post('asunto'), $request->post('mensaje'), $fileName));

            return redirect()->back()
                ->with('message', 'Mensaje enviado correctamente')
                ->with('messageType', 'success');

        } catch (Exception $e) {
            throw $e;
        }
    }

    public function contactInformation($id)
    {
        $cliente = $this->clientesRepo->findOneBy(['id' => $id]);
        $estadosCliente = $this->clientesRepo->getAllClientStatus();

        return view('admin.crm.clientes.profile', [
            'cliente'        => $cliente,
            'estadosCliente' => $estadosCliente
        ]);
    }

    public function updateContactInformation($id, EditClientRequest $request)
    {
        $data = $request->except(['_token', 'pais', 'estado']);
        $data['pais_id'] = $request->get('pais');
        $data['cliente_estado_id'] = $request->get('estado');

        $this->clientesRepo->updateBy(['id' => $id], $data);

        return redirect()->back()
            ->with('message', Alerts::UPDATE_SUCCESS)
            ->with('messageType', 'success');
    }
}
