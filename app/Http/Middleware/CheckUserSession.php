<?php

namespace App\Http\Middleware;

use Closure;
use App\Globals\Constants;
use App\Helpers\Plan;

class CheckUserSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!request()->session()->has('userSessionActive')) {
            
            $role = auth()->user()->roles->first();

            request()->session()->put('roleName', $role->name);
            request()->session()->put('roleDisplayName', $role->display_name);

            if ($role->name === Constants::NAME_ROLE_CLIENTE) {

                request()->session()->put('userClientStatus', (!empty(auth()->user()->cliente)) ? auth()->user()->cliente->estado->slug : '');

                $planSuscription = app('rinvex.subscriptions.plan_subscriptions')->ofUser(auth()->user())->first();
                $plan = $planSuscription->plan;
                $arrPlanFeatures = $plan->features->pluck('value', 'slug')->toArray();

                $dataPlan = [
                    'slug'         => $plan->slug,
                    'name'         => $plan->name,
                    'startDate'    => date('d-m-Y', strtotime($planSuscription->starts_at)),
                    'endDate'      => date('d-m-Y', strtotime($planSuscription->ends_at))
                ];

                request()->session()->put('plan', $dataPlan);
                request()->session()->put('userSessionActive', 1);
            }
        }

        return $next($request);
    }
}
