<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ContactClientEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $client, $subject, $message, $filename;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($client, $subject, $message, $filename)
    {
        $this->client = $client;
        $this->subject = $subject;
        $this->message = $message;
        $this->filename = $filename;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
