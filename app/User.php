<?php

namespace App;

use App\Globals\Constants;
use App\Notifications\CustomResetPasswordNotification;
use App\Notifications\CustomVerifyEmailNotification;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Rinvex\Subscriptions\Traits\HasSubscriptions;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable implements MustVerifyEmail
{
    use EntrustUserTrait, Notifiable, SoftDeletes, HasSubscriptions;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new CustomVerifyEmailNotification($this->getAttributes()));
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new CustomResetPasswordNotification($token));
    }

    public function restore()
    {
        $this->restoreA();
        $this->restoreB();
    }

    public function cliente()
    {
        return $this->hasOne(\App\Models\Cliente::class, 'user_id');
    }

    public function entrenador()
    {
        return $this->hasOne(\App\Models\Entrenador::class, 'user_id');
    }

    public function nutricionista()
    {
        return $this->hasOne(\App\Models\Nutricionista::class, 'user_id');
    }

    public function getFullName()
    {
        if ($this->hasRole([Constants::NAME_ROLE_SUPERADMINISTRADOR, Constants::NAME_ROLE_ADMINISTRADOR])) {
            $fullName = $this->name;
        } elseif ($this->hasRole(Constants::NAME_ROLE_ENTRENADOR)) {            
            $this->load('entrenador');
            $fullName = $this->entrenador->getFullName();
        } elseif ($this->hasRole(Constants::NAME_ROLE_NUTRICIONISTA)) {
            $this->load('nutricionista');
            $fullName = $this->nutricionista->getFullName();
        } elseif ($this->hasRole(Constants::NAME_ROLE_CLIENTE)) {
            $this->load('cliente');
            $fullName = $this->cliente->getFullName();
        } else {
            $fullName = null;
        }

        return $fullName;
    }

    public function getStatus()
    {
        return ($this->status) ? 'Activo' : 'Inactivo';
    }
}
