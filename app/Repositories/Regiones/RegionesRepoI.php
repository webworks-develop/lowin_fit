<?php

namespace App\Repositories\Regiones;

interface RegionesRepoI
{
	public function getRegionsList();
	public function getRegionsDispatchList();
	public function getRegionsWithComunas();
}