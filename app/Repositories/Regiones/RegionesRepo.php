<?php

namespace App\Repositories\Regiones;

use App\Models\Region;
use App\Globals\Constants;
use App\Repositories\Regiones\RegionesRepoI;
use Illuminate\Support\Facades\DB;
use Inquiloper\BaseRepository\BaseRepository;

class RegionesRepo extends BaseRepository implements RegionesRepoI
{
    public function __construct(Region $model)
    {
        parent::__construct($model);
    }

    public function getRegionsList()
    {
    	return $this->model->orderBy('nombre', 'ASC')->get();
    }

    public function getRegionsDispatchList()
    {
        return $this->model->whereNotIn('nombre', ['Región Metropolitana'])->orderBy('nombre', 'ASC')->get();
    }

    public function getRegionsWithComunas()
    {
        return $this->model->orderBy('nombre', 'ASC')->with(['comunas'])->get();
    }
}