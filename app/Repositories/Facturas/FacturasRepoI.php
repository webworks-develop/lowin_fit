<?php

namespace App\Repositories\Facturas;

interface FacturasRepoI
{
	public function getAllInvoices();
	public function getPendingInvoices();
	public function getApprovedInvoices();
	public function getCanceledInvoices();
	public function getFullInvoiceById($id);
	public function getInvoicePaymentTypes();
	public function createInvoice($data);
	public function approve($id, $data);
	public function cancel($id);
	public function changeStatusInvoice($pedidoId, $status);
}