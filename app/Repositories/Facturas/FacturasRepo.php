<?php

namespace App\Repositories\Facturas;

use Exception;
use App\Events\ApproveInvoiceEvent;
use App\Events\CancelInvoiceEvent;
use App\Globals\Constants;
use App\Models\Cliente;
use App\Models\ClienteEstado;
use App\Models\Factura;
use App\Models\FacturaEstado;
use App\Models\FacturaTipoPago;
use App\Repositories\Custom\CustomRepo;
use App\Repositories\Facturas\FacturasRepoI;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Inquiloper\BaseRepository\BaseRepository;

class FacturasRepo extends BaseRepository implements FacturasRepoI
{
    protected $clienteModel, $clienteEstadoModel, $facturaEstadoModel, $facturaTipoPagoModel;
    protected $customRepo;

    public function __construct(Cliente $cliente, ClienteEstado $clienteEstado, CustomRepo $customRepo, Factura $model, FacturaEstado $facturaEstado, FacturaTipoPago $facturaTipoPago)
    {
        parent::__construct($model);

        $this->clienteModel = $cliente;
        $this->clienteEstadoModel = $clienteEstado;
        $this->facturaEstadoModel = $facturaEstado;
        $this->facturaTipoPagoModel = $facturaTipoPago;

        $this->customRepo = $customRepo;
    }

    public function getAllInvoices()
    {
        return $this->model->when(session()->get('roleName') !== Constants::NAME_ROLE_ADMINISTRADOR, function ($q) {
            return $q->where('cliente_id', auth()->user()->cliente->id);
        })->with(['tipoMoneda', 'cliente', 'estado', 'tipoPago'])->orderBy('id', 'DESC')->get();
    }

    public function getPendingInvoices()
    {
        return $this->model->with(['tipoMoneda', 'cliente', 'estado', 'tipoPago'])->whereHas('estado', function ($query) {
            $query->where('slug', Constants::NAME_ESTADO_FACTURA_PENDIENTE);
        })->orderBy('id', 'DESC')->get();
    }

    public function getApprovedInvoices()
    {
        return $this->model->with(['tipoMoneda', 'cliente', 'estado', 'tipoPago'])->whereHas('estado', function ($query) {
            $query->where('slug', Constants::NAME_ESTADO_FACTURA_APROBADO);
        })->orderBy('id', 'DESC')->get();
    }

    public function getCanceledInvoices()
    {
        return $this->model->with(['tipoMoneda', 'cliente', 'estado', 'tipoPago'])->whereHas('estado', function ($query) {
            $query->where('slug', Constants::NAME_ESTADO_FACTURA_ANULADO);
        })->orderBy('id', 'DESC')->get();
    }

    public function getFullInvoiceById($id)
    {
        return $this->model->with([
            'cliente',
            'cliente.user',
            'detalles',
            'detalles.plan',
            'estado',
            'tipoMoneda',
            'tipoPago'
        ])->findOrFail($id);
    }

    public function getInvoicePaymentTypes()
    {
        return $this->facturaTipoPagoModel->orderBy('id', 'ASC')->get();
    }

    public function createInvoice($data) 
    {
        try {

            DB::beginTransaction();

            $pendingInvoiceEstatus = $this->facturaEstadoModel->where('slug', Constants::NAME_ESTADO_FACTURA_PENDIENTE)->firstOrFail();
            $invoiceTypePayment = $this->facturaTipoPagoModel->where('slug', $data['payment'])->firstOrFail();
            $currencyInvoice = $this->customRepo->getFilterCurrencyBySlug($data['currency']);

            /** Generate Order Invoice */
            $flag = false;

            while (!$flag) {
                $order = Str::random(32);

                if ($this->model->where('orden', $order)->count() == 0) {
                    $flag = true;
                }
            }

            $dataInvoice = [
                'orden'                => $order,
                'fecha'                => date('Y-m-d'),
                'fecha_vencimiento'    => $data['expired'],
                'tipo_moneda_id'       => $currencyInvoice->id,                
                'subtotal'             => $data['price'],
                'total'                => $data['price'],
                'cliente_id'           => $data['client'],
                'factura_estado_id'    => $pendingInvoiceEstatus->id,
                'factura_tipo_pago_id' => $invoiceTypePayment->id
            ];

            $invoice = $this->model->create($dataInvoice);

            /** Store Invoice Details */
            foreach ($data['products'] as $product) {
                $dataDetailInvoice = [
                    'factura_id'  => $invoice->id,
                    'plan_id'     => $product['plan_id'],
                    'nombre'      => $product['plan'],
                    'cantidad'    => $product['qty'],
                    'precio'      => $product['price']
                ];

                $invoice->detalles()->create($dataDetailInvoice);
            }

            DB::commit();
            return $invoice;

        } catch (Exception $e) {
            DB::rollback();
            return false;
        }
    }

    public function approve($id, $data)
    {
        try {

            DB::beginTransaction();

            $approveInvoiceEstatus = $this->facturaEstadoModel->where('slug', Constants::NAME_ESTADO_FACTURA_APROBADO)->firstOrFail();
            $activeClientStatus = $this->clienteEstadoModel->where('slug', Constants::NAME_ESTADO_CLIENTE_ACTIVO)->firstOrFail();
            $invoice = $this->model->find($id);

            $invoice->cliente->update([
                'nombre'            => $data['nombre'],
                'apellido'          => $data['apellido'],
                'telefono'          => $data['telefono'],
                'telefono_extra'    => $data['telefono_extra'],
                'cliente_estado_id' => $activeClientStatus->id
            ]);

            $invoice->update([
                'boleta'               => (isset($data['boleta'])) ? $data['boleta'] : null,
                'path_boleta'          => (isset($data['path_boleta'])) ? $data['path_boleta'] : null,
                'factura_estado_id'    => $approveInvoiceEstatus->id,
                'factura_tipo_pago_id' => $data['tipo_pago']
            ]);

            //Approved Invoice Event
            event(new ApproveInvoiceEvent($id));

            DB::commit();
            return true;

        } catch (Exception $e) {            
            DB::rollback();
            throw $e;
        }
    }

    public function cancel($id)
    {
        try {

            DB::beginTransaction();

            $cancelInvoiceEstatus = $this->facturaEstadoModel->where('slug', Constants::NAME_ESTADO_FACTURA_ANULADO)->firstOrFail();

            $this->model->where('id', $id)->update([
                'factura_estado_id' => $cancelInvoiceEstatus->id
            ]);

            //Cancel Invoice Event
            event(new CancelInvoiceEvent($id));

            DB::commit();
            return true;

        } catch (Exception $e) {
            DB::rollback();
            return false;
        }        
    }

    public function changeStatusInvoice($pedidoId, $status)
    {
        try {

            DB::beginTransaction();

            $this->model->find($pedidoId)->update(['estado' => $status]);

            DB::commit();

            return true;

        } catch (Exception $e) {
            DB::rollback();
            return false;
        }    
    }
}