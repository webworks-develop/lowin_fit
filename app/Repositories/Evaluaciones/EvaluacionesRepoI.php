<?php

namespace App\Repositories\Evaluaciones;

interface EvaluacionesRepoI
{
    public function getUniqueEvaluationCode();
    
    public function createEvaluationTrainers($data, $dataItems);


    public function createEvaluationNutritionists($data, $dataItems);
}