<?php

namespace App\Repositories\Evaluaciones;

use Exception;
use App\Globals\Constants;
use App\Models\Evaluacion;
use App\Repositories\Evaluaciones\EvaluacionesRepoI;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Inquiloper\BaseRepository\BaseRepository;

class EvaluacionesRepo extends BaseRepository implements EvaluacionesRepoI
{
    protected $rolesModel, $userModel;

    public function __construct(Evaluacion $model)
    {
        parent::__construct($model);
    }

    public function getUniqueEvaluationCode()
    {
        $code = null;
        $flag = false;

        while (!$flag) {
            $code = Str::random(25);

            if ($this->model->where('codigo', $code)->count() == 0) {
                $flag = true;
            }
        }

        return $code;
    }

    public function createEvaluationTrainers($data, $dataItems)
    {
        try {

            DB::beginTransaction();

            //Valid Evaluation Code
            $result = $this->model->where('codigo', $data['codigo'])->count();

            if($result > 0) {
                $data['codigo'] = $this->getUniqueEvaluationCode();
            }

            $evaluation = $this->model->create($data);

            foreach ($dataItems as $evaluationItem) {
                /** Set Evaluacion ID */
                $evaluationItem['evaluacion_id'] = $evaluation->id;

                $evaluation->itemsEvaluacion()->create($evaluationItem);
            }            

            DB::commit();
            return true;

        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function createEvaluationNutritionists($data, $dataItems)
    {
        try {

            DB::beginTransaction();

            //Valid Evaluation Code
            $result = $this->model->where('codigo', $data['codigo'])->count();

            if ($result > 0) {
                $data['codigo'] = $this->getUniqueEvaluationCode();
            }

            $evaluation = $this->model->create($data);

            foreach ($dataItems as $evaluationItem) {
                /** Set Evaluacion ID */
                $evaluationItem['evaluacion_id'] = $evaluation->id;

                $evaluation->itemsEvaluacion()->create($evaluationItem);
            }

            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}