<?php

namespace App\Repositories\Custom;

use Exception;
use App\Globals\Constants;
use App\Models\TipoMoneda;
use App\Repositories\Custom\CustomRepoI;
use Illuminate\Support\Facades\DB;
use Inquiloper\BaseRepository\BaseRepository;

class CustomRepo extends BaseRepository implements CustomRepoI
{
    protected $tipoModenaModel;

    public function __construct(TipoMoneda $tipoModenaModel)
    {
        $this->tipoModenaModel = $tipoModenaModel;
    }

    public function getFiltersCurrency()
    {
        return $this->tipoModenaModel->where('estado', 1)->orderBy('id', 'ASC')->get();
    }    

    public function getFilterCurrencyBySlug($slug)
    {
        return $this->tipoModenaModel->where('slug', $slug)->firstOrFail();
    }
}