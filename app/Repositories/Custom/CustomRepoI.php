<?php

namespace App\Repositories\Custom;

interface CustomRepoI
{
    public function getFiltersCurrency();
    public function getFilterCurrencyBySlug($slug);
}