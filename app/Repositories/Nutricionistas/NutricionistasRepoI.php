<?php

namespace App\Repositories\Nutricionistas;

interface NutricionistasRepoI
{
    public function getAllNutritionists();
    public function store($data, $dataUser);
    public function deleteNutritionist($id);
    public function activate($id);
    public function suspend($id);
}
