<?php

namespace App\Repositories\Nutricionistas;

use Exception;
use App\Globals\Constants;
use App\User;
use App\Models\Nutricionista;
use App\Models\Role;
use App\Repositories\Nutricionistas\NutricionistasRepoI;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Inquiloper\BaseRepository\BaseRepository;

class NutricionistasRepo extends BaseRepository implements NutricionistasRepoI
{
    protected $rolesModel, $userModel;

    public function __construct(Nutricionista $model, Role $rolesModel, User $userModel)
    {
        parent::__construct($model);

        $this->rolesModel = $rolesModel;
        $this->userModel = $userModel;
    }

    public function getAllNutritionists()
    {
        return $this->model->orderBy('id', 'ASC')->with(['comuna', 'user'])->get();
    }

    public function store($data, $dataUser)
    {
        try {

            DB::beginTransaction();

            /** Create a user and attach a client role */
            $roleNutritionist = $this->rolesModel->where('name', Constants::NAME_ROLE_NUTRICIONISTA)->firstOrFail();
            $user = $this->userModel->create($dataUser);

            $user->attachRole($roleNutritionist);

            /** Generate Client Code */
            $flag = false;

            while (!$flag) {
                $code = Str::random(15);

                if ($this->model->where('codigo', $code)->count() == 0) {
                    $flag = true;
                }
            }

            $data['codigo'] = $code;
            $data['user_id'] = $user->id;

            $nutritionist = $this->model->create($data);

            //Send Email Verification
            $user->sendEmailVerificationNotification();

            DB::commit();
            return true;

        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function deleteNutritionist($id)
    {
        try {

            DB::beginTransaction();

            $this->model->findOrFail($id)->delete();

            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function activate($id)
    {
        try {

            DB::beginTransaction();

            $this->model->where('id', $id)->update([
                'estado' => 1
            ]);

            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function suspend($id)
    {
        try {

            DB::beginTransaction();

            $this->model->where('id', $id)->update([
                'estado' => 0
            ]);

            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}
