<?php

namespace App\Repositories\Usuarios;

use Exception;
use App\User;
use App\Globals\Constants;
use App\Models\Role;
use App\Repositories\Usuarios\UsuariosRepoI;
use Illuminate\Support\Facades\DB;
use Inquiloper\BaseRepository\BaseRepository;

class UsuariosRepo extends BaseRepository implements UsuariosRepoI
{
	protected $roleModel;

    public function __construct(Role $roleModel, User $model)
    {
        parent::__construct($model);

        $this->roleModel = $roleModel;
    }

    public function getAllUsers()
    {
    	return $this->model->when(auth()->user()->hasRole('superadministrador'), function ($q) {
            return $q->where('id', '!=', auth()->user()->id);
        })->when(! auth()->user()->hasRole('superadministrador'), function ($q) {
            return $q->whereHas('roles', function ($query) {
                $query->whereNotIn('name', ['superadministrador']);
            })->where('id', '!=', auth()->user()->id);
        })->orderBy('id', 'DESC')->get();
    }

    public function getAllAdministrators()
    {
        return $this->model->whereHas('roles', function($query){
            $query->where('roles.name', Constants::NAME_ROLE_ADMINISTRADOR);
        })->get();
    }

    public function getAllClients()
    {
        return $this->model->whereHas('roles', function($query){
            $query->where('roles.name', Constants::NAME_ROLE_CLIENTE);
        })->get();
    }

    public function getCountUsers()
    {
        return $this->model->count();
    }

    public function getUsersByWhereIn($field, $array)
    {
        return $this->model->whereIn($field, $array)->get();
    }

    public function createUser($data, $role)
    {
        try {

            DB::beginTransaction();
                        
            $roles = $this->roleModel->where('name', $role)->first();
            $this->model->fill($data);
                
            $this->model->save();
            $this->model->attachRole($roles);

            DB::commit();

            return true;

        } catch (Exception $e) {
            DB::rollback();
            return false;   
        }
    }

    public function updateUser($id, $data, $role)
    {
        try {

            DB::beginTransaction();
            
            $user = $this->model->find($id);
            $user->fill($data);
            $user->save();

            $rolUser = $user->roles->first();

            //Si se le cambio el rol al usuario
            if($role != $rolUser->name)
            {
                $user->detachRole($rolUser);
                $roles = $this->roleModel->where('name', $role)->first();

                $user->attachRole($roles);
            }

            DB::commit();

            return true;

        } catch (Exception $e) {
            DB::rollback();
            return false;   
        }
    }

    public function suspend($id)
    {
        try {
            
            DB::beginTransaction();

            $this->model->where('id', $id)->update([
                'status' => 0
            ]);

            DB::commit();

            return true;

        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function activate($id)
    {
        try {
            
            DB::beginTransaction();

            $this->model->where('id', $id)->update([
                'status' => 1
            ]);

            DB::commit();

            return true;

        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }      
    }
}