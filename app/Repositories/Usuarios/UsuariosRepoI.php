<?php

namespace App\Repositories\Usuarios;

interface UsuariosRepoI
{
	public function getAllUsers();
	public function getAllAdministrators();
	public function getAllClients();
	public function getCountUsers();
	public function getUsersByWhereIn($field, $array);
	public function createUser($data, $role);
	public function updateUser($id, $data, $role);
	public function suspend($id);
	public function activate($id);
}