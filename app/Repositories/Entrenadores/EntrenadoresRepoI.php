<?php

namespace App\Repositories\Entrenadores;

interface EntrenadoresRepoI
{
    public function getAllTrainers();
    public function store($data, $dataUser);
    public function deleteTrainer($id);
    public function activate($id);
    public function suspend($id);
}