<?php

namespace App\Repositories\Entrenadores;

use Exception;
use App\Globals\Constants;
use App\User;
use App\Models\Entrenador;
use App\Models\Role;
use App\Repositories\Entrenadores\EntrenadoresRepoI;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Inquiloper\BaseRepository\BaseRepository;

class EntrenadoresRepo extends BaseRepository implements EntrenadoresRepoI
{
    protected $rolesModel, $userModel;

    public function __construct(Entrenador $model, Role $rolesModel, User $userModel)
    {
        parent::__construct($model);

        $this->rolesModel = $rolesModel;
        $this->userModel = $userModel;
    }

    public function getAllTrainers()
    {
        return $this->model->orderBy('id', 'ASC')->with(['comuna', 'user'])->get();
    }

    public function store($data, $dataUser)
    {
        try {

            DB::beginTransaction();

            /** Create a user and attach a client role */
            $roleTrainer = $this->rolesModel->where('name', Constants::NAME_ROLE_ENTRENADOR)->firstOrFail();
            $user = $this->userModel->create($dataUser);

            $user->attachRole($roleTrainer);            

            /** Generate Client Code */
            $flag = false;

            while (!$flag) {
                $code = Str::random(15);

                if ($this->model->where('codigo', $code)->count() == 0) {
                    $flag = true;
                }
            }

            $data['codigo'] = $code;
            $data['user_id'] = $user->id;

            $trainer = $this->model->create($data);

            //Send Mail Verification
            $user->sendEmailVerificationNotification();

            DB::commit();
            return true;
            
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function deleteTrainer($id)
    {
        try {

            DB::beginTransaction();

            $this->model->findOrFail($id)->delete();

            DB::commit();
            return true;
            
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function activate($id)
    {
        try {

            DB::beginTransaction();

            $this->model->where('id', $id)->update([
                'estado' => 1
            ]);

            DB::commit();
            return true;

        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function suspend($id)
    {
        try {

            DB::beginTransaction();

            $this->model->where('id', $id)->update([
                'estado' => 0
            ]);

            DB::commit();
            return true;

        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}