<?php

namespace App\Repositories\Planes;

interface PlanesRepoI
{
    public function getAllPlans($active = null);
    public function createPlan($data);
    public function editPlan($id, $data);
    public function suspend($id);
    public function activate($id);
}