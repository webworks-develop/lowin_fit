<?php

namespace App\Repositories\Planes;

use Exception;

use App\Globals\Constantss;
use App\Repositories\Planes\PlanesRepoI;
use Illuminate\Support\Facades\DB;
use Inquiloper\BaseRepository\BaseRepository;
use Rinvex\Subscriptions\Models\Plan as CustomPlan;

class PlanesRepo extends BaseRepository implements PlanesRepoI
{
    public function __construct(CustomPlan $model)
    {
        parent::__construct($model);
    }

    public function getAllPlans($active = null)
    {
        return $this->model->when(!is_null($active), function($q) {
            return $q->where('is_active', 1);
        })->orderBy('id', 'ASC')->get();
    }

    public function createPlan($data)
    {
        try {
            DB::beginTransaction();            

            //Set Customs Rules
            $this->setCustomModelRules();
            $plan = $this->model->create($data);

            DB::commit();
            return true;

        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function editPlan($id, $data)
    {
        try {
            DB::beginTransaction();

            //Set Customs Rules
            $this->setCustomModelRules();
            //Update columns plan
            $plan = $this->model->findOrFail($id);
            $plan->fill($data);
            $plan->save();

            DB::commit();
            return true;
            
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function suspend($id)
    {
        try {
            
            DB::beginTransaction();

            $this->model->where('id', $id)->update([
                'is_active' => 0
            ]);

            DB::commit();
            return true;

        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function activate($id)
    {
        try {
            
            DB::beginTransaction();

            $this->model->where('id', $id)->update([
                'is_active' => 1
            ]);

            DB::commit();
            return true;

        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }      
    }

    private function setCustomModelRules()
    {
        $modelRules = $this->model->getRules();

        $modelRules['signup_fee'] = 'nullable|sometimes';
        $modelRules['invoice_interval'] = 'sometimes|in:hour,day,week,month,quarter,half-year,year';
        $modelRules['trial_interval'] = 'nullable|sometimes|in:hour,day,week,month,quarter,half-year,year';
        $modelRules['grace_interval'] = 'nullable|sometimes|in:hour,day,week,month,quarter,half-year,year';

        //Refresh Model Roles
        $this->model->setRules($modelRules);
    }
}