<?php

namespace App\Repositories\Comunas;

use App\Models\Comuna;
use App\Globals\Constantes;
use App\Repositories\Comunas\ComunasRepoI;
use Illuminate\Support\Facades\DB;
use Inquiloper\BaseRepository\BaseRepository;

class ComunasRepo extends BaseRepository implements ComunasRepoI
{
    public function __construct(Comuna $model)
    {
        parent::__construct($model);
    }

    public function getComunasListByRegion($region)
    {
    	return $this->model->where('region_id', $region)->orderBy('nombre', 'ASC')->get();
    }
}