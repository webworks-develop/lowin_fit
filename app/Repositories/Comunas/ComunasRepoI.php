<?php

namespace App\Repositories\Comunas;

interface ComunasRepoI
{
	public function getComunasListByRegion($region);
}