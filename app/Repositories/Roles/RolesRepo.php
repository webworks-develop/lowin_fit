<?php

namespace App\Repositories\Roles;

use App\Models\Role;
use App\Globals\Constants;
use App\Repositories\Roles\RolesRepoI;
use Illuminate\Support\Facades\DB;
use Inquiloper\BaseRepository\BaseRepository;

class RolesRepo extends BaseRepository implements RolesRepoI
{
    public function __construct(Role $model)
    {
        parent::__construct($model);
    }

    public function getRolesList()
    {
        return $this->model->whereIn('name', [Constants::NAME_ROLE_ADMINISTRADOR])
            ->orderBy('id', 'ASC')
            ->get();
    }
}