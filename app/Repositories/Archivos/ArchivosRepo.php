<?php

namespace App\Repositories\Archivos;

use Exception;
use App\Models\Archivo;
use App\Globals\Constants;
use App\Repositories\Archivos\ArchivosRepoI;
use Inquiloper\BaseRepository\BaseRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ArchivosRepo extends BaseRepository implements ArchivosRepoI
{
    public function __construct(Archivo $model)
    {
        parent::__construct($model);
    }
}