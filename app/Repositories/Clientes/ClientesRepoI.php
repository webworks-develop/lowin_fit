<?php

namespace App\Repositories\Clientes;

interface ClientesRepoI
{
    public function getAllClients();
    public function getAllPendingClients();
    public function getAllApprovedClients();
    public function getAllClientStatus();
    public function register($data);
    public function deleteClient($id);
    public function activate($id);
    public function suspend($id);
    public function cancel($id);
    public function close($id);
}