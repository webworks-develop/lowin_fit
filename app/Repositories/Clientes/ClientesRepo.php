<?php

namespace App\Repositories\Clientes;

use App\Events\NewInvoiceEvent;
use App\Events\RegisteredClientEvent;
use Exception;
use App\User;
use App\Globals\Constants;
use App\Models\Cliente;
use App\Models\ClienteEstado;
use App\Models\Role;
use App\Repositories\Clientes\ClientesRepoI;
use App\Repositories\Facturas\FacturasRepoI;
use Illuminate\Console\Scheduling\Event;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Inquiloper\BaseRepository\BaseRepository;
use Rinvex\Subscriptions\Models\Plan as CustomPlan;
use Rinvex\Subscriptions\Models\PlanSubscription as CustomPlanSubscription;


class ClientesRepo extends BaseRepository implements ClientesRepoI
{
    protected $clienteEstadoModel, $planModel, $planSuscriptionModel, $rolesModel, $userModel;
    protected $facturasRepo;

    public function __construct(
        Cliente $model, 
        ClienteEstado $clienteEstadoModel, 
        FacturasRepoI $facturasRepo, 
        CustomPlan $planModel, 
        CustomPlanSubscription $planSubscriptionModel, 
        Role $rolesModel, 
        User $userModel
    )
    {
        parent::__construct($model);

        $this->clienteEstadoModel = $clienteEstadoModel;
        $this->planModel = $planModel;
        $this->planSuscriptionModel = $planSubscriptionModel;
        $this->rolesModel = $rolesModel;
        $this->userModel = $userModel;

        $this->facturasRepo = $facturasRepo;
    }

    public function getAllClients()
    {
        return $this->model->orderBy('id', 'ASC')->orderBy('id', 'DESC')->with(['comuna', 'estado', 'user', 'user.subscriptions', 'user.subscriptions.plan'])->get();
    }

    public function getAllPendingClients()
    {
        return $this->model->select('cliente.*')->addSelect(DB::raw("(
                SELECT factura.id FROM factura ORDER BY factura.id DESC LIMIT 1
            ) AS id_ultima_factura")
        )->whereHas('estado', function ($query) {
            $query->where('slug', Constants::NAME_ESTADO_CLIENTE_PENDIENTE);
        })->orderBy('id', 'DESC')->with(['comuna', 'estado', 'user', 'user.subscriptions', 'user.subscriptions.plan'])->get();
    }

    public function getAllApprovedClients()
    {
        return $this->model->whereHas('estado', function ($query) {
            $query->where('slug', '<>', Constants::NAME_ESTADO_CLIENTE_PENDIENTE);
        })->orderBy('id', 'DESC')->with(['comuna', 'estado', 'user', 'user.subscriptions', 'user.subscriptions.plan'])->get();
    }

    public function getAllClientStatus()
    {
        return $this->clienteEstadoModel->orderBy('id', 'ASC')->get();
    }

    public function register($data)
    {
        try {

            DB::beginTransaction();

            /** Create a user and attach a client role */
            $roleClient = $this->rolesModel->where('name', Constants::NAME_ROLE_CLIENTE)->firstOrFail();

            $user = $this->userModel->create([
                'email'    => $data['email'],
                'password' => bcrypt($data['password']),
                'status'   => 1
            ]);

            $user->attachRole($roleClient);
            $user->sendEmailVerificationNotification();

            /** Create a client */
            $clientStatusPending = $this->clienteEstadoModel->where('slug', Constants::NAME_ESTADO_CLIENTE_PENDIENTE)->firstOrFail();
            $country = $this->paisModel->where('slug', $data['country'])->firstOrFail();

            /** Generate Client Code */
            $flag = false;

            while (!$flag) {
                $code = Str::random(15);

                if ($this->model->where('codigo', $code)->count() == 0) {
                    $flag = true;
                }
            }

            $client = $this->model->create([
                'codigo'            => $code,
                'nombre'            => $data['name'],
                'apellido'          => $data['lastname'],
                'rut'               => $data['rut'],
                'telefono'          => $data['phone'],
                'telefono_extra'    => $data['extra_phone'],
                'pais_id'           => $country->id,
                'direccion'         => $data['address'],
                'user_id'           => $user->id,
                'cliente_estado_id' => $clientStatusPending->id
            ]);

            /** Create a suscription from plan selected */
            $plan = app('rinvex.subscriptions.plan')->where('slug', $data['plan'])->firstOrFail();
            $user->newSubscription('main', $plan);

            //Get New Plan Suscription
            $planSuscription = app('rinvex.subscriptions.plan_subscriptions')->ofUser($user)->first();

            /** Create a suscription invoice */
            $dataInvoice = [
                'client'    => $client->id,
                'currency'  => strtolower($plan->currency),
                'expired'   => date('Y-m-d', strtotime($planSuscription->ends_at)),
                'payment'   => $data['payment'],
                'price'     => $plan->price,
                'products'  => [
                    [
                        'plan_id'    => $plan->id,
                        'plan'       => $plan->name,
                        'qty'        => 1,
                        'price'      => $plan->price,
                    ]
                ]
            ];

            $invoice = $this->facturasRepo->createInvoice($dataInvoice);

            request()->session()->put('clientRegistered', $client->id);
            request()->session()->put('invoiceClientRegistered', $invoice->id);

            DB::commit();
            return true;

        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function store($data, $dataUser, $plan)
    {
        try {

            DB::beginTransaction();

            /** Create a user and attach a client role */
            $roleClient = $this->rolesModel->where('name', Constants::NAME_ROLE_CLIENTE)->firstOrFail();

            $user = $this->userModel->create($dataUser);

            $user->attachRole($roleClient);            

            /** Create a client */
            $clientStatusPending = $this->clienteEstadoModel->where('slug', Constants::NAME_ESTADO_CLIENTE_PENDIENTE)->firstOrFail();
            /** Generate Client Code */
            $flag = false;

            while (!$flag) {
                $code = Str::random(15);

                if ($this->model->where('codigo', $code)->count() == 0) {
                    $flag = true;
                }
            }

            $data['codigo'] = $code;
            $data['cliente_estado_id'] = $clientStatusPending->id;
            $data['user_id'] = $user->id;

            $client = $this->model->create($data);            

            /** Create a suscription from plan selected */
            $plan = $this->planModel->where('slug', $plan)->firstOrFail();
            $user->newSubscription('main', $plan);

            //Get New Plan Suscription
            $planSuscription = $this->planSuscriptionModel->ofUser($user)->first();

            /** Create a suscription invoice */
            $dataInvoice = [
                'client'    => $client->id,
                'currency'  => strtolower($plan->currency),
                'expired'   => date('Y-m-d', strtotime($planSuscription->ends_at)),
                'payment'   => 'webpay',
                'price'     => $plan->price,
                'products'  => [
                    [
                        'plan_id'    => $plan->id,
                        'plan'       => $plan->name,
                        'qty'        => 1,
                        'price'      => $plan->price,
                    ]
                ]
            ];

            $invoice = $this->facturasRepo->createInvoice($dataInvoice);

            //Send Email Registered Client
            event(new RegisteredClientEvent($client->id));

            //Send Email Confirmation
            $user->sendEmailVerificationNotification();

            //Sened Email New Invoice
            event(new NewInvoiceEvent($invoice->id));

            DB::commit();
            return true;
            
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function deleteClient($id)
    {
        try {

            DB::beginTransaction();

            $this->model->findOrFail($id)->delete();

            DB::commit();
            return true;
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function activate($id)
    {
        try {

            DB::beginTransaction();

            $clientStatus = $this->clienteEstadoModel->where('slug', Constants::NAME_ESTADO_CLIENTE_ACTIVO)->firstOrFail();
            $client = $this->model->where('id', $id)->with(['user'])->firstOrFail();

            //Update Status Client
            $client->cliente_estado_id = $clientStatus->id;
            $client->save();

            DB::commit();
            return true;

        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function suspend($id)
    {
        try {

            DB::beginTransaction();

            $clientStatus = $this->clienteEstadoModel->where('slug', Constants::NAME_ESTADO_CLIENTE_SUSPENDIDO)->firstOrFail();
            $client = $this->model->where('id', $id)->with(['user'])->firstOrFail();

            //Update Status Client
            $client->cliente_estado_id = $clientStatus->id;
            $client->save();

            DB::commit();
            return true;

        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function cancel($id)
    {
        try {

            DB::beginTransaction();

            $clientStatus = $this->clienteEstadoModel->where('slug', Constants::NAME_ESTADO_CLIENTE_ANULADO)->firstOrFail();

            $this->model->where('id', $id)->update([
                'cliente_estado_id' => $clientStatus->id
            ]);

            DB::commit();

            return true;
        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }

    public function close($id)
    {
        try {

            DB::beginTransaction();

            $client = $this->model->findOrFail($id);
            //Delete Client
            $client->delete();

            //Logout User
            auth()->logout();

            DB::commit();
            return true;

        } catch (Exception $e) {
            DB::rollback();
            throw $e;
        }
    }
}