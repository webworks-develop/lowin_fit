<?php

namespace App\Helpers;

use Exception;
use Intervention\Image\Facades\Image;

class ImageResizer
{
    const CODE_FLAG_IMAGE_RESIZER_BANNER = 1;
    const CODE_FLAG_IMAGE_RESIZER_PRODUCT = 2;

    public static function setImageResizer($storagePath, $publicPath, $idColumn, $file, $flag)
    {
        try {

            $imageResize = Image::make($file);

            //Validate Folder
            self::validateFolders($publicPath, $idColumn, $flag);

            // backup status
            $imageResize->backup();
            // perform some modifications
            $imageResize->resize(100, 100);
            $imageResize->save(public_path() . $storagePath . $idColumn . '/thumbs-ico/' . $file->hashName());

            // reset image (return to backup state)
            $imageResize->reset();

            // perform other modifications
            $imageResize->resize(600, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $imageResize->save(public_path() . $storagePath . $idColumn . '/thumbs-min/' . $file->hashName());

            if ($flag === self::CODE_FLAG_IMAGE_RESIZER_BANNER) {
                // reset image (return to backup state)
                $imageResize->reset();

                // perform other modifications
                $imageResize->resize(480, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $imageResize->save(public_path() . $storagePath . $idColumn . '/thumbs-mobile/' . $file->hashName());

                // reset image (return to backup state)
                $imageResize->reset();

                // perform other modifications
                $imageResize->resize(800, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $imageResize->save(public_path() . $storagePath . $idColumn . '/thumbs-tablets/' . $file->hashName());
            }

            return true;

        } catch (Exception $e) {
            throw $e;
        }
    }

    public static function setImageResizerFromTemp($storagePath, $publicPath, $idColumn, $fileTempPath, $filename, $flag)
    {
        try {

            $imageResize = Image::make($fileTempPath);

            //Validate Folder
            self::validateFoldersFromTemp($publicPath, $idColumn);

            // backup status
            $imageResize->backup();
            // perform some modifications
            $imageResize->resize(100, 100);
            $imageResize->save(public_path() . $storagePath . $idColumn . '/thumbs-ico/' . $filename);

            // reset image (return to backup state)
            $imageResize->reset();

            // perform other modifications
            $imageResize->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $imageResize->save(public_path() . $storagePath . $idColumn . '/thumbs-min/' . $filename);

            // reset image (return to backup state)
            $imageResize->reset();
            $imageResize->save(public_path() . $storagePath . $idColumn . '/' . $filename);

            return true;
        } catch (Exception $e) {
            throw $e;
        }
    }

    private static function validateFolders($path, $idColumn, $flag)
    {
        if (!is_dir(storage_path('app' . $path . $idColumn . '/thumbs-ico/'))) {
            mkdir(storage_path('app' . $path . $idColumn . '/thumbs-ico/'));
        }

        if (!is_dir(storage_path('app' . $path . $idColumn . '/thumbs-min/'))) {
            mkdir(storage_path('app' . $path . $idColumn . '/thumbs-min/'));
        }

        if($flag === self::CODE_FLAG_IMAGE_RESIZER_BANNER) 
        {
            if (!is_dir(storage_path('app' . $path . $idColumn . '/thumbs-mobile/'))) {
                mkdir(storage_path('app' . $path . $idColumn . '/thumbs-mobile/'));
            }

            if (!is_dir(storage_path('app' . $path . $idColumn . '/thumbs-tablets/'))) {
                mkdir(storage_path('app' . $path . $idColumn . '/thumbs-tablets/'));
            }
        }        
    }

    private static function validateFoldersFromTemp($path, $idColumn)
    {
        if (!is_dir(storage_path('app' . $path . $idColumn))) {
            mkdir(storage_path('app' . $path . $idColumn));
        }

        if (!is_dir(storage_path('app' . $path . $idColumn . '/thumbs-ico/'))) {
            mkdir(storage_path('app' . $path . $idColumn . '/thumbs-ico/'));
        }

        if (!is_dir(storage_path('app' . $path . $idColumn . '/thumbs-min/'))) {
            mkdir(storage_path('app' . $path . $idColumn . '/thumbs-min/'));
        }
    }
}
