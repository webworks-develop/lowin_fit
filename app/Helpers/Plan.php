<?php

namespace App\Helpers;
class Plan
{
    const CURRENCY_CLP = 'CLP';
    const CURRENCY_USD = 'USD';

    /**
     * List Plans' intervals
     *
     * @var array
     */
    private static $arrayPlansIntervals = [
        'day'   => 'Días',
        'month' => 'Mes(es)',
        'year'  => 'Ano(s)'
    ];

    /**
     * Array Currency Plans
     *
     * @var array
     */
    private static $arrayPlansCurrency = [
        self::CURRENCY_CLP => self::CURRENCY_CLP,
        self::CURRENCY_USD => self::CURRENCY_USD
    ];

    /**
     * Format a specific current
     *
     * @param String $currency
     * @param Float $value
     * 
     * @return String
     */
    public static function formatMoneyValueByCurrencyAndValue(String $currency, Float $value): String
    {
        if(strtoupper($currency) === self::CURRENCY_CLP) {
            return '$' . number_format($value, 0, '.', '.');
        } elseif (strtoupper($currency) === self::CURRENCY_USD) {
            return '$' . number_format($value, 2, '.', ',');
        } else {
            return '';
        }
    }

    /**
     * Format Currency CLP to number format for database
     *
     * @param String $value
     * 
     * @return String
     */
    public static function formatCurrencyCLPtoNumberToSqlStandard(String $value): String
    {
        return str_replace('.', '', substr($value, 1));
    }

    /**
     * Format Currency USD to number format for database
     *
     * @param String $value
     * 
     * @return String
     */
    public static function formatCurrencyUSDtoNumberToSqlStandard(String $value): String
    {
        return str_replace(',', '', substr($value, 1));
    }

    /**
     * List Status
     *
     * @var array
     */
    private static $arrayStatus = [
        0 => 'Inactivo',
        1 => 'Activo'
    ];

    /**
     * Get full text currency
     *
     * @param String $currency
     * @param Float $price
     
     * @return String
     */
    public static function getFullCurrency(String $currency, Float $price): String
    {
        return $currency . ' ' . self::formatMoneyValueByCurrencyAndValue($currency, $price);
    }

    /**
     * Format Currency to number format for database
     *
     * @param String $currency
     * @param Float $price
     * 
     * @return Float
     */
    public static function getFormatCurrencyToNumberToSqlStandard(String $currency, String $price): Float
    {
        if (strtoupper($currency) === self::CURRENCY_CLP) {
            return self::formatCurrencyCLPtoNumberToSqlStandard($price);
        } elseif (strtoupper($currency) === self::CURRENCY_USD) {
            return self::formatCurrencyUSDtoNumberToSqlStandard($price);
        } else {
            return '';
        }
    }

    /**
     * Get a interval name
     *
     * @param String $interval
     * 
     * @return String
     */
    public static function getNameInterval(String $interval): String
    {
        return self::$arrayPlansIntervals[$interval];
    }

    /**
     * Get a list of currency plans
     *
     * @return Array
     */
    public static function getListPlansCurrency(): Array
    {
        return self::$arrayPlansCurrency;
    }

    /**
     * Get a list of intervals
     *
     * @return Array
     */
    public static function getListPlansIntervals(): Array
    {
        return self::$arrayPlansIntervals;
    }

    /**
     * Get a full text status
     *
     * @param Int $status
     * 
     * @return String
     */
    public static function getFullStatus(Int $status): String
    {
        return self::$arrayStatus[$status];
    }
}