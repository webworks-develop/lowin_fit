<?php

namespace App\Notifications;

use App\Repositories\Clientes\ClientesRepoI;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Lang;
use Illuminate\Auth\Notifications\VerifyEmail;

class CustomVerifyEmailNotification extends VerifyEmail
{
    public function toMail($notifiable)
    {
        if (static::$toMailCallback) {
            return call_user_func(static::$toMailCallback, $notifiable);
        }

        $notifiable->load('cliente');

        return (new MailMessage)
            ->subject('Confirmación de correo electrónico | ' . config('app.name'))
            ->view('admin.emails.auth.verify-email', [
                'verificationUrl' => $this->verificationUrl($notifiable),
                'fullName'        => $notifiable->getFullName()
            ]);
    }
}
