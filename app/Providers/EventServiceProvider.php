<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        'App\Events\RegisteredClientEvent' => [
            'App\Listeners\RegisteredClientListener',
        ],
        'App\Events\NewInvoiceEvent' => [
            'App\Listeners\NewInvoiceListener',
        ],
        'App\Events\ApproveInvoiceEvent' => [
            'App\Listeners\ApproveInvoiceListener',
        ],
        'App\Events\CancelInvoiceEvent' => [
            'App\Listeners\CancelInvoiceListener',
        ],
        'App\Events\ContactClientEvent' => [
            'App\Listeners\ContactClientListener',
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
