<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoriesProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\App\Repositories\Archivos\ArchivosRepoI::class, \App\Repositories\Archivos\ArchivosRepo::class);
        $this->app->bind(\App\Repositories\Clientes\ClientesRepoI::class, \App\Repositories\Clientes\ClientesRepo::class);    
        $this->app->bind(\App\Repositories\Comunas\ComunasRepoI::class, \App\Repositories\Comunas\ComunasRepo::class);
        $this->app->bind(\App\Repositories\Custom\CustomRepoI::class, \App\Repositories\Custom\CustomRepo::class);
        $this->app->bind(\App\Repositories\Entrenadores\EntrenadoresRepoI::class, \App\Repositories\Entrenadores\EntrenadoresRepo::class);
        $this->app->bind(\App\Repositories\Evaluaciones\EvaluacionesRepoI::class, \App\Repositories\Evaluaciones\EvaluacionesRepo::class);
        $this->app->bind(\App\Repositories\Facturas\FacturasRepoI::class, \App\Repositories\Facturas\FacturasRepo::class);
        $this->app->bind(\App\Repositories\Planes\PlanesRepoI::class, \App\Repositories\Planes\PlanesRepo::class);
        $this->app->bind(\App\Repositories\Nutricionistas\NutricionistasRepoI::class, \App\Repositories\Nutricionistas\NutricionistasRepo::class);
        $this->app->bind(\App\Repositories\Regiones\RegionesRepoI::class, \App\Repositories\Regiones\RegionesRepo::class);
        $this->app->bind(\App\Repositories\Roles\RolesRepoI::class, \App\Repositories\Roles\RolesRepo::class);
        $this->app->bind(\App\Repositories\Usuarios\UsuariosRepoI::class, \App\Repositories\Usuarios\UsuariosRepo::class);
    }
}
